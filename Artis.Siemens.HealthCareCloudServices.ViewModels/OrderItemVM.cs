﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class OrderItemVM
    {
        public int Id { get; set; }
        public string Item { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public string Batch { get; set; }
        public int Qty { get; set; }
        public string Weight { get; set; }
        public string Dim { get; set; }
        public string DG { get; set; }
        public string SchenkerSHP { get; set; }
        public string SchenkerDEL { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
