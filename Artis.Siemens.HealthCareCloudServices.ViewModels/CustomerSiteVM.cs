﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class CustomerSiteVM
    {
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string FLNumber { get; set; }
        public string SystemStatus { get; set; }
        public string EmployeePrimCSEId { get; set; }
        public string EmployeeSecCSEId { get; set; }
        public string SerialNo { get; set; }
        public string EQDesc { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string DeliveryRemarks { get; set; }
    }
}
