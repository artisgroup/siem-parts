﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class OrderConfirmVM
    {

        public OrderConfirmVM()
        {
            Services = new List<LeadTimeCostFormulaVM>();
            Flights = new List<InternationalFlightsVM>();
            Notification = new NotificationVM();
        }

        public CustomerSiteVM IBase { get; set; }
        public ZMonVM ZMon { get; set; }
        public IList<ZMonVM> ZMons { get; set; }
        public NotificationVM Notification { get; set; }
        public EmployeeVM Employee { get; set; }
        public EmployeeVM AssignedCSE1 { get; set; }
        public EmployeeVM AssignedCSE2 { get; set; }
        public OrderVM Order { get; set; }
        public DateTime? LastVisit { get; set; }
        public DateTime? NextVisit { get; set; }
        public IList<LeadTimeCostFormulaVM> Services { get; set; }
        public IList<InternationalFlightsVM> Flights { get; set; }
        public decimal TotalWeight { get; set; }
        public string WeightWarning { get; set; }
    }

    public class ZMonVM
    {
        public string Notification_Number { get; set; }
        public string Short_Text { get; set; }
        public string Effect_On_Operation { get; set; }
        public string Personal_Number { get; set; }
        public string Task_System_Status { get; set; }
    }

    public class NotificationVM
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string CallerName { get; set; }
        public string CallerPhone { get; set; }
        public DateTime NotificationDateTime { get; set; }
        //public string EQNumber { get; set; }
        //public string WorkCenter { get; set; }
        //public string ServiceRegion { get; set; }
        public string EffectCode { get; set; }
        public string Priority { get; set; }
        public EmployeeVM CallClarifiedEmployee { get; set; }
        public CustomerSiteVM FLBase { get; set; }
        //public string Contract { get; set; }
        // public string CallClarified { get; set; }
        // public string SiteVisit { get; set; }
        // public string NoSiteVisits { get; set; }
        // public string SparePartOrdered { get; set; }
        public string Downtime { get; set; }

    }

    public class LeadTimeCostFormulaVM
    {
        public string Id { get; set; }
        public string OriginRegion { get; set; }
        public string DestinationPostCode { get; set; }
        public string DestinationZone { get; set; }
        public string AvailableServices { get; set; }
        public string Tailgate { get; set; }
        public string CutOffTime { get; set; }
        public string DeliveryTime { get; set; }
        public string RateFormula { get; set; }
        public string ServiceLevelRemarks { get; set; }
        public string ServiceLevelCode { get; set; }
        public string ETA { get; set; }
        public string Cost { get; set; }
    }

    public class InternationalFlightsVM
    {
        public int Id { get; set; }
        public string OriginAirportCode { get; set; }
        public string OriginAirportName { get; set; }
        public string DestinationAirportCode { get; set; }
        public string DestinationAirportName { get; set; }
        public string FlightNo { get; set; }
        public TimeSpan CsmlCutoff { get; set; }
        public TimeSpan DepartureTime { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public TimeSpan AvailableforLocal { get; set; }
        public TimeSpan EstimatedMetroDelivery { get; set; }
        public string DaysServiced { get; set; }
    }
}
