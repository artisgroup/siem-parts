﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{

    public class SchenkerOrderPostVM
    {
        public int OrderId { get; set; }
        public string ServiceLevelCode { get; set; }
        public string ServiceLevelDescription { get; set; }

        //public string Date { get; set; }
        public string WarehousePriority { get; set; }
        public string SiemensContactName { get; set; }
        public string SiemensContactPhone { get; set; }

        //get the amount
        public string Cost { get; set; }
    }

    public class SchenkerOrderVM
    {

        public IList<SchenkerOrderItemVM> OrderItems { get; set; }

        public SchenkerOrderVM()
        {
            OrderItems = new List<SchenkerOrderItemVM>();
        }


        //<?xml version="1.0" encoding="utf-8"?>
        //<create_delivery>

        //  <message_id>DLV_4009067993_02092015065128</message_id>
        public string MessageId { get; set; }

        //  <data_area>
        //    <Outbound_Delivery>
        //      <outbound_header>

        //        #39: I10 <STOPONumber>4506176966</STOPONumber>
        public string STOPONumber { get; set; }

        //        #19: DDMMYY format (I8) <ExpectedDeliveryDate>02092015</ExpectedDeliveryDate>
        public string ExpectedDeliveryDate { get; set; }

        //        #20: A35 <CustomerPurchaseOrderNumber></CustomerPurchaseOrderNumber>
        //public string CustomerPurchaseOrderNumber { get { return ""; } set; }

        //        #21: A40 <DeliveryAddressName1>IMed - Sunshine Coast Medical Imagi</DeliveryAddressName1>: 
        public string DeliveryAddressName1 { get; set; }

        //        #22: A40 <DeliveryAddressName2>Wesley Medical Imaging</DeliveryAddressName2>
        public string DeliveryAddressName2 { get; set; }

        //        #35: A50 <DeliveryAddressName3 />
        public string DeliveryAddressName3 { get; set; }

        //        #24: A51 <DeliveryAddressHouseNumber>3 Lyrebird Street</DeliveryAddressHouseNumber>
        public string DeliveryAddressHouseNumber { get; set; }


        //        #25: A40 <DeliveryAddressCity />
        public const string DeliveryAddressCity = "";

        //        #26: A40 <CityInShipToParty>Buderim</CityInShipToParty>
        public string CityInShipToParty { get; set; }

        //        27: A10 <PostalCode>4556</PostalCode>
        public string PostalCode { get; set; }

        //        #28: A3 <Region>QLD</Region>
        public string Region { get; set; }

        //        #29: A2 <CustomerCountry>AU</CustomerCountry>
        public string CustomerCountry { get; set; }

        //        #30: A3 <Incoterms />
        public const string Incoterms = "";

        //        #31: A40 <UserFirstName>Emma</UserFirstName>
        public string UserFirstName { get; set; }

        //        #32: A40 <UserLastName>Witte</UserLastName>
        public string UserLastName { get; set; }

        //        #33: A241<UserEmail>emma.witte@siemens.com</UserEmail>
        public string UserEmail { get; set; }

        //        #34: A50 <DeliveryAddressStreet2 /> TO BE LEFT BLANK???
        public string DeliveryAddressStreet2 { get; set; }

        //        #23: A40 <DeliveryAddressStreet3>Attn: Rebecca Ryan 07 5373 2904</DeliveryAddressStreet3>
        public string DeliveryAddressStreet3 { get; set; }

        //        <DeliveryAddressStreet4 />
        //          - see DeliveryAddressCity #25
        //          - or #36?
        public string DeliveryAddressStreet4 { get; set; }

        //        #37: A50 <DeliveryAddressStreet5>+61418466467 DIRK KASTEEL</DeliveryAddressStreet5>
        public string DeliveryAddressStreet5 { get; set; }

        //        #38: A50 <UnloadingPoint />
        public string UnloadingPoint { get; set; }

        //        #1: I10 <DeliveryOrder></DeliveryOrder>
        public const string DeliveryOrder = "";

        //        #41: A12 <NotificationNumber>728103060241</NotificationNumber>
        public string NotificationNumber { get; set; }

        //        #42: Time (HH:MM:SS) <DeliveryTime>10:00:00</DeliveryTime>
        public string DeliveryTime { get; set; }

        //        #43: <ServiceLevel />
        public string ServiceLevelCode { get; set; }

        //        #44 <DeliveryType />
        public string DeliveryType { get; set; }

        //        #45: <SiteContactName />
        public string SiteContactName { get; set; }

        //        #46: <SiteContactPhone />
        public string SiteContactPhone { get; set; }

        //        #47: <SiemensContactName />
        public string SiemensContactName { get; set; }

        //        #48: <SiemensContactPhone />
        public string SiemensContactPhone { get; set; }

        //        #49: <WarehousePriority />
        public string WarehousePriority { get; set; }

        //        #50: <SpecialDeliveryRemarks />
        public string SpecialDeliveryRemarks { get; set; }

        //  #8: A4 <Plant>BRS</Plant>
        public string Plant { get; set; }

        //      </outbound_header>

        //    </Outbound_Delivery>
        //  </data_area>
        //</create_delivery>
        //Cost
        public string Cost { get; set; }
    }
}
