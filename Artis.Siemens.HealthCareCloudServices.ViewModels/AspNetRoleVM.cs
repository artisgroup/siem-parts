﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class AspNetRoleVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        

        public AspNetUserVM[] Users { get; set; }
    }
}
