﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class IBaseDeliveryDetailsVM
    {
        public IBaseDeliveryDetail IBaseDeliveryDetail { get; private set; }

        public SelectList DropPoints { get; private set; }

        public IBaseDeliveryDetailsVM(IBaseDeliveryDetail iBaseDeliveryDetail, IEnumerable dropPoints)
        {
            IBaseDeliveryDetail = iBaseDeliveryDetail;
            DropPoints = new SelectList(dropPoints, "Id", "Name1", IBaseDeliveryDetail.DropPointId);
        }


        public IBaseDeliveryDetailsVM(IEnumerable dropPoints)
        {
            IBaseDeliveryDetail = new IBaseDeliveryDetail();
            DropPoints = new SelectList(dropPoints, "Id", "Name1", 0);
        }

        
    }
}
