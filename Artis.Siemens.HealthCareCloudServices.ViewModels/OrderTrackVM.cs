﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{

    public class OrderTrackPostVM
    {
        public string RetrievalMode { get; set; }
        //search values for Track Order Status
        public string Requisitioner { get; set; }
        public string PurchasingDoc { get; set; }
        public string DestinationRegion { get; set; }
        public string OriginLocation { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string ReceivingSloc { get; set; }
        public string CurrentUserLogin { get; set; }
        public bool HotSite { get; set; }
        public bool Historical { get; set; }
}

    public class OrderTrackVM
    {
        public int OrderId { get; set; }
        public string Status { get; set; }
        public string Destination { get; set; }
        public string CustomerSite { get; set; }
        public string CustomerName { get; set; }

        public string CustomerRoom { get; set; }
        public string CustomerDepartment { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string DestPostCode { get; set; }
        public string Region { get; set; }
        public string DestRegion { get; set; }
        public string Country { get; set; }
        public string System { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string Requisitioner { get; set; }
        public string ServiceLevel { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Origin { get; set; }
        public List<string> ConNotes { get; set; }
        public DateTime? LastEvent { get; set; }
        public string Alerts { get; set; }
        public string HotSite { get; set; }
        public string EffectCode { get; set; }
        public string ServiceLevelDesc { get; set; }
        public string EventComment { get; set; }
        public string ServiceLevelType { get; set; }
        public string Tailgate { get; set; }
        public string WarehousePriority { get; set; }
        public string Cost { get; set; }
    }

    public class OrderTrackEventVM
    {
        public DateTime? EventDateTime { get; set; }
        public string EventLocation { get; set; }
        public string ReasonCode { get; set; }
        public string ConNote { get; set; }
        public string ItemNo { get; set; }
        public string EventComment { get; set; }
        public string TrackingURL { get; set; }

    }

}
