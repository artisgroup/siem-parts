﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class ValidationResult
    {
        public ValidationResult()
        {
            this.Fields = new List<Field>();
        }

        public bool HasErrors { get; set; }
        public IList<Field> Fields { get; set; }
    }

    public class Field
    {
        public Field()
        {
            this.ValidationType = ValidationType.Error;
        }
        public string Name { get; set; }
        public string Message { get; set; }
        public ValidationType ValidationType { get; set; }

    }

    public enum ValidationType
    {
        Error,
        Warning
    }
}
