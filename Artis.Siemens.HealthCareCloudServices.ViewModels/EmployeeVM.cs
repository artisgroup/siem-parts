﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class EmployeeVM
    {
        public string Id { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Team { get; set; }
        public string Sloc { get; set; }
        public string Mobile { get; set; }
        public string Name1 { get { return "Siemens Healthcare Ltd"; } }
    }
}
