﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class SchenkerOrderItemVM
    {
        private static string blank = string.Empty;


        //      <outbound_lines>
        //        <STOPOLineNumber>000010<MainItem /><SubItem /><SAPMaterialNumber>8618113</SAPMaterialNumber><OldMaterialNumber /><MaterialDescription>08618113 \Power supply</MaterialDescription><Plant>BRS</Plant><StorageLocation>2015</StorageLocation><OrderQty>1.000</OrderQty><UOM>PC</UOM><BatchNumber>0005856315</BatchNumber><ExpiryDate /><SerialNumber /><Numerator>1</Numerator><Denominator>1</Denominator><BaseUnit>PC</BaseUnit><LoadingGroup>0000</LoadingGroup></STOPOLineNumber>
        //        <STOPOLineNumber>000020<MainItem /><SubItem /><SAPMaterialNumber>8614500</SAPMaterialNumber><OldMaterialNumber /><MaterialDescription>08614500 \Power supply</MaterialDescription><Plant>BRS</Plant><StorageLocation>2015</StorageLocation><OrderQty>1.000</OrderQty><UOM>PC</UOM><BatchNumber>0005744131</BatchNumber><ExpiryDate /><SerialNumber /><Numerator>1</Numerator><Denominator>1</Denominator><BaseUnit>PC</BaseUnit><LoadingGroup>0000</LoadingGroup></STOPOLineNumber>
        //      </outbound_lines>

        //  #40: I6 <STOPOLineNumber>000010
        public string STOPOLineNumber { get; set; }

        //  #3: A1 <MainItem /> blank
        public const string MainItem = "";

        //  #4: I6 <SubItem /> blank
        public const string SubItem = "";

        //  #5: A18 <SAPMaterialNumber>8618113</SAPMaterialNumber>
        public string SAPMaterialNumber { get; set; }

        //  #6: A18 <OldMaterialNumber /> blank
        public const string OldMaterialNumber = "";

        //  #7: A40 <MaterialDescription>08618113 \Power supply</MaterialDescription>
        public string MaterialDescription { get; set; }

        //  #8: A4 <Plant>BRS</Plant>
        public string Plant { get; set; }

        //  #9: A4 <StorageLocation>2015</StorageLocation>
        //  OriginStorageLocation
        public string StorageLocation { get; set; }

        //  #10: DEC(13,3) <OrderQty>1.000</OrderQty>
        public string OrderQty { get; set; }

        //  #11: A3 <UOM>PC</UOM>
        public string UOM { get; set; }

        //  #12: A10 <BatchNumber>0005856315</BatchNumber>
        public string BatchNumber { get; set; }

        //  #13: Date   <ExpiryDate /> blank
        public const string ExpiryDate = "";

        //  #14: A18 <SerialNumber />
        public string SerialNumber { get; set; }

        //  #15: DEC(5,0)  <Numerator>1</Numerator> blank
        public const string Numerator = "";

        //<Denominator>1</Denominator>
        //  #16: DEC(5,0) <BaseUnit>PC</BaseUnit> blank
        public const string Denominator = "";

        //  #18: A4 <LoadingGroup>0000</LoadingGroup> blank
        public const string LoadingGroup = "";


        //</STOPOLineNumber>


    }
}
