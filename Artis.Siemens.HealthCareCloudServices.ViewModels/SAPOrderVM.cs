﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.ViewModels
{
    public class SAPOrderVM
    {
        public string DocType { get; set; }
        public string PurchasingDoc { get; set; }
        public string Item { get; set; }
        public string Material { get; set; }
        public string Batch { get; set; }
        public string Quantity { get; set; }
        public string OrderUnit { get; set; }
        public string SupplyingSloc { get; set; }
        public string ReceivingSloc { get; set; }
        public string DeliveryDate { get; set; }
        public string DeliveryTime { get; set; }
        public string Requisitioner { get; set; }

        
    }
}
