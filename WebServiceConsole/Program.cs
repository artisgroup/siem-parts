﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace WebServiceConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            //string orderxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            //    "<create_delivery>" +
            //        "<message_id>DLV_7506055680_28102015104125</message_id>" +
            //        "<data_area>" +
            //            "<Outbound_Delivery>" +
            //            "<outbound_header>" +
            //                "<STOPONumber>7506055669</STOPONumber>" +
            //              "<ExpectedDeliveryDate>26102015</ExpectedDeliveryDate>" +
            //                "<CustomerPurchaseOrderNumber/>" +
            //                "<DeliveryAddressName1>Royal Melbourne Hospital</DeliveryAddressName1>" +
            //                "<DeliveryAddressName2>Radiol Dept (Angio Lev1)</DeliveryAddressName2>" +
            //                "<DeliveryAddressName3 />" +
            //                "<DeliveryAddressHouseNumber>Grattan Street</DeliveryAddressHouseNumber>" +
            //                "<DeliveryAddressCity />" +
            //                "<CityInShipToParty>Carlton</CityInShipToParty>" +
            //                "<PostalCode>3053</PostalCode>" +
            //                "<Region>VIC</Region>" +
            //                "<CustomerCountry>AU</CustomerCountry>" +
            //                "<Incoterms />" +
            //                "<UserFirstName>Test</UserFirstName>" +
            //                "<UserLastName>Test</UserLastName>" +
            //                "<UserEmail>test@test.com</UserEmail>" +
            //                "<DeliveryAddressStreet2 />" +
            //                "<DeliveryAddressStreet3></DeliveryAddressStreet3>" +
            //                "<DeliveryAddressStreet4 />" +
            //                "<DeliveryAddressStreet5></DeliveryAddressStreet5>" +
            //                "<UnloadingPoint />" +
            //                "<DeliveryOrder/>" +
            //                "<NotificationNumber>728200563615</NotificationNumber>" +
            //                "<DeliveryTime>22:41</DeliveryTime>" +
            //                "<ServiceLevel />" +
            //                "<DeliveryType />" +
            //                "<SiteContactName></SiteContactName>" +
            //                "<SiteContactPhone></SiteContactPhone>" +
            //                "<SiemensContactName />" +
            //                "<SiemensContactPhone />" +
            //                "<WarehousePriority />" +
            //                "<SpecialDeliveryRemarks></SpecialDeliveryRemarks>" +
            //                "<OrderType>{{orderType}}</OrderType>" +
            //                "</outbound_header>" +
            //                "<outbound_lines>" +
            //                "<STOPOLine>" +
            //                "<LineNumber>10</LineNumber>" +
            //                "<MainItem />" +
            //                "<SubItem />" +
            //                "<SAPMaterialNumber>000000000003089208</SAPMaterialNumber>" +
            //                "<OldMaterialNumber />" +
            //                "<MaterialDescription>03089208       \\COPY PROT. USB DONGLE FL</MaterialDescription>" +
            //                "<Plant>SYS</Plant>" +
            //                "<StorageLocation>2011</StorageLocation>" +
            //                "<OrderQty>1</OrderQty>" +
            //                "<UOM>CM</UOM>" +
            //                "<BatchNumber></BatchNumber>" +
            //                "<ExpiryDate />" +
            //                "<SerialNumber />" +
            //                "<Numerator></Numerator>" +
            //                "<Denominator></Denominator>" +
            //                "<BaseUnit></BaseUnit>" +
            //                "<LoadingGroup></LoadingGroup>" +
            //                "</STOPOLine>" +
            //                "</outbound_lines>" +
            //                "</Outbound_Delivery>" +
            //                "</data_area></create_delivery>";
            //string schenkerurl = "https://auwebtest01.dbschenker.com.au/SiemensMessage_Dev";
            //var response = PostToShenker(schenkerurl, orderxml);

            string awsurl = "https://cloudservices.siemens.com/parts/vendor/schenker/orderstatusupdate";
           // string awsurl = "http://localhost:50784/vendor/schenker/orderstatusupdate";

            string postData = @"<SchenkerStatusUpdate>
                                  <PODocNumber>4506055683</PODocNumber>
                                  <ItemNo>000010</ItemNo>
                                  <EventCode>SHP</EventCode>
                                  <EventDateTime>2016-01-27T22:51:50</EventDateTime>
                                  <ReasonCode>0</ReasonCode>
                                  <EventComment>BLANK SHIP-TO-SUBURB.</EventComment>
                                  <MessageDateTime>2016-01-28T08:52:47</MessageDateTime>
                                  <ConNote>4506055683</ConNote>
                                  <CarrierCode>WMS</CarrierCode>
                                  <EventLocation>
                                  </EventLocation>
                                  <TrackingURL>
                                  </TrackingURL>
                                </SchenkerStatusUpdate>";
            string resp = PostToSiemens(awsurl, postData);          
        }
        public static string PostToShenker(string uri, string strPostData)
        {
            HttpWebRequest req = null;
            Stream reqStream = null;
            HttpWebResponse resp = null;
            Stream respStream = null;
            string response = string.Empty;
            try
            {
                req = WebRequest.Create(uri) as HttpWebRequest;
                req.KeepAlive = false;
                req.Method = "POST";
                //ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateServerCertificate);

                byte[] buffer = Encoding.UTF8.GetBytes(strPostData);
                req.ContentLength = buffer.Length;
                req.ContentType = "text/xml";
                req.Proxy = WebRequest.DefaultWebProxy;
                req.Proxy.Credentials = new NetworkCredential(string.Empty, string.Empty, "schenker-asia");
                reqStream = req.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);

                try
                {
                    resp = req.GetResponse() as HttpWebResponse;
                }
                catch (WebException ex)
                {
                    resp = (HttpWebResponse)ex.Response;
                }

                respStream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(respStream);
                response = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
            finally
            {
                if (reqStream != null)
                {
                    reqStream.Close();
                }
                if (resp != null)
                {
                    resp.Close();
                }
                if (respStream != null)
                {
                    respStream.Close();
                }
            }
            return response;
        }
              
        public static string PostToSiemens(string v_strURL ,string xml)
         {
            //Declare XMLResponse document
            XmlDocument XMLResponse = null;

            //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebRequest objHttpWebRequest;

            //Declare an HTTP-specific implementation of the WebResponse class
            HttpWebResponse objHttpWebResponse = null;

            //Declare a generic view of a sequence of bytes
            Stream objRequestStream = null;
            Stream objResponseStream = null;

            //Declare XMLReader
            XmlTextReader objXMLReader;

            //Creates an HttpWebRequest for the specified URL.
            objHttpWebRequest = (HttpWebRequest)WebRequest.Create(v_strURL);

            XmlDocument v_objXMLDoc = new XmlDocument();
            v_objXMLDoc.LoadXml(xml);

            try
            {
                //---------- Start HttpRequest 

                //Set HttpWebRequest properties
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(v_objXMLDoc.InnerXml);
                objHttpWebRequest.Method = "POST";
                objHttpWebRequest.ContentLength = bytes.Length;
                objHttpWebRequest.KeepAlive= false;
                objHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";

           
                //Get Stream object 
                objRequestStream = objHttpWebRequest.GetRequestStream();
               
                    //Writes a sequence of bytes to the current stream 
                    objRequestStream.Write(bytes, 0, bytes.Length);
          

                //Close stream
                objRequestStream.Close();

                //---------- End HttpRequest

                //Sends the HttpWebRequest, and waits for a response.
                objHttpWebResponse = (HttpWebResponse)objHttpWebRequest.GetResponse();

                //---------- Start HttpResponse
                if (objHttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    //Get response stream 
                    objResponseStream = objHttpWebResponse.GetResponseStream();

                    //Load response stream into XMLReader
                    objXMLReader = new XmlTextReader(objResponseStream);

                    //Declare XMLDocument
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(objXMLReader);

                   //Set XMLResponse object returned from XMLReader
                    XMLResponse = xmldoc;

                    //Close XMLReader
                    objXMLReader.Close();
                }

                //Close HttpWebResponse
                objHttpWebResponse.Close();
            }
            catch (WebException we)
            {
                throw new Exception(we.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //Close connections
                objRequestStream.Close();
                objResponseStream.Close();
                objHttpWebResponse.Close();

                //Release objects
                objXMLReader = null;
                objRequestStream = null;
                objResponseStream = null;
                objHttpWebResponse = null;
                objHttpWebRequest = null;
            }

            //Return
            return XMLResponse.InnerXml.ToString();
            }
        
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }


    }
}
