﻿using System.Web;
using System.Web.Optimization;

namespace Artis.Siemens.HealthCareCloudServices.MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-{version}.js",
                       "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/scripts/angular.min.js",
                        "~/scripts/angular-animate.min.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/js/app/oe")
                .Include("~/scripts/app/modules/core/module.js")
                .IncludeDirectory("~/scripts/app/modules/core/directives","*.js",true)
                .Include("~/scripts/app/modules/OrderEntry/module.js")
                .IncludeDirectory("~/scripts/app/modules/OrderEntry/controllers", "*.js", true)
                .IncludeDirectory("~/scripts/app/modules/OrderEntry/services", "*.js", true)
                );

            bundles.Add(new ScriptBundle("~/bundles/MvcBootstrapTimepicker").Include(
                        "~/Scripts/MvcBootstrapTimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/MvcBootstrapTimepicker.css",
                      "~/Content/site.css"));

            //bundles.Add(new ScriptBundle("~/bundles/angular").Include(
            //            "~/Scripts/jquery.validate*",
            //            "~/Scripts/angular.js",
            //            "~/Scripts/angular-resource.js",
            //            "~/Scripts/angular-route.js",
            //            "~/Scripts/app/SparePartDeliveryApp.js"
            //            ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
