﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers
{
    public class LeadTimeCostFormulasController : Controller
    {
        private HCCSContext db = new HCCSContext();

        // GET: LeadTimeCostFormulas
        public ActionResult Index()
        {
            return View(db.LeadTimeCostFormulas.ToList());
        }

        // GET: LeadTimeCostFormulas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeadTimeCostFormula leadTimeCostFormula = db.LeadTimeCostFormulas.Find(id);
            if (leadTimeCostFormula == null)
            {
                return HttpNotFound();
            }
            return View(leadTimeCostFormula);
        }

        // GET: LeadTimeCostFormulas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LeadTimeCostFormulas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OriginRegion,DestinationPostCode,DestinationCity,DestinationZone,Tailgate,ServiceLevelCode,ServiceLevelDescription,CutOffTime,DeliveryTime,RateFormula,ServiceLevelRemarks,LastEditedTime,LastEditedPerson")] LeadTimeCostFormula leadTimeCostFormula)
        {
            if (ModelState.IsValid)
            {
                db.LeadTimeCostFormulas.Add(leadTimeCostFormula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(leadTimeCostFormula);
        }

        // GET: LeadTimeCostFormulas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeadTimeCostFormula leadTimeCostFormula = db.LeadTimeCostFormulas.Find(id);
            if (leadTimeCostFormula == null)
            {
                return HttpNotFound();
            }
            return View(leadTimeCostFormula);
        }

        // POST: LeadTimeCostFormulas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OriginRegion,DestinationPostCode,DestinationCity,DestinationZone,Tailgate,ServiceLevelCode,ServiceLevelDescription,CutOffTime,DeliveryTime,RateFormula,ServiceLevelRemarks,LastEditedTime,LastEditedPerson")] LeadTimeCostFormula leadTimeCostFormula)
        {
            if (ModelState.IsValid)
            {
                db.Entry(leadTimeCostFormula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(leadTimeCostFormula);
        }

        // GET: LeadTimeCostFormulas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LeadTimeCostFormula leadTimeCostFormula = db.LeadTimeCostFormulas.Find(id);
            if (leadTimeCostFormula == null)
            {
                return HttpNotFound();
            }
            return View(leadTimeCostFormula);
        }

        // POST: LeadTimeCostFormulas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LeadTimeCostFormula leadTimeCostFormula = db.LeadTimeCostFormulas.Find(id);
            db.LeadTimeCostFormulas.Remove(leadTimeCostFormula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
