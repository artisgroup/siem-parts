﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using Artis.Siemens.HealthCareCloudServices.EF.Repository;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers
{
    public class InternationalDestinationsController : Controller
    {
        private InternationalDestinationsRepository _repository = new InternationalDestinationsRepository();

        // GET: InternationalDestinations
        public ActionResult Index()
        {
            return View(_repository.All);
        }

        // GET: InternationalDestinations/Details/5
        public ActionResult Details(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            if (internationalDestination == null)
            {
                return HttpNotFound();
            }
            return View(internationalDestination);
        }

        // GET: InternationalDestinations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InternationalDestinations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DestinationPostCode,DestinationRegion,DestinationAirportCode,DestinationAirportName,LastEditedTime,LastEditedPerson")] InternationalDestination internationalDestination)
        {
            if (ModelState.IsValid)
            {
                _repository.InsertOrUpdate(internationalDestination, EntityState.Added);
                _repository.Save();
                return RedirectToAction("Index");
            }

            return View(internationalDestination);
        }

        // GET: InternationalDestinations/Edit/5
        public ActionResult Edit(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            if (internationalDestination == null)
            {
                return HttpNotFound();
            }
            return View(internationalDestination);
        }

        // POST: InternationalDestinations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DestinationPostCode,DestinationRegion,DestinationAirportCode,DestinationAirportName,LastEditedTime,LastEditedPerson")] InternationalDestination internationalDestination)
        {
            if (ModelState.IsValid)
            {
                _repository.InsertOrUpdate(internationalDestination, EntityState.Modified);
                _repository.Save();
                return RedirectToAction("Index");
            }
            return View(internationalDestination);
        }

        // GET: InternationalDestinations/Delete/5
        public ActionResult Delete(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            if (internationalDestination == null)
            {
                return HttpNotFound();
            }
            return View(internationalDestination);
        }

        // POST: InternationalDestinations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            _repository.Delete(internationalDestination);
            _repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
