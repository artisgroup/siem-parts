﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using Artis.Siemens.HealthCareCloudServices.EF.Repository;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers
{
    public class DropPointsController : Controller
    {
        private DropPointsRepository _repository = new DropPointsRepository();

        // GET: DropPoints
        public ActionResult Index()
        {
            return View(_repository.All);
        }

        // GET: DropPoints/Details/5
        public ActionResult Details(int id)
        {
            DropPoint dropPoint = _repository.Find(id);
            if (dropPoint == null)
            {
                return HttpNotFound();
            }
            return View(dropPoint);
        }

        // GET: DropPoints/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DropPoints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name1,Name2,Street,PostCode,Region,Remarks,CutOffBne,CutOffSyd,CutOffMel,CutOffAdl,CutOffPer,Long,LastEditedTime,LastEditedPerson,Lat,Suburb")] DropPoint dropPoint)
        {
            if (ModelState.IsValid)
            {
                _repository.InsertOrUpdate(dropPoint, EntityState.Added);
                _repository.Save();
                return RedirectToAction("Index");
            }

            return View(dropPoint);
        }

        // GET: DropPoints/Edit/5
        public ActionResult Edit(int id)
        {
            DropPoint dropPoint = _repository.Find(id);
            if (dropPoint == null)
            {
                return HttpNotFound();
            }
            return View(dropPoint);
        }

        // POST: DropPoints/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name1,Name2,Street,PostCode,Region,Remarks,CutOffBne,CutOffSyd,CutOffMel,CutOffAdl,CutOffPer,Long,LastEditedTime,LastEditedPerson,Lat,Suburb")] DropPoint dropPoint)
        {
            if (ModelState.IsValid)
            {
                _repository.InsertOrUpdate(dropPoint, EntityState.Modified);
                _repository.Save();
                return RedirectToAction("Index");
            }
            return View(dropPoint);
        }

        // GET: DropPoints/Delete/5
        public ActionResult Delete(int id)
        {
            DropPoint dropPoint = _repository.Find(id);
            if (dropPoint == null)
            {
                return HttpNotFound();
            }
            return View(dropPoint);
        }

        // POST: DropPoints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DropPoint dropPoint = _repository.Find(id);
            _repository.Delete(dropPoint);
            _repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
