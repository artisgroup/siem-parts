﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers.API
{
    [RoutePrefix("api/orderentry")]
    public class OrderEntryController : BaseAPIController
    {
        [HttpGet, Route("ValidateOrderHeader")]
        public IHttpActionResult ValidateOrderHeader()
        {
            return Ok();
        }
    }
}
