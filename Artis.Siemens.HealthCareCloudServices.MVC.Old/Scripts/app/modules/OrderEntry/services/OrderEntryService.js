﻿angular.module('SpareParts.OrderEntry').service('OrderEntryService', OrderEntryService);

OrderEntryService.$inject = ['$http', '$q', '$log'];

function OrderEntryService($http, $q, $log) {
    var _validateOrderHeader = function () {
        $log.log('about to call api');

        return $http({
            method: 'GET',
            url: '/api/orderentry/ValidateOrderHeader'
        });
    };

    return {
        validateOrderHeader: _validateOrderHeader
    }
}