﻿angular.module('SpareParts.OrderEntry').controller('OrderCtrl', OrderCtrl);

OrderCtrl.$inject = ['$log', 'OrderEntryService'];

function OrderCtrl($log, OrderEntryService) {
    var ord = this;

    ord.SAPOrder = '';
    ord.SAPOrderValid = false;
    ord.InvalidOrderMsg = '';
    ord.HeaderValidated = true;
    var SAPOrderObj = {
        DocType: '',
        PurchasingDoc: '',
        Item: '',
        Material: '',
        Batch: '',
        Quantity: '',
        OrderUnit: '',
        SupplyingSloc: '',
        ReceivingSloc: '',
        DeliveryDate: '',
        DeliveryTime: '',
        Requisitioner: ''
    };
    ord.SAPOrderObj = {};

    var Orders = [];

    ord.parseSAPOrder = function () {
        if (!validateInput()) return;
        if (!validateOrdersUnique()) return;
        // all good, carry on
        ord.HeaderValidated = false;
        ord.SAPOrderValid = true;
        ord.SAPOrder = '';
        populateOrderHeader();
        ord.HeaderValidated = true;
        populateOrderDetails();
    };

    function populateOrderHeader() {
        angular.copy(Orders[0], ord.SAPOrderObj);
        var date = ord.SAPOrderObj.DeliveryDate.split('.');
        var time = ord.SAPOrderObj.DeliveryTime.split(':');
        ord.SAPOrderObj.DeliveryDate = moment(date[2] + date[1] + date[0], 'YYYYMMDD').format('DD:MM:YYYY');
        ord.SAPOrderObj.DeliveryTime = time[0] + ':' + time[1];
        OrderEntryService.validateOrderHeader();
    }
    function populateOrderDetails() {

    }


    ord.closeInvalidOrderMsg = function () {
        ord.InvalidOrderMsg = '';
    };

    function validateInput() {
        ord.InvalidOrderMsg = '';
        var orderDetails = ord.SAPOrder.split(/\r\n|\r|\n/g);
        for (var i = 0; i < orderDetails.length; i++) {
            var row = orderDetails[i].split('\t');
            if (row.length !== 12) {
                ord.InvalidOrderMsg = 'The order details you have posted are invalid.  The row should contain at least 12 columns';
                return false;
            }
            var j = 0;
            var order = {};
            angular.copy(SAPOrderObj, order);

            for (var x in order) {
                order[x] = row[j]; j++;
            }
            Orders.push(order);
        }

        $log.log(Orders);
        return true;
    }

    function validateOrdersUnique() {
        if (Orders.length === 1) return true;

        var prevOrder = Orders[0];

        for (var i = 1; i < Orders.length; i++) {
            var currOrder = Orders[i];
            if (
                currOrder.DocType === prevOrder.DocType &&
                currOrder.PurchasingDoc === prevOrder.PurchasingDoc &&
                currOrder.SupplyingSloc === prevOrder.SupplyingSloc &&
                currOrder.ReceivingSloc === prevOrder.ReceivingSloc &&
                currOrder.DeliveryDate === prevOrder.DeliveryDate &&
                currOrder.DeliveryTime === prevOrder.DeliveryTime &&
                currOrder.Requisitioner === prevOrder.Requisitioner &&
                currOrder.Item !== prevOrder.Item
                ) {
                // validation passed
            } else {
                ord.InvalidOrderMsg = 'Your order could not be submitted because the order lines are different.';
                Orders = [];
                return false;
            }
        }
        Orders = [];
        return true;
    }

}

