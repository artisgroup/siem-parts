﻿angular.module('SpareParts').directive('loadingIndicator', loadingIndicator);

function loadingIndicator() {

    return {
        scope: {},
        bindToController: {
            hidewhen: '='
        },
        controller: function () { },
        controllerAs: 'vm',
        template: '<i class="fa fa-spinner fa-pulse" ng-hide="vm.hidewhen"></i>'
    }

}