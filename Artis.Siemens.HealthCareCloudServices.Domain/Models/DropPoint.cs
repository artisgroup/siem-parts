﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    /// <summary>
    /// Annotate this class for MVC presentation
    /// </summary>
    public class DropPoint
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You must enter Drop Point 1 name.")]
        [StringLength(40, ErrorMessage = "Drop Point 1 name must be 40 characters or less.")]
        [Display(Name = "Name")]
        public string Name1 { get; set; }

        
        [StringLength(40, ErrorMessage = "Alternate Drop Point must be 40 characters or less.")]
        [Display(Name = "Alternate Drop Point")]
        public string Name2 { get; set; }

        [Required(ErrorMessage = "You must enter Street.")]
        [StringLength(40, ErrorMessage = "Street must be 40 characters or less.")]
        [Display(Name = "Street")]
        public string Street { get; set; }

        [Required(ErrorMessage = "You must enter PostCode.")]
        [StringLength(40, ErrorMessage = "PostCode must be 6 characters or less.")]
        [Display(Name = "PostCode")]
        public string PostCode { get; set; }

        [Required(ErrorMessage = "You must enter Region.")]
        [StringLength(40, ErrorMessage = "Region must be 6 characters or less.")]
        [Display(Name = "Region")]
        public string Region { get; set; }

        //[Required(ErrorMessage = "You must enter Remarks.")]
        //[StringLength(40, ErrorMessage = "Remarks must be 120 characters or less.")]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Required(ErrorMessage = "You must enter Longitude.")]
        [Display(Name = "Longitude")]
        public double Long { get; set; }

        [Required(ErrorMessage = "You must enter the Latitude.")]
        [Display(Name = "Latitude")]
        public double Lat { get; set; }

        [Required(ErrorMessage = "You must enter the Suburb.")]
        [StringLength(40, ErrorMessage = "Suburb must be 40 characters or less.")]
        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Required(ErrorMessage = "You must enter Cutoff Brisbane.")]
        [DataType(DataType.Duration)]
        [Display(Name = "Cutoff Brisbane")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        public TimeSpan CutOffBne { get; set; }

        [Required(ErrorMessage = "You must enter Cutoff Sydney.")]
        [DataType(DataType.Duration)]
        [Display(Name = "Cutoff Sydney")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        public TimeSpan CutOffSyd { get; set; }

        [Required(ErrorMessage = "You must enter Cutoff Melbourne.")]
        [DataType(DataType.Duration)]
        [Display(Name = "Cutoff Melbourne")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        public TimeSpan CutOffMel { get; set; }

        [Required(ErrorMessage = "You must enter Cutoff Adelaide.")]
        [DataType(DataType.Duration)]
        [Display(Name = "Cutoff Adelaide")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        public TimeSpan CutOffAdl { get; set; }

        [Required(ErrorMessage = "You must enter Cutoff Perth.")]
        [DataType(DataType.Duration)]
        [Display(Name = "Cutoff Perth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        [RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        public TimeSpan CutOffPer { get; set; }

        [Display(Name = "Last Edited")]
        public DateTime? LastEditedTime { get; set; }

        [Display(Name = "Last Edited By")]
        public string LastEditedPerson { get; set; }


    }
}
