﻿namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class ValidationData
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

    }
}
