﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Qualification
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string GID { get; set; }
        public string QualificationType { get; set; }
        public string SAPDivision { get; set; }
        public string IVKGroup { get; set; }
        public int QualificationId { get; set; }
        public string QualificationDesc { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
