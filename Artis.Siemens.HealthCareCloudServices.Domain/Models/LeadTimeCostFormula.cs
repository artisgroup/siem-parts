﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class LeadTimeCostFormula
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You must enter Origin Region.")]
        [StringLength(6, ErrorMessage = "Origin Region name must be 6 characters or less.")]
        [Display(Name = "Origin Region")]
        public string OriginRegion { get; set; }

        [Required(ErrorMessage = "You must enter Destination PostCode.")]
        [StringLength(6, ErrorMessage = "Destination PostCode must be 6 characters or less.")]
        [Display(Name = "Dest. PostCode")]
        public string DestinationPostCode { get; set; }

        [Required(ErrorMessage = "You must enter Destination City.")]
        [StringLength(40, ErrorMessage = "Destination City must be 40 characters or less.")]
        [Display(Name = "Dest. City")]
        public string DestinationCity { get; set; }

        [Required(ErrorMessage = "You must enter Destination Zone.")]
        [StringLength(40, ErrorMessage = "Destination Zone must be 40 characters or less.")]
        [Display(Name = "Dest. Zone")]
        public string DestinationZone { get; set; }

        [Required(ErrorMessage = "You must enter Available Services.")]
        [StringLength(50, ErrorMessage = "Available Services must be 50 characters or less.")]
        [Display(Name = "Avail. Serv.")]
        public string AvailableServices { get; set; }

        [Required(ErrorMessage = "You must enter Tailgate.")]
        [StringLength(1, ErrorMessage = "Must choose Tailgate.")]
        [Display(Name = "Tailgate")]
        public string Tailgate { get; set; }

        [Required(ErrorMessage = "You must enter Service Level Code.")]
        [StringLength(12, ErrorMessage = "Service Level Code must be 12 characters or less.")]
        [Display(Name = "Service Level Code")]
        public string ServiceLevelCode { get; set; }

        [Required(ErrorMessage = "You must enter Service Level Description.")]
        [StringLength(40, ErrorMessage = "Service Level Description must be 40 characters or less.")]
        [Display(Name = "Service Level Desc.")]
        public string ServiceLevelDescription { get; set; }

        [Display(Name = "Cut Off")]
        public string CutOffTime { get; set; }

        //[Required(ErrorMessage = "You must enter Delivery Time.")]
        //[Display(Name = "Delivery Time ")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh\\:mm}")]
        //[RegularExpression(@"((([0-1][0-9])|(2[0-3]))(:[0-5][0-9])(:[0-5][0-9])?)", ErrorMessage = "Time must be between 00:00 to 23:59")]
        [Required(ErrorMessage = "You must enter Delivery Time.")]
        [StringLength(20, ErrorMessage = "Delivery Time must be 20 characters or less.")]
        [Display(Name = "Delivery Time")]
        public string DeliveryTime { get; set; }

        [Required(ErrorMessage = "You must enter Rate Formula.")]
        [StringLength(240, ErrorMessage = "Rate Formula must be 240 characters or less.")]
        [Display(Name = "Rate Formula")]
        public string RateFormula { get; set; }

        [StringLength(120, ErrorMessage = "Service Level Remarks must be 120 characters or less.")]
        [Display(Name = "Service Level Remarks")]
        public string ServiceLevelRemarks { get; set; }

        [Required(ErrorMessage = "You must enter Generic Service.")]
        [StringLength(1, ErrorMessage = "Must choose Generic Service.")]
        [Display(Name = "Generic Service")]
        public string GenericService { get; set; }

        [Display(Name = "Last Edited")]
        public DateTime? LastEditedTime { get; set; }

        [Display(Name = "Last Edited By")]
        public string LastEditedPerson { get; set; }
    }
}
