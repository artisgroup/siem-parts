﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class DeliveryCode
    {
        public int DeliveryCodeId { get; set; }
        public string CarrierStatusCode { get; set; }
        public string Description { get; set; }
        public bool IsDeliveryCode { get; set; }
        public bool IsETACode { get; set; }
    }
}
