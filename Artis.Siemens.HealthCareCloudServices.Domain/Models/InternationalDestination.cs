﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class InternationalDestination
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You must enter PostCode.")]
        [StringLength(6, ErrorMessage = "PostCode must be 6 characters or less.")]
        [Display(Name = "PostCode")]
        public string DestinationPostCode { get; set; }
        
        [Required(ErrorMessage = "You must enter Region.")]
        [StringLength(6, ErrorMessage = "Region must be 6 characters or less.")]
        [Display(Name = "Region")]
        public string DestinationRegion { get; set; }

        [Required(ErrorMessage = "You must enter Airport Code.")]
        [StringLength(6, ErrorMessage = "Airport Code must be 6 characters or less.")]
        [Display(Name = "Airport Code")]
        public string DestinationAirportCode { get; set; }

        [Required(ErrorMessage = "You must enter Airport Name.")]
        [StringLength(40, ErrorMessage = "Airport Name must be 40 characters or less.")]
        [Display(Name = "Airport Name")]
        public string DestinationAirportName { get; set; }

        [Display(Name = "Last Edited Time")]
        public DateTime LastEditedTime { get; set; }

        [Display(Name = "Last Edited Person")]
        public string LastEditedPerson { get; set; }
    }
}
