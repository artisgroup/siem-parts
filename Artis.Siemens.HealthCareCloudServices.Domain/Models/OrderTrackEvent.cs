﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class OrderTrackEvent
    {
        public DateTime? EventDateTime { get; set; }
        public string Location { get; set; }
        public string Event { get; set; }
        public string Text { get; set; }
    }
}
