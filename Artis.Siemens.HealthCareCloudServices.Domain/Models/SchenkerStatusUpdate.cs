﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class SchenkerStatusUpdate
    {
        public int Id { get; set; }
        public string PODocNumber { get; set; }
        public string ItemNo { get; set; }
        public string EventCode { get; set; }
        public DateTime? EventDateTime { get; set; }
        public string ReasonCode { get; set; }
        public string EventComment { get; set; }
        public DateTime MessageDateTime { get; set; }
        public string ConNote { get; set; }
        public string CarrierCode { get; set; }
        public string EventLocation { get; set; }
        public string TrackingURL { get; set; }
    }

}
