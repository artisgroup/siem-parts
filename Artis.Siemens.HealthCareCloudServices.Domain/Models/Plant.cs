﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Plant
    {
        public int Id { get; set; }
        public string OrderType { get; set; }
        public string SLOC { get; set; }
        public string DestinationAirport { get; set; }
        public string TargetPlant { get; set; }
    }
}
