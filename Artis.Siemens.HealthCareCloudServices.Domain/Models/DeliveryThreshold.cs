﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class DeliveryThreshold
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You must enter Delivery Type.")]
        [StringLength(40, ErrorMessage = "DeliveryType must be 40 characters or less.")]
        [Display(Name = "Delivery Type")]
        public string DeliveryType { get; set; }

        [Required(ErrorMessage = "You must enter Max Gross Weight (KG).")]
        [Display(Name = "Max Gross Weight (KG)")]
        public int MaxGrossWeight { get; set; }

        [Required(ErrorMessage = "You must enter Max Combined Dimensions (CM).")]
        [Display(Name = "Max Combined Dimensions (CM)")]
        public int MaxCombinedDimensions { get; set; }

        [StringLength(150, ErrorMessage = "Warning Message must be 150 characters or less.")]
        [Display(Name = "Warning Message")]
        public string WarningMessage { get; set; }

        [Required(ErrorMessage = "You must enter Max Single Dimension (CM).")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Max Single Dimension (CM)")]
        public int MaxSingleDimension { get; set; }

        [Display(Name = "Last Edited")]
        public DateTime? LastEditedTime { get; set; }

        [Display(Name = "Last Edited By")]
        public string LastEditedPerson { get; set; }
    }
}
