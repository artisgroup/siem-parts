﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class InternationalFlightsCutoff
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You must enter Origin Airport Code.")]
        [StringLength(6, ErrorMessage = "Origin Airport Code must be 6 characters or less.")]
        [Display(Name = "Origin Airport Code")]
        public string OriginAirportCode { get; set; }

        [Required(ErrorMessage = "You must enter Origin Airport Name.")]
        [StringLength(40, ErrorMessage = "Origin Airport Name must be 40 characters or less.")]
        [Display(Name = "Origin Airport Name")]
        public string OriginAirportName { get; set; }

        [Required(ErrorMessage = "You must enter Destination Airport Code.")]
        [StringLength(6, ErrorMessage = "Destination Airport Code must be 6 characters or less.")]
        [Display(Name = "Dest. Airport Code")]
        public string DestinationAirportCode { get; set; }

        [Required(ErrorMessage = "You must enter Destination Airport Name.")]
        [StringLength(40, ErrorMessage = "Destination Airport Name must be 40 characters or less.")]
        [Display(Name = "Dest. Airport Name")]
        public string DestinationAirportName { get; set; }

        [Required(ErrorMessage = "You must enter Flight No.")]
        [StringLength(10, ErrorMessage = "Flight No must be 10 characters or less.")]
        [Display(Name = "Flight No")]
        public string FlightNo { get; set; }

        [Required(ErrorMessage = "You must enter CsmlCutoff.")]
        [Display(Name = "CSML Cutoff")]
        public TimeSpan CsmlCutoff { get; set; }

        [Required(ErrorMessage = "You must enter DepartureTime.")]
        [Display(Name = "Departure")]
        public TimeSpan DepartureTime { get; set; }

        [Required(ErrorMessage = "You must enter ArrivalTime.")]
        [Display(Name = "Arrival")]
        public TimeSpan ArrivalTime { get; set; }

        [Required(ErrorMessage = "You must enter Available for Local.")]
        [Display(Name = "Available for Local")]
        public TimeSpan AvailableforLocal { get; set; }

        [Required(ErrorMessage = "You must enterEstimated Metro Delivery.")]
        [Display(Name = "Estimated Metro Delivery")]
        public TimeSpan EstimatedMetroDelivery { get; set; }

        [Required(ErrorMessage = "You must enter Days Serviced.")]
        [StringLength(40, ErrorMessage = "Airport Code must be 40 characters or less.")]
        [Display(Name = "Days Serviced")]
        public string DaysServiced { get; set; }

        [Display(Name = "Last Edited")]
        public DateTime LastEditedTime { get; set; }

        [Display(Name = "Last Edited By")]
        public string LastEditedPerson { get; set; }
    }
}
