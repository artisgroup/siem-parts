﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class MaterialMaster
    {
        public MaterialMaster()
        {
            PartConsumptions=new List<PartConsumption>();
        }


        public string  Id { get; set; }
        public string MaterialDescription { get; set; }
        public string UnitOfMeasure { get; set; }
        public string Size { get; set; }
        public string GrossWeight { get; set; }
        public string NetWeight { get; set; }
        public string WeightUOM { get; set; }
        public string Volume { get; set; }
        public string VolumeUOM { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string DimensionUOM { get; set; }
        public string DangerousGoods { get; set; }
        public string SAPBusinessUnit { get; set; }
        public string Repairable { get; set; }
        public string Quality { get; set; }
        public string TechnicalCriticality { get; set; }
        public string SparePartType { get; set; }
        public string MovingAveragePrice { get; set; }
        //public string Currency { get; set; }

        public virtual ICollection<PartConsumption> PartConsumptions { get; set; }
    }
}
