﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{

    public class AspNetUserRoles
    {
        
        public int UserId { get; set; }
        public int RoleId { get; set; }

        
    }
}
