﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
   
    public class AspNetRole
    {
        public AspNetRole()
        {
            //this.Users = new List<AspNetUser>();
        }
        [Key]
        public int RoleId { get; set; }
        public string Name { get; set; }

        //public virtual ICollection<AspNetUser> Users { get; set; }
    }
}
