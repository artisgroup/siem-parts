﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
   public class AspNetUser
    {
        public AspNetUser()
        {
            this.Roles = new List<AspNetRole>();
        }
        [Key]
        public int UserId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }

       public virtual ICollection<AspNetRole> Roles { get; set; }
    }
}
