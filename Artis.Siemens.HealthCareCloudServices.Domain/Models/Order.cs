﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Order
    {
        public Order() 
        {
            this.Items = new List<OrderItem>();
        }

        public int Id { get; set; }
        public string NotificationNumber { get; set; }
        public string DocType { get; set; }
        public string PONumber { get; set; }
        public string DeliveryType { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string OriginLocation { get; set; }
        public int ReceiverId { get; set; }
        public string Batch { get; set; }
        public string CustomerSite { get; set; }
        public string CustomerSiteContactName { get; set; }
        public string CustomerSiteContactPhone { get; set; }
        public string CustomerSiteDeliveryRemarks { get; set; }
        public int? DropPointId { get; set; }
        public int? EngineerAddressId { get; set; }
        public string OriginRegion { get; set; }
        public string DestRegion { get; set; }
        public string DestPostCode { get; set; }
        public string CreatedBy { get; set; }
        public string SLoc { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        
        public string CustomerRoom { get; set; }
        public string CustomerDepartment { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public DateTime? SchenkerTimestamp { get; set; }
        public string SchenkerResponse { get; set; }
        public string TailGate { get; set; }
        public string SchenkerXML { get; set; }
        public string ServiceLevelType { get; set; }
        public string ServiceLevelDescription { get; set; }
        public string ServiceLevelCode { get; set; }
        public virtual ICollection<OrderItem> Items { get; set; }

        //include warehousepriority
        public string WarehousePriority { get; set; }
        public string Cost { get; set; }
    }
}
