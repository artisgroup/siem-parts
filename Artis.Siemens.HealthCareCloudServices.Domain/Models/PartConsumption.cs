﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class PartConsumption
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int MaterialMasterId { get; set; }
        public string Batch { get; set; }
        public int Quantity { get; set; }
        public double Value { get; set; }
        public DateTime ConsumptionDate { get; set; }

        public virtual MaterialMaster MaterialMaster { get; set; }
        public virtual Notification Notification { get; set; }
    }
}
