﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Notification
    {
        public Notification()
        {
            //Dispatches = new List<Dispatch>();
            //PartConsumptions = new List<PartConsumption>();
            FLBase = new IBase();
            CallClarifiedEmployee = new Employee();
        }


        public string Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        //public string EQNumber { get; set; }
        public string CallerName { get; set; }
        public string CallerPhone { get; set; }
        public DateTime NotificationDateTime { get; set; }
        //public string WorkCenter { get; set; }
        //public string ServiceRegion { get; set; }
        public string EffectCode { get; set; }
        public string Priority { get; set; }
        //public string Contract { get; set; }
       // public string CallClarified { get; set; }
       // public int CallClarifiedEmployeeId { get; set; }
        //public string SiteVisit { get; set; }
        //public string NoSiteVisits { get; set; }
        //public string SparePartOrdered { get; set; }
        public string Downtime { get; set; }
        public IBase FLBase { set; get; }
        public Employee CallClarifiedEmployee { get; set; }
       // public virtual ICollection<Dispatch> Dispatches { get; set; }
       // public virtual ICollection<PartConsumption> PartConsumptions { get; set; }
    }
}
