﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public Nullable<int> EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
