﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class IBaseDeliveryDetail
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "You must enter Drop Point.")]
        [Display(Name = "Drop Point")]
        public int DropPointId { get; set; }

        [Required(ErrorMessage = "You must enter FL#.")]
        [Display(Name = "FL#")]
        public string FLId { get; set; }

        [StringLength(120, ErrorMessage = "Remarks must be 120 characters or less.")]
        [Display(Name = "Remarks")]
        public string SpecialRemarks { get; set; }

        [Required(ErrorMessage = "You must enter Contact Name.")]
        [StringLength(40, ErrorMessage = "Contact Name must be 40 characters or less.")]
        [Display(Name = "Contact Name")]
        public string DeliverySiteContactName { get; set; }

        [StringLength(40, ErrorMessage = "Contact Number must be 40 characters or less.")]
        [Display(Name = "Contact Number")]
        public string DeliverySiteContactNumber { get; set; }

        [Display(Name = "Last Edited")]
        public DateTime LastEditedTime { get; set; }

        [Display(Name = "Last Edited By")]
        public string LastEditedPerson { get; set; }

        public  virtual DropPoint DropPoint { get; set; }

    }
}
