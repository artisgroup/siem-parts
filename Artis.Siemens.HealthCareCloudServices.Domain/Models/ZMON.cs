﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class ZMON
    {

        public int Id { get; set; }
        public string Notification_Number { get; set; }
        public string Sequential_Task_Number { get; set; }
        public string Task_Code { get; set; }
        public string Short_Text_For_Task { get; set; }
        public string For_Task { get; set; }
        public string Notification_Type { get; set; }
        public string Short_Text { get; set; }
        public string Technical_Identification { get; set; }
        public string Priority { get; set; }
        public string Division { get; set; }
        public string Customer_Number { get; set; }
        public string Functional_Location { get; set; }
        public string Equipment_Number { get; set; }
        public string Equipment_Description { get; set; }
        public string Asset_Location { get; set; }
        public Nullable<System.DateTime> Creation_Date { get; set; }
        public Nullable<System.DateTime> Creation_Date_Time { get; set; }
        public Nullable<System.DateTime> Planned_Start_Date { get; set; }
        public Nullable<System.DateTime> Planned_Start_Date_Time { get; set; }
        public Nullable<System.DateTime> Planned_Finish_Date { get; set; }
        public Nullable<System.DateTime> Planned_Finish_Date_Time { get; set; }
        public string Personal_Number { get; set; }
        public string Changed_By { get; set; }
        public string Task_System_Status { get; set; }
        public string Name_Of_Caller { get; set; }
        public string Effect_On_Operation { get; set; }
        public string Notification_System_Status { get; set; }
        public string Long_Text { get; set; }
        public string Sales_Organization { get; set; }
        public string Serial_Number { get; set; }
        public string Location_Name { get; set; }
        public string City { get; set; }
        public string Work_Center { get; set; }
        public Nullable<System.DateTime> EDT { get; set; }

    }
}
