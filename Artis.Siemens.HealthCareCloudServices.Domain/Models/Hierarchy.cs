﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Hierarchy
    {

        public int Id { get; set; }
        public string IVKGroup { get; set; }
        public string SystemGroup { get; set; }
        public string SystemGroupL2 { get; set; }
        public string SystemGroupL3 { get; set; }
        public string Division { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitSubLevel { get; set; }
    }
}
