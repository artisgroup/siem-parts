﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Dispatch
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public bool Cancelled { get; set; }
        public int EmployeeId { get; set; }
        public DateTime ETA { get; set; }
        public DateTime Travel { get; set; }
        public DateTime OnSiteStart { get; set; }
        public DateTime OnSiteFinish { get; set; }


        public virtual Notification Notification { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
