﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class Employee
    {
        public Employee()
        {
            Qualifications = new List<Qualification>();
            Dispatches = new List<Dispatch>();
        }

        public string Id { get; set; }
        public string GID { get; set; }
        public string GivenName { get; set; }
        public string LastName { get; set; }
        public string JobType { get; set; }
        public string ServiceRegion { get; set; }
        public string Team { get; set; }
        public string Position { get; set; }
        public string Sloc { get; set; }
        public string RetSloc { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string PartsName1 { get; set; }
        public string PartsName2 { get; set; }
        public string PartsName3 { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string DeliveryRemarks { get; set; }

       
        
        public virtual ICollection<Qualification> Qualifications { get; set; }
        public virtual ICollection<Dispatch> Dispatches { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        //public virtual ICollection<IBase> SecondaryCSEs { get; set; }
        //public virtual ICollection<IBase> PrimaryFASs { get; set; }
        //public virtual ICollection<IBase> ServiceMgrs { get; set; }
        //public virtual ICollection<IBase> SalesMgrs { get; set; }
        //public virtual ICollection<IBase> PrimaryCSEs { get; set; }


    }
}
