﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain.Models
{
    public class IBase
    {
        public string FLNumber { get; set; }
        public string FLDesc { get; set; }
        public string EQNumber { get; set; }
        public string EQDesc { get; set; }
        public string MaterialNo { get; set; }
        public string SerialNo { get; set; }
        public string SystemStatus { get; set; }
        public string ServiceRegion { get; set; }
        public string Company { get; set; }
        public string Plant { get; set; }
        public string WorkCentre { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public string NameCo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string GeoCodes { get; set; }
        public string TimeZone { get; set; }
        public string Tel { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string Email4 { get; set; }
        public string Email5 { get; set; }
        public string Email6 { get; set; }
        public string Email7 { get; set; }
        public string Email8 { get; set; }
        public string Email9 { get; set; }
        public string Email10 { get; set; }
        public string Comments { get; set; }
        public string SortField { get; set; }
        public string SalesOrg { get; set; }
        public string DistChannel { get; set; }
        public string SAPDivision { get; set; }
        public string URL { get; set; }
        public string EvolvePO { get; set; }
        public string EvolvePOEndDate { get; set; }
        public string EmployeeSecCSEId { get; set; }
        public string EmployeePrimCSEId { get; set; }
        public string EmployeePrimFASId { get; set; }
        public string EmployeeServiceMgrId { get; set; }
        public string EmployeeSalesMgrId { get; set; }
        public string ContractLevel { get; set; }
        public string ContractStart { get; set; }
        public string ContractEnd { get; set; }


        //public MaterialMaster MaterialMaster { get; set; }
        //public virtual Employee EmployeeIDSecCSE { get; set; }
        //public virtual Employee EmployeeIDPrimCSE { get; set; }
        //public virtual Employee EmployeeIDPrimFAS { get; set; }
        //public virtual Employee EmployeeIDServiceMgr { get; set; }
        //public virtual Employee EmployeeIDSalesMgr { get; set; }

        public virtual ICollection<Notification> Notifications { get; set; }


    }
}
