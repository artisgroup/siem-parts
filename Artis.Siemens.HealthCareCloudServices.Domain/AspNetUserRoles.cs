﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Domain
{
    public class AspNetUserRoles
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
