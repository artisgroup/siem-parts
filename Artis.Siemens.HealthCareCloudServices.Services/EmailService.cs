﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class EmailService
    {
        private IEmployeeRepository _empRepo;
        private IOrderRepository _ordRepo;

        public EmailService(IEmployeeRepository empRepo, IOrderRepository ordRepo)
        {
            _ordRepo = ordRepo;
            _empRepo = empRepo;
        }

        public void SendEmail(string emailTemplate, string subject, SchenkerStatusUpdate statusUpdate)
        {
            var emailFrom = "";
            if (ConfigurationManager.AppSettings["EmailFrom"] != null)
                emailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();

            //determine settings for email server
            var emailServer = ConfigurationManager.AppSettings["EmailServer"];
            if (emailServer == null)
                return;

            //username and password for SMTP
            const String SMTP_USERNAME = "AKIAJALM5KQA6PDKLANQ";
            const String SMTP_PASSWORD = "AmNy0k1zP5phX7WFKdoC8QGLpAGfeW9xOw1AbI/0xV4q";
            //get the order - need to get information about it
            var ocVM = _ordRepo.GetByPurchaseOrderNumber(statusUpdate.PODocNumber);

            string fileLocation = "~/app_data/" + emailTemplate + ".htm";
            var emailTemplateContents = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath(fileLocation));

            //set the subject line for the email
            string emailSubject = subject;
            emailSubject = emailSubject.Replace("{{PurchaseOrder}}", ocVM.Order.PurchasingDoc);
            emailSubject = emailSubject.Replace("{{JobNumber}}", ocVM.Order.Requisitioner);

            //replace the bits within the email - from order
            var emailBody = emailTemplateContents;

            emailBody = emailBody.Replace("{{PurchaseOrder}}", ocVM.Order.PurchasingDoc);
            emailBody = emailBody.Replace("{{JobNumber}}", ocVM.Order.Requisitioner);

            //Site name(and address)
            if (ocVM.Order.ServiceLevelType == "C")
            {
                emailBody = emailBody.Replace("{{AddressLine01}}", "Customer Site");
                emailBody = emailBody.Replace("{{AddressLine02}}", ocVM.Order.CustomerSite + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine03}}", "Name 2 (Room): " + ocVM.Order.CustomerRoom + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine04}}", "Name 3 (Department): " + ocVM.Order.CustomerDepartment + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine05}}", "Street: " + ocVM.Order.Street + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine06}}", "City: " + ocVM.Order.City + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine07}}", "State: " + ocVM.Order.DestRegion + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine08}}", "PostCode: " + ocVM.Order.PostCode + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine09}}", "Country: " + ocVM.Order.Country + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine10}}", "Site Contact Name: " + ocVM.Order.CustomerSiteContactName + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine11}}", "Site Contact Phone: " + ocVM.Order.CustomerSiteContactPhone + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine12}}", "Site Specific Delivery Remarks: " + ocVM.Order.CustomerSiteDeliveryRemarks + "<br/>");
            }
            else if (ocVM.Order.ServiceLevelType == "D")
            {
                emailBody = emailBody.Replace("{{AddressLine01}}", "Drop Point");
                emailBody = emailBody.Replace("{{AddressLine02}}", ocVM.Order.CustomerSite + "<br/>");
                if (ocVM.Order.CustomerRoom != null)
                    emailBody = emailBody.Replace("{{AddressLine03}}", ocVM.Order.CustomerRoom + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine04}}", ocVM.Order.Street + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine05}}", ocVM.Order.City + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine06}}", ocVM.Order.DestRegion + ", " + ocVM.Order.DestPostCode + "<br/>");
            }
            else if (ocVM.Order.ServiceLevelType == "E")
            {
                emailBody = emailBody.Replace("{{AddressLine01}}", "Engineer Address");
                emailBody = emailBody.Replace("{{AddressLine02}}", "Siemens Healthcare Ltd<br/>");
                emailBody = emailBody.Replace("{{AddressLine03}}", "c/o " + ocVM.Order.CustomerSite + " <br/>");
                emailBody = emailBody.Replace("{{AddressLine04}}", ocVM.Order.Street + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine05}}", ocVM.Order.City + "<br/>");
                emailBody = emailBody.Replace("{{AddressLine06}}", ocVM.Order.DestRegion + ", " + ocVM.Order.DestPostCode + "<br/>");
            }

            //blank out any unused addresslines
            emailBody = emailBody.Replace("{{AddressLine01}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine02}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine03}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine04}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine05}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine06}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine07}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine08}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine09}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine10}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine11}}", string.Empty);
            emailBody = emailBody.Replace("{{AddressLine12}}", string.Empty);

            //Part number and description and qty
            emailBody = emailBody.Replace("{{PartsOrdered}}", GetPartsDetails(ocVM));

            //Connote / booking reference
            emailBody = emailBody.Replace("{{ConNote}}", statusUpdate.ConNote);

            //Date and time of eta
            emailBody = emailBody.Replace("{{ETADateTime}}", ocVM.Order.DeliveryDate + "  " + ocVM.Order.DeliveryTime);

            //(DSC MEL = despatching warehouse / state)
            emailBody = emailBody.Replace("{{OriginLocation}}", ocVM.Order.OriginLocation);

            // now do the email sending
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = emailServer;
            client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
            //DEBUG -> hMailServer
            //client.UseDefaultCredentials = false;
            //client.Credentials = new System.Net.NetworkCredential("chris@testwookie.com", "wallywrx");

            MailMessage mailItem = new MailMessage(emailFrom, GetEmailRecipients(ocVM));
            mailItem.Subject = emailSubject;
            mailItem.CC.Add(GetEmailCCs());
            mailItem.IsBodyHtml = true;
            mailItem.Body = emailBody;
            client.Send(mailItem);

        }

        private string GetPartsDetails(OrderConfirmVM ocVM)
        {
            string returnText = string.Empty;

            //for each of the items - build up the string to put in the email
            foreach (OrderItemVM oi in ocVM.Order.Items)
            {
                if (returnText != string.Empty) returnText += "<br/>";

                returnText += oi.Description + " (x " + oi.Qty + ")";
            }

            return returnText;
        }

        private string GetEmailCCs()
        {
            string emailCCs = string.Empty;

            //sourced from web.config - separated with comma
            if (ConfigurationManager.AppSettings["EmailCCs"] != null)
                emailCCs = ConfigurationManager.AppSettings["EmailCCs"];

            return emailCCs;
        }

        private string GetEmailRecipients(OrderConfirmVM ocVM)
        {
            //default email address
            string returnEmailAddresses = string.Empty;

            foreach (var z in ocVM.ZMons)
            {
                //When checking ZMON's for people, need to check the "Task_system_status"
                //Only for rows that are tagged as : TSOS, TSRL

                if (z.Task_System_Status == "TSOS" || z.Task_System_Status == "TSRL")
                {
                    Employee e = _empRepo.GetByEmployeeId(z.Personal_Number);

                    if (e != null)
                    {
                        if (returnEmailAddresses != string.Empty) returnEmailAddresses = returnEmailAddresses + ", ";
                        returnEmailAddresses = returnEmailAddresses + e.Email;
                    }
                }
            }

            //if blank - just send to the "FROM" address - and will CC to everyone elses
            if (returnEmailAddresses == "")
            {
                if (ConfigurationManager.AppSettings["EmailFrom"] != null)
                    returnEmailAddresses = ConfigurationManager.AppSettings["EmailFrom"].ToString();
            }

            return returnEmailAddresses;

        }
    }
}
