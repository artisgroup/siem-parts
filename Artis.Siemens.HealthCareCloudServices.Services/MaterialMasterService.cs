﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class MaterialMasterService
    {
        private IMaterialMasterRepository _mmRepo;

        public MaterialMasterService(IMaterialMasterRepository mmRepo)
        {
            _mmRepo = mmRepo;
        }

        public OrderItemVM GetOrderItem(string materialNo)
        {
            var material = _mmRepo.GetByMaterialNo(materialNo);
            if(material != null) return Mapper.Map<OrderItemVM>(material);
            return new OrderItemVM { Material = "Not found" };
        }
    }
}
