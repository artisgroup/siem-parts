﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class DateTimeService
    {
        public static string GetTimeForRegion(string regionName)
        {
            TimeZoneRegion region;
            TimeZoneInfo timezone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");

            if (regionName != null)
            {
                region = (TimeZoneRegion)Enum.Parse(typeof(TimeZoneRegion), regionName);


                timezone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                switch (region)
                {
                    case TimeZoneRegion.VIC:
                    case TimeZoneRegion.NSW:
                    case TimeZoneRegion.TAS:
                        timezone = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                        break;
                    case TimeZoneRegion.QLD:
                        timezone = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
                        break;
                    case TimeZoneRegion.SA:
                        timezone = TimeZoneInfo.FindSystemTimeZoneById("Cen. Australia Standard Time");
                        break;
                    case TimeZoneRegion.SGP:
                        timezone = TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time");
                        break;
                    case TimeZoneRegion.FRA:
                        timezone = TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time");
                        break;
                    case TimeZoneRegion.WA:
                        timezone = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                        break;
                }
            }
                return TimeZoneInfo.ConvertTime(DateTime.Now, timezone).ToString("HH:mm");
            
        }

        
        
    }

    public enum TimeZoneRegion
    {
        VIC,
        NSW,
        QLD,
        SA,
        TAS,
        SGP,
        FRA,
        WA
    }
}
