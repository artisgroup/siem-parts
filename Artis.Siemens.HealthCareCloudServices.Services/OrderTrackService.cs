﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using System.Globalization;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using System.Collections;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class OrderTrackService
    {
        private IOrderTrackRepository _orderTrackRepo;

        public OrderTrackService(IOrderTrackRepository orderTrackRepo)
        {
            _orderTrackRepo = orderTrackRepo;
        }

        public IList<OrderTrack> GetTrackDeliveries(OrderTrackPostVM postData)
        {
            return _orderTrackRepo.GetTrackDeliveries(postData);
        }
        public IList<OrderItem> GetPartsOrdered(string purchaseOrder)
        {
            return _orderTrackRepo.GetPartsOrdered(purchaseOrder);
        }
        public IList<SchenkerStatusUpdate> GetTransportEvents(string purchaseOrder)
        {
            return _orderTrackRepo.GetTransportEvents(purchaseOrder);

        }
        //public IList<OrderItem> GetPartsOrdered(string purchaseOrder, string conNote)
        //{
        //    return _orderTrackRepo.GetPartsOrdered(purchaseOrder, conNote);
        //}
        //public IList<SchenkerStatusUpdate> GetTransportEvents(string purchaseOrder, string conNote)
        //{
        //    return _orderTrackRepo.GetTransportEvents(purchaseOrder, conNote);

        //}
    }
}
