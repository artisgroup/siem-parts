﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using Zirpl.CalcEngine;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class OrderConfirmService
    {
        private IOrderRepository _orderRepo;
        private OrderWeightService _ows;

        public OrderConfirmService(IOrderRepository orderRepo, OrderWeightService ows)
        {
            _orderRepo = orderRepo;
            _ows = ows;
        }

        public OrderConfirmVM GetOrderById(int id)
        {
            var orderDetails = _orderRepo.GetById(id);

            foreach (var lcf in orderDetails.Services)
            {
                //lcf.ETA = _calculateETA(lcf.DeliveryTime, orderDetails.Order.OriginRegion);
                lcf.Cost = _calculateCost(orderDetails.Order, lcf.RateFormula);
            }

            orderDetails.TotalWeight = _ows.CalculateDeadWeight(orderDetails.Order);
            orderDetails.WeightWarning = "";

            if(orderDetails.Order != null) CheckThresholds(orderDetails);

            //if(orderDetails.TotalWeight > thresholds.MaxGrossWeight)

            //foreach(var flight in orderDetails.Flights)
            //{
            //    var now = Convert.ToDateTime(DateTimeService.GetTimeForRegion(flight.OriginAirportCode)).Date;
            //    var cutOff = now.Add(flight.CsmlCutoff);
            //    var departure = now.Add(flight.DepartureTime);
            //}

            return orderDetails;
        }

        public IList<LeadTimeCostFormula> GetGenericServices()
        {
            return _orderRepo.GetGenericServices();
        }

        //public string CalculateETA(string deliveryLeadTime, string originRegion)
        //{
        //    TimeSpan ts;
        //    if (TimeSpan.TryParse(deliveryLeadTime, out ts))
        //        return _calculateETA(ts, originRegion);

        //    else
        //        return string.Empty;
        //}

        private void CheckThresholds(OrderConfirmVM orderDetails)
        {
            var thresholds = _orderRepo.GetDeliveryThresholds(orderDetails.Order.DropPointId.HasValue ? "DropPoint" : "Other");

            foreach (var item in orderDetails.Order.Items)
            {
                if (_ows.GetWeightFromItem(item.Weight) > thresholds.MaxGrossWeight)
                {
                    orderDetails.WeightWarning = thresholds.WarningMessage;
                    break;
                }

                var dimensions = _ows.GetDimensions(item.Dim);
                if (dimensions.Sum() > thresholds.MaxCombinedDimensions)
                {
                    orderDetails.WeightWarning = thresholds.WarningMessage;
                    break;
                }

                foreach (var dimension in dimensions)
                {
                    if (dimension > thresholds.MaxSingleDimension)
                    {
                        orderDetails.WeightWarning = thresholds.WarningMessage;
                        break;
                    }
                }
            }
        }
        
        private string _calculateCost(OrderVM orderVM, string formula)
        {
            var weight = _ows.GetWeightForCalculation(orderVM);
            formula = formula.ToLower().Replace("weight", weight.ToString());

            //ditch the calculation - if it has KM
            // if there is a "KM" calculation - the exact amount will be included in the column for "RateFormula"
            if (formula.ToLower().IndexOf("km") > 0)
                return string.Empty;

            //do a calc for the formula - if it errors, return BLANK
            var ce = new CalculationEngine();
            string returnResult = string.Empty;

            try
            {
                //do the math calc - and return formatted correctly
                returnResult = string.Format("{0:C}", ce.Evaluate(formula));
            }
            catch
            {
                //if there is a calc error - eg. contains "not applicable" - return blank
                returnResult = "";
            }

            return returnResult;

        }

        private string _calculateETA(TimeSpan deliveryLeadTime, string originRegion)
        {
            var timeAtOrigin = Convert.ToDateTime(DateTimeService.GetTimeForRegion(originRegion));
            var eta = timeAtOrigin.Add(deliveryLeadTime);
            return eta.ToString("dd/MM/yyyy - HH:mm:ss");
        }
    }
}
