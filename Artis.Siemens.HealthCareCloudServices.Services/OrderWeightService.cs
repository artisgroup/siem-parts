﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class OrderWeightService
    {
        public decimal GetWeightForCalculation(OrderVM order)
        {
            var deadWeight = CalculateDeadWeight(order);
            var dimWeight = CalculatedDimensionalWeight(order);

            return deadWeight > dimWeight ? deadWeight : dimWeight;
        }

        public decimal CalculateDeadWeight(OrderVM order)
        {
            // Weight returned is always in KG
            decimal weight = 0;
            if (order != null)
            {
                foreach (var item in order.Items)
                {
                    weight += GetWeightFromItem(item.Weight) * Convert.ToDecimal(item.Qty);
                }
            }
            return weight;
        }

        public decimal GetWeightFromItem(string itemWeight)
        {
            decimal calculatedWeight = 0;

            if (itemWeight != null)
            {
                var weightArray = itemWeight.Split(' ');
                var weight = Convert.ToDecimal(weightArray[0]);
                var uom = weightArray[1];
                calculatedWeight = weight;
                switch (uom.ToUpper().Trim())
                {
                    case "G":
                        calculatedWeight = weight / 1000;
                        break;
                    case "MG":
                        calculatedWeight = weight / (1000 * 1000);
                        break;
                }

            }

            return calculatedWeight;
        }

        public decimal[] GetDimensions(string dimensions)
        {
            string[] dimArray;
            decimal length;
            decimal width;
            decimal height;
            GetLWH(dimensions, out dimArray, out length, out width, out height);

            var uom = dimArray[2].Split(' ')[1];

            switch (uom)
            {
                case "M":
                    length *= 100;
                    width *= 100;
                    height *= 100;
                    break;
                case "MM":
                    length /= 10;
                    width /= 10;
                    height /= 10;
                    break;
            }

            return new decimal[] { length, width, height };
        }



        #region Private Methods

        private decimal CalculatedDimensionalWeight(OrderVM order)
        {
            decimal weight = 0;
            foreach (var item in order.Items)
            {
                weight += GetDimWeightForItem(item.Dim) * Convert.ToDecimal(item.Qty);
            }
            return weight / 1000;
        }

        private static void GetLWH(string dimensions, out string[] dimArray, out decimal length, out decimal width, out decimal height)
        {

            dimArray = dimensions.Split('x');
            length = Convert.ToDecimal(dimArray[0]);
            width = Convert.ToDecimal(dimArray[1]);
            height = Convert.ToDecimal(dimArray[2].Replace(" CM", "").Replace("M", " "));
        }

        private decimal GetDimWeightForItem(string dimensions)
        {
            string[] dimArray;
            decimal length;
            decimal width;
            decimal height;
            GetLWH(dimensions, out dimArray, out length, out width, out height);

            if (length == 0 || width == 0 || height == 0) return 0;

            var uom = dimArray[2].Split(' ')[1];
            switch (uom.ToUpper().Trim())
            {
                case "M":
                    return length * width * height;
                case "CM":
                    return (length / 100) * (width / 100) * (height / 100);
                case "MM":
                    return (length / 100 * 10) * (width / 100 * 10) * (height / 100 * 10);
            }
            return 0;
        }

        #endregion
    }
}
