﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using System.Globalization;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using AutoMapper;
using System.Collections;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class OrderEntryService
    {

        private List<OriginLocationMapping> _olMappings;
        private IOrderRepository _orderRepo;
        private IEmployeeRepository _empRepo;
        private Hashtable _origLocations = new Hashtable();
        private OrderWeightService _ows;
        public OrderEntryService(IOrderRepository orderRepo, OrderWeightService ows, IEmployeeRepository empRepo)
        {
            _orderRepo = orderRepo;
            _empRepo = empRepo;
            _ows = ows;
            _olMappings = new List<OriginLocationMapping>();
            _olMappings.Add(new OriginLocationMapping { DocType = "UB", SupplyingSloc = "2012", Name = "Schenker - Sydney - 2012" });
            _olMappings.Add(new OriginLocationMapping { DocType = "UB", SupplyingSloc = "2011", Name = "Schenker - Melbourne - 2011" });
            _olMappings.Add(new OriginLocationMapping { DocType = "UB", SupplyingSloc = "2013", Name = "Schenker - Adelaide - 2013" });
            _olMappings.Add(new OriginLocationMapping { DocType = "UB", SupplyingSloc = "2014", Name = "Schenker - Perth - 2014" });
            _olMappings.Add(new OriginLocationMapping { DocType = "UB", SupplyingSloc = "2015", Name = "Schenker - Brisbane - 2015" });
            _olMappings.Add(new OriginLocationMapping { DocType = "ZS", SupplyingSloc = "", Name = "CSML - Singapore" });
            _olMappings.Add(new OriginLocationMapping { DocType = "EB", SupplyingSloc = "", Name = "CSML - Frankfurt" });
            _olMappings.Add(new OriginLocationMapping { DocType = "NB", SupplyingSloc = "", Name = "CSML - Frankfurt" });

            _origLocations.Add("Schenker - Sydney - 2012".ToLower(), "NSW");
            _origLocations.Add("Schenker - Melbourne - 2011".ToLower(), "VIC");
            _origLocations.Add("Schenker - Adelaide - 2013".ToLower(), "SA");
            _origLocations.Add("Schenker - Perth - 2014".ToLower(), "WA");
            _origLocations.Add("Schenker - Brisbane - 2015".ToLower(), "QLD");
            _origLocations.Add("CSML - Singapore".ToLower(), "SGP");
            _origLocations.Add("CSML - Frankfurt".ToLower(), "FRA");
            //For null values
            _origLocations.Add("", "");

        }

        public string GetOriginRegion(string originLocation)
        {
            return _origLocations[originLocation.ToLower()].ToString();

        }

        public IList<EmployeeVM> SearchEmp(string query)
        {
            var employees = _empRepo.SearchEA(query);
            return Mapper.Map<IList<EmployeeVM>>(employees);
        }

        public ValidationResult ValidateSAPOrder(SAPOrderVM orderHeader)
        {

            var validationResult = new ValidationResult();
            validationResult = ValidateNotoficationNumber(orderHeader.Requisitioner);
            validationResult = ValidateDate(validationResult, orderHeader.DeliveryDate);
            validationResult = ValidatePONumber(validationResult, orderHeader.PurchasingDoc);

            //Displays only warning message to Notification Number and allows to put an order
            if (validationResult.Fields.Count > 0)
            {
                if (validationResult.Fields[0].Name != "Notification Number")
                {
                    validationResult.HasErrors = validationResult.Fields.Count > 0;
                }
            }

            return validationResult;
        }

        public string GetOriginLocation(string docType, string supplyingSloc)
        {
            if (docType.ToUpper() == "UB")
            {
                var mapping = _olMappings.Where(m =>
                    string.Compare(m.DocType, docType, true) == 0 && string.Compare(m.SupplyingSloc, supplyingSloc, true) == 0).FirstOrDefault();
                return mapping == null ? "" : mapping.Name;
            }
            else
            {
                var mapping = _olMappings.Where(m =>
                string.Compare(m.DocType, docType, true) == 0).FirstOrDefault();
                return mapping == null ? "" : mapping.Name;
            }
        }

        public CustomerSiteVM GetCustomerSite(string notificationNumber)
        {
            return Mapper.Map<CustomerSiteVM>(_orderRepo.GetIBase(notificationNumber));
        }

        public DropPoint GetDropPoint(string notificationnumber)
        {
            return _orderRepo.GetDropPoint(notificationnumber);
        }

        public EmployeeVM GetEmployeeById(string empNo)
        {
            return Mapper.Map<EmployeeVM>(_orderRepo.GetEmployeeById(empNo));
        }
        public EmployeeVM GetEmployeeBySloc(string sloc)
        {
            return Mapper.Map<EmployeeVM>(_orderRepo.GetEmployeeBySloc(sloc));
        }
        public int Create(OrderVM orderVM)
        {
            Order order = null;
            List<OrderItem> items = null;

            //if doing an update - need to reload the model of stuff
            if (orderVM.Id != null)
            {
                //initial load of stuff
                OrderConfirmVM ocVM = _orderRepo.GetById(Convert.ToInt32(orderVM.Id));
                items = Mapper.Map<List<OrderItem>>(ocVM.Order.Items);

                //now push the updates
                order = Mapper.Map<Order>(orderVM);
                items = Mapper.Map<List<OrderItem>>(orderVM.Items);
            }
            else
            {
                order = Mapper.Map<Order>(orderVM);
                items = Mapper.Map<List<OrderItem>>(orderVM.Items);
            }

            order.OriginRegion = GetOriginRegion(order.OriginLocation);
            order.Items = items;

            if (orderVM.Id != null)
            {
                //need to set the same ID on the items
                foreach (OrderItem oi in order.Items)
                {
                    oi.OrderId = order.Id;
                }
            }

            return _orderRepo.Create(order).Id;

        }


        public string CheckWeightWarning(OrderItemVM[] items)
        {
            return CheckThresholds(items);
        }

        #region Private Methods

        private ValidationResult ValidateNotoficationNumber(string notificationNumber)
        {
            var validationResult = new ValidationResult { HasErrors = false };
            if (notificationNumber.Length < 12)
            {
                validationResult.Fields.Add(new Field { Name = "Notification Number", Message = "Must be at least 12 characters." });
                return validationResult;
            }

            if (!_orderRepo.NotificationNumberExists(notificationNumber))
            {
                validationResult.Fields.Add(new Field { Name = "Notification Number", Message = "The notification number you supplied doesn't exist.", ValidationType = ValidationType.Warning });
                return validationResult;
            }

            return validationResult;
        }

        private ValidationResult ValidateDate(ValidationResult validationResult, string date)
        {
            if (validationResult == null) validationResult = new ValidationResult();
            DateTime dateResult;
            var field = "Delivery Date";
            if (!DateTime.TryParseExact(date, "dd'.'MM'.'yyyy",
                           CultureInfo.InvariantCulture,
                           DateTimeStyles.None,
                           out dateResult))
            {
                validationResult.Fields.Add(new Field { Name = field, Message = "The date you provided is invalid." });
            }
            else
            {
                if (dateResult < DateTime.Now.Date)
                {
                    validationResult.Fields.Add(new Field { Name = field, Message = "The delivery date is in the past.", ValidationType = ValidationType.Warning });
                }
            }

            return validationResult;
        }

        private ValidationResult ValidatePONumber(ValidationResult validationResult, string p)
        {
            if (validationResult == null) validationResult = new ValidationResult();

            if (p.Length < 10)
            {
                validationResult.Fields.Add(new Field { Name = "PO Number", Message = "Purchase Order Number must be at least 10 characters." });
            }
            decimal decimalTryParse = 0;
            if (!decimal.TryParse(p, out decimalTryParse))
            {
                validationResult.Fields.Add(new Field { Name = "PO Number", Message = "Purchase Order must be a number." });
            }

            return validationResult;
        }

        private string CheckThresholds(OrderItemVM[] items)
        {
            var thresholds = _orderRepo.GetDeliveryThresholds("Other");

            foreach (var item in items)
            {
                if (_ows.GetWeightFromItem(item.Weight) > thresholds.MaxGrossWeight)
                {
                    return thresholds.WarningMessage;

                }

                var dimensions = _ows.GetDimensions(item.Dim);
                if (dimensions.Sum() > thresholds.MaxCombinedDimensions)
                {
                    return thresholds.WarningMessage;

                }

                foreach (var dimension in dimensions)
                {
                    if (dimension > thresholds.MaxSingleDimension)
                    {
                        return thresholds.WarningMessage;
                    }
                }
            }

            return string.Empty;
        }

        #endregion

        public IList<DropPoint> SearchDP(string query)
        {
            return _orderRepo.SearchDP(query);
        }

        public IList<CustomerSiteVM> SearchCS(string query)
        {
            return Mapper.Map<IList<CustomerSiteVM>>(_orderRepo.SearchCS(query));
        }

        public IList<LeadTimeCostFormula> GetGenericServices()
        {
            return _orderRepo.GetGenericServices();
        }
    }

    class OriginLocationMapping
    {
        public string DocType { get; set; }
        public string SupplyingSloc { get; set; }
        public string Name { get; set; }
    }


}
