﻿using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using AutoMapper;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class MembershipService
    {
        private IMembershipRoleRepository _membershipRepo;

        public MembershipService(IMembershipRoleRepository membershipRepo)
        {
            _membershipRepo = membershipRepo;
        }

        public AspNetUserVM GetUserByLoginName(string loginName)
        {
            return _membershipRepo.GetUserByLoginName(loginName);
        }

        public int Create(AspNetUserVM aspnetuserVM)
        {

            var aspnetuser = Mapper.Map<AspNetUser>(aspnetuserVM);
           
          
            //var UserId = _membershipRepo.Create(aspnetuser).UserId;

             return _membershipRepo.Create(aspnetuser).UserId;


        }
       

    }
}
