﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class SchenkerService
    {
        private ISchenkerStatusUpdateRepository _schenkerRepo;
        private EmailService _emailService;
        private IOrderRepository _ordRepo;

        public SchenkerService(ISchenkerStatusUpdateRepository schenkerRepo, IEmployeeRepository empRepo, IOrderRepository ordRepo)
        {
            _schenkerRepo = schenkerRepo;
            _ordRepo = ordRepo;
            _emailService = new EmailService(empRepo, ordRepo);

        }

        public void LogOrderStatusUpdate(SchenkerStatusUpdate orderUpdate)
        {
            //convert from 00010 to 10 (for example)
            int intItemNo = 0;
            if (int.TryParse(orderUpdate.ItemNo, out intItemNo))
                orderUpdate.ItemNo = intItemNo.ToString();

            _schenkerRepo.Insert(orderUpdate);

        }

        public void EmailNotify(SchenkerStatusUpdate orderUpdate)
        {
            if (orderUpdate.EventCode == "SHP")
            {
                //if all line items have been SHP'ed
                if (_ordRepo.CheckAllOrderItems(orderUpdate.PODocNumber, orderUpdate.EventCode))
                {
                    _emailService.SendEmail("email_eta", "ORD#{{PurchaseOrder}} SERV#{{JobNumber}}", orderUpdate);
                }
            }
            else if (orderUpdate.EventCode == "DEL")
            {
                //if all line items have been DEL'ed
                if (_ordRepo.CheckAllOrderItems(orderUpdate.PODocNumber, orderUpdate.EventCode))
                {
                    _emailService.SendEmail("email_delivery", "AWS Delivery", orderUpdate);
                }
            }
        }


        public void StatusUpdate(int orderId, string serviceLevelCode, string serviceLevelDescription, string xml, string response, string Warehousepriority, string Cost)
        {
            //after submitting XML to Schenker - this is updated within the order table
            _schenkerRepo.UpdateSchenkerStatus(orderId, serviceLevelCode, serviceLevelDescription, xml, response, Warehousepriority, Cost);
        }

        public string GetOrderXml(string orderTemplate, string orderLinesTemplate, int orderId, string serviceLevelCode, string warehousePriority, string siemensContactName, string siemensContactPhone, string userFName, string userLName, string userEmail, string Cost)
        {

            var soVM = _schenkerRepo.GetOrderDetails(serviceLevelCode, orderId, warehousePriority, siemensContactName, siemensContactPhone, userFName, userLName, userEmail, Cost);

            var order = orderTemplate;
            var now = DateTime.Now.ToString("ddMMyyyyHHmmss");
            order = order.Replace("{{poNum}}", soVM.STOPONumber);
            order = order.Replace("{{now}}", now);
            order = order.Replace("{{delDate}}", soVM.ExpectedDeliveryDate);
            order = order.Replace("{{custSite}}", soVM.DeliveryAddressName1);
            order = order.Replace("{{delAddName2}}", soVM.DeliveryAddressName2);
            order = order.Replace("{{delAddName3}}", soVM.DeliveryAddressName3);
            // order = order.Replace("{{delAddName5}}", soVM.DeliveryAddressName5);
            order = order.Replace("{{delStreet}}", soVM.DeliveryAddressHouseNumber);
            order = order.Replace("{{delCity}}", soVM.CityInShipToParty);
            if (soVM.PostalCode.Length == 3)
                order = order.Replace("{{postCode}}", soVM.PostalCode.PadLeft(4,'0'));
            else
                order = order.Replace("{{postCode}}", soVM.PostalCode);
            order = order.Replace("{{region}}", soVM.Region);
            order = order.Replace("{{uFName}}", soVM.UserFirstName);
            order = order.Replace("{{uLName}}", soVM.UserLastName);
            order = order.Replace("{{uEMail}}", userEmail);
            order = order.Replace("{{notNum}}", soVM.NotificationNumber);
            order = order.Replace("{{delTime}}", soVM.DeliveryTime);
            order = order.Replace("{{serviceLevel}}", soVM.ServiceLevelCode.PadLeft(2, '0'));   //eg. 03
            order = order.Replace("{{delType}}", soVM.DeliveryType);
            order = order.Replace("{{scName}}", soVM.SiteContactName);
            order = order.Replace("{{scPhone}}", soVM.SiteContactPhone);
            order = order.Replace("{{siemensContactName}}", soVM.SiemensContactName);
            order = order.Replace("{{siemensContactPhone}}", soVM.SiemensContactPhone);

            order = order.Replace("{{splRmk}}", soVM.SpecialDeliveryRemarks);

            if (soVM.WarehousePriority == "True")
                order = order.Replace("{{warehousePriority}}", "YES");
            else
                order = order.Replace("{{warehousePriority}}", "NO");

            //order = order.Replace("{{orderType}}", soVM.orderType);

            var orderLines = "";

            foreach (var item in soVM.OrderItems)
            {
                var orderLine = orderLinesTemplate;
                orderLine = orderLine.Replace("{{lineNum}}", item.STOPOLineNumber.PadLeft(6, '0'));
                orderLine = orderLine.Replace("{{matNum}}", item.SAPMaterialNumber);
                orderLine = orderLine.Replace("{{matDesc}}", item.MaterialDescription);
                orderLine = orderLine.Replace("{{plant}}", item.Plant);
                orderLine = orderLine.Replace("{{sLoc}}", item.StorageLocation);
                orderLine = orderLine.Replace("{{qty}}", item.OrderQty);
                orderLine = orderLine.Replace("{{uom}}", item.UOM);
                orderLine = orderLine.Replace("{{batch}}", item.BatchNumber);
                orderLines += orderLine;
            }

            order = order.Replace("{{orderLines}}", orderLines);
            return order;
        }

        public void UpdateOrderItem(SchenkerStatusUpdate orderUpdate)
        {
            //for the chosen Schenker item - need to update
            //_schenkerRepo.UpdateOrderItem(orderUpdate.PODocNumber, 
            //                                   orderUpdate.ItemNo,
            //                                   orderUpdate.ConNote,
            //                                   orderUpdate.EventCode, 
            //                                   orderUpdate.MessageDateTime);
            //change the values that is saving in the order item table from message datetime to actual event date time
            _schenkerRepo.UpdateOrderItem(orderUpdate.PODocNumber,
                                               orderUpdate.ItemNo,
                                               orderUpdate.ConNote,
                                               orderUpdate.EventCode,
                                               orderUpdate.EventDateTime);

        }

        public SchenkerStatusUpdate PushOrdertoZapier()
        {
            return _schenkerRepo.PushtoZapierOrder();
        }

        public string GetOrderforZapier(string ZapierData)
        {

            var soVM = _schenkerRepo.PushtoZapierOrder();
            var order = ZapierData;
            
            var EventDateTime = soVM.EventDateTime.ToString();
            var MessageDateTime = soVM.MessageDateTime.ToString();

            order = order.Replace("{{poNum}}", soVM.PODocNumber);
            order = order.Replace("{{ItemNo}}", soVM.ItemNo);
            order = order.Replace("{{EventCode}}", soVM.EventCode);
            order = order.Replace("{{EventDateTime}}", EventDateTime);
            order = order.Replace("{{ReasonCode}}", soVM.ReasonCode);
            order = order.Replace("{{EventComment}}", soVM.EventComment);
            order = order.Replace("{{MessageDateTime}}", MessageDateTime);            
            order = order.Replace("{{Connote}}", soVM.ConNote);
            order = order.Replace("{{CarrierCode}}", soVM.CarrierCode);
            order = order.Replace("{{EventLocation}}", soVM.EventLocation);
            order = order.Replace("{{TrackingURL}}", soVM.TrackingURL);
           
            return order;
        }

        public OrderVM GetOrderDetailsZapier(string PONumber, string POLine)
        {
            return _schenkerRepo.GetOrderDetailsZapier(PONumber, POLine);
        }        
        public ZMON GetDetailsZMON(string NotificationNumber)
        {
            return _schenkerRepo.GetDetailsZMON(NotificationNumber);
        }
        public IBase GetIBASEDetails(string Requisitioner)
        {
            return _schenkerRepo.GetIBASEDetails(Requisitioner);
        }
        public string GetOrderJson(string orderTemplate, string orderLinesTemplate, int orderId, string serviceLevelCode, string warehousePriority, string siemensContactName, string siemensContactPhone, string userFName, string userLName, string userEmail, string Cost)
        {

            var soVM = _schenkerRepo.GetOrderDetails(serviceLevelCode, orderId, warehousePriority, siemensContactName, siemensContactPhone, userFName, userLName, userEmail, Cost);

            var order = orderTemplate;
            var now = DateTime.Now.ToString("ddMMyyyyHHmmss");
            order = order.Replace("{{poNum}}", soVM.STOPONumber);
            order = order.Replace("{{now}}", now);
            order = order.Replace("{{delDate}}", soVM.ExpectedDeliveryDate);
            order = order.Replace("{{custSite}}", soVM.DeliveryAddressName1);
            order = order.Replace("{{delAddName2}}", soVM.DeliveryAddressName2);
            order = order.Replace("{{delAddName3}}", soVM.DeliveryAddressName3);
            order = order.Replace("{{delStreet}}", soVM.DeliveryAddressHouseNumber);
            order = order.Replace("{{delCity}}", soVM.CityInShipToParty);
            if (soVM.PostalCode.Length == 3)
                order = order.Replace("{{postCode}}", soVM.PostalCode.PadLeft(4, '0'));
            else
                order = order.Replace("{{postCode}}", soVM.PostalCode);
            order = order.Replace("{{region}}", soVM.Region);
            order = order.Replace("{{uFName}}", soVM.UserFirstName);
            order = order.Replace("{{uLName}}", soVM.UserLastName);
            order = order.Replace("{{uEMail}}", userEmail);
            order = order.Replace("{{notNum}}", soVM.NotificationNumber);
            order = order.Replace("{{delTime}}", soVM.DeliveryTime);
            order = order.Replace("{{serviceLevel}}", soVM.ServiceLevelCode.PadLeft(2, '0'));   //eg. 03
            order = order.Replace("{{delType}}", soVM.DeliveryType);
            order = order.Replace("{{scName}}", soVM.SiteContactName);
            order = order.Replace("{{scPhone}}", soVM.SiteContactPhone);
            order = order.Replace("{{siemensContactName}}", soVM.SiemensContactName);
            order = order.Replace("{{siemensContactPhone}}", soVM.SiemensContactPhone);

            order = order.Replace("{{splRmk}}", soVM.SpecialDeliveryRemarks);

            if (soVM.WarehousePriority == "True")
                order = order.Replace("{{warehousePriority}}", "YES");
            else
                order = order.Replace("{{warehousePriority}}", "NO");

            var orderLines = "";

            foreach (var item in soVM.OrderItems)
            {
                var orderLine = orderLinesTemplate;
                orderLine = orderLine.Replace("{{lineNum}}", item.STOPOLineNumber.PadLeft(6, '0'));
                orderLine = orderLine.Replace("{{matNum}}", item.SAPMaterialNumber);
                orderLine = orderLine.Replace("{{matDesc}}", item.MaterialDescription);
                orderLine = orderLine.Replace("{{plant}}", item.Plant);
                orderLine = orderLine.Replace("{{sLoc}}", item.StorageLocation);
                orderLine = orderLine.Replace("{{qty}}", item.OrderQty);
                orderLine = orderLine.Replace("{{uom}}", item.UOM);
                orderLine = orderLine.Replace("{{batch}}", item.BatchNumber);
                orderLines += orderLine;
            }
            order = order.Replace("{{orderLines}}", orderLines);
            return order;
        }

    }
}
