﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using AutoMapper;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.Services
{
    public class EmployeeService
    {
        private IEmployeeRepository _empRepo;

        public EmployeeService(IEmployeeRepository empRepo)
        {

            _empRepo = empRepo;
        }
        public IList<EmployeeVM> GetActiveEmployees()
        {
            var emps = _empRepo.GetAll;
            return Mapper.Map<IList<Employee>,IList<EmployeeVM>>(emps);
        }
        public EmployeeVM GetById(string id)
        {
            return Mapper.Map<EmployeeVM>(_empRepo.GetById(id));
        }

        public IList<EmployeeVM> SearchEmp(string query)
        {
            var employees = _empRepo.SearchEA(query);
            return Mapper.Map<IList<EmployeeVM>>(employees);
        }
    }

    /*
        <option value="1025">1025 - CROWDEN, ROBERT</option>
        <option value="1026">1026 - DAVIS, GRAHAM</option>
        <option value="1028">1028 - DEMASI, GEORGE</option>
        <option value="1031">1031 - GROSCHE, KARL</option>
        <option value="1032">1032 - HAMPTON, SHAUN</option>
        <option value="1033">1033 - HART, BRIAN</option>
        <option value="1034">1034 - HAYCOCK, KERRY</option>
        <option value="1036">1036 - JOHNSTONE, COLIN</option>
        <option value="1037">1037 - KEULEMANS, MARC</option>
        <option value="1041">1041 - MULLER, PETER</option>
        <option value="1045">1045 - RAINES, WARREN</option>
        <option value="1047">1047 - SCHMIDT, LEO</option>
        <option value="1049">1049 - SPATARO, ORAZIO</option>
         
    */
}
