﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;

namespace Artis.Siemens.HealthCareCloudServices.Services.Mappings
{
    class MaterialMasterProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<MaterialMaster, OrderItemVM>()
                .ForMember(dest => dest.Material, src => src.MapFrom(s => s.Id))
                .ForMember(dest => dest.Description, src => src.MapFrom(s => s.MaterialDescription))
                .ForMember(dest => dest.UOM, src => src.MapFrom(s => s.UnitOfMeasure))
                .ForMember(dest => dest.Batch, src => src.Ignore())
                .ForMember(dest => dest.Item, src => src.Ignore())
                .ForMember(dest => dest.Qty, src => src.Ignore())
                .ForMember(dest => dest.Weight, src => src.MapFrom(s => s.GrossWeight.Trim() + " " + s.WeightUOM))
                .ForMember(dest => dest.Dim, src => src.ResolveUsing<DimResolver>())
                .ForMember(dest => dest.DG, src => src.ResolveUsing<DGResolver>());

            Mapper.CreateMap<IBase, CustomerSiteVM>()
                .ForMember(dest => dest.ContactName, src => src.MapFrom(s => s.NameCo))
                .ForMember(dest => dest.ContactPhone, src => src.MapFrom(s => s.Tel))
                .ForMember(dest => dest.DeliveryRemarks, src => src.MapFrom(s => s.Comments));
        }

        class DimResolver : ValueResolver<MaterialMaster, string>
        {
            protected override string ResolveCore(MaterialMaster source)
            {
                return string.Format("{0}x{1}x{2} {3}", source.Length.Trim(), source.Width.Trim(), source.Height.Trim(), source.DimensionUOM);
            }
        }

        class DGResolver : ValueResolver<MaterialMaster, string>
        {
            protected override string ResolveCore(MaterialMaster source)
            {
                return string.IsNullOrEmpty(source.DangerousGoods) ? "NO" : "YES";
            }
        }
    }
}
