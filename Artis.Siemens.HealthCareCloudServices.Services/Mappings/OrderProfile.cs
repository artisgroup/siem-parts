﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;

namespace Artis.Siemens.HealthCareCloudServices.Services.Mappings
{
    class OrderProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<OrderVM, Order>()
                .ForMember(dest => dest.NotificationNumber, src => src.MapFrom(s => s.Requisitioner))
                .ForMember(dest => dest.PONumber, src => src.MapFrom(s => s.PurchasingDoc))
                .ForMember(dest => dest.ReceiverId, src => src.MapFrom(s => s.ReceivingSloc))
                .ForMember(dest => dest.DeliveryDate, src => src.ResolveUsing<DateResolver>())
                .ForMember(s => s.Items, d => d.Ignore())
                .ReverseMap()
                .ForMember(dest => dest.Requisitioner, src => src.MapFrom(s => s.NotificationNumber))
                .ForMember(dest => dest.PurchasingDoc, src => src.MapFrom(s => s.PONumber))
                .ForMember(dest => dest.ReceivingSloc, src => src.MapFrom(s => s.ReceiverId))
                .ForMember(s => s.Items, d => d.Ignore())
                .ForMember(s => s.DeliveryDate, src => src.MapFrom(s => s.DeliveryDate.ToString("dd/MM/yyyy")))
                .ForMember(s => s.DeliveryTime, src => src.MapFrom(s => s.DeliveryDate.ToString("HH:mm")))
                ;

            Mapper.CreateMap<OrderItemVM, OrderItem>()
                .ForMember(dest => dest.MaterialNo, src => src.MapFrom(s => s.Material))
                .ForMember(dest => dest.RowNo, src => src.MapFrom(s => s.Item))
                .ForMember(dest => dest.UOM, src => src.MapFrom(s => s.UOM))
                .ReverseMap()
                .ForMember(dest => dest.Material, src => src.MapFrom(s => s.MaterialNo))
                .ForMember(dest => dest.Item, src => src.MapFrom(s => s.RowNo));

            ConfigureOthers();
        }

        private void ConfigureOthers()
        {
            Mapper.CreateMap<ZMON, ZMonVM>().ReverseMap();
            Mapper.CreateMap<LeadTimeCostFormula, LeadTimeCostFormulaVM>().ReverseMap();
            Mapper.CreateMap<InternationalFlightsCutoff, InternationalFlightsVM>().ReverseMap();
        }

        class DateResolver : ValueResolver<OrderVM, DateTime>
        {
            protected override DateTime ResolveCore(OrderVM source)
            {
                return Convert.ToDateTime(string.Format("{0} {1}", source.DeliveryDate, source.DeliveryTime), new CultureInfo("en-AU"));
            }
        }

    }


}
