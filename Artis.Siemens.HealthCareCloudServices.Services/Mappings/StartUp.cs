﻿using WebActivatorEx;
using Artis.Siemens.HealthCareCloudServices.Services.Mappings;
[assembly: PreApplicationStartMethod(typeof(StartUp), "CreateMappings")]

namespace Artis.Siemens.HealthCareCloudServices.Services.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMapper;


    class StartUp
    {
        static void CreateMappings()
        {

            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new EmployeeProfile());
                cfg.AddProfile(new MaterialMasterProfile());
                cfg.AddProfile(new OrderProfile());
                cfg.AddProfile(new MembershipProfile());
            }
            );
        }
    }
}
