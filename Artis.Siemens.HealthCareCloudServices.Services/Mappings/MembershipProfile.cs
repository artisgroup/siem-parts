﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;


namespace Artis.Siemens.HealthCareCloudServices.Services.Mappings
{
    class MembershipProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AspNetRole, AspNetRoleVM>().ReverseMap();
            Mapper.CreateMap<AspNetUser, AspNetUserVM>().ReverseMap();
            
        }
    }
}
