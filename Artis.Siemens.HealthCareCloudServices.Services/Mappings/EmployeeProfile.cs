﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Artis.Siemens.HealthCareCloudServices.Services.Mappings
{
    class EmployeeProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Employee, EmployeeVM>().ForMember(dest => dest.Surname, src => src.MapFrom(s => s.LastName)).ReverseMap();
        }
    }

    
}
