﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.EF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.Web.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestFlights();
            //TestLastVisit();
            //TestNextVisit();
            //TestAvailableServices();
            TestSearchCS();
        }


        private static void TestFlights()
        {
            OrderRepository repo=new OrderRepository();

            List<InternationalFlightsCutoff> flights=repo.GetFlights("singapore", "tas", "3195");

            int count=flights.Count;
        }


        private static void TestLastVisit()
        {
            OrderRepository repo = new OrderRepository();

            DateTime? lastVist = repo.GetLastVist("728103086870");
        }

        private static void TestNextVisit()
        {
            OrderRepository repo = new OrderRepository();

            DateTime? nextVist = repo.GetNextVist("728103086870");
        }


        private static void TestAvailableServices()
        {
            OrderRepository repo = new OrderRepository();

            //List<string> services = repo.GetAvailableServices("VIC", "3195");
        }


        private static void TestSearchCS()
        {
            IOrderRepository repo = new OrderRepository();

            IList<IBase> ibases = repo.SearchCS("80");
            int a = 1;
        }

    }
}
