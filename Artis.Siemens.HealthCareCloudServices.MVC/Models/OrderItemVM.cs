﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Models
{
    public class OrderItemVM
    {
        public string Item { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public string Batch { get; set; }
        public string Qty { get; set; }
        public string Weight { get; set; }
        public string Dim { get; set; }
        public string DG { get; set; }
    }
}