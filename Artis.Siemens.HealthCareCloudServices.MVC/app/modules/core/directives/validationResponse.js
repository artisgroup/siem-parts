﻿angular.module('SpareParts.Core').directive('validationResponse', validationResponse);

function validationResponse() {
    return {
        scope: {
            type: '@',
            message: '@',
            item:'='
        },
        template: [
            '<span ng-class="{\'0\':\'text-danger\', \'1\': \'text-warning\'}[item.ValidationType]">',
                '<i class="fa" ng-class="{\'0\': \'fa-times\', \'1\': \'fa-exclamation-triangle\'}[item.ValidationType]"></i>&nbsp;{{item.Message}}',
            '</span>'
        ].join('')
    };
}