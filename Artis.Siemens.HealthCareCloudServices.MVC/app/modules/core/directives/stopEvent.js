﻿angular.module('SpareParts.Core').directive('stopEvent', stopEvent);

function stopEvent() {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('click', function (e) {
                $(this).blur();
                e.stopPropagation();
                e.preventDefault();
            });
        }
    };
}