﻿// This is an extension of ng-click to allow it to be used on elements like spans and div when they have focus and the user presses
// the enter key.
angular.module('SpareParts.Core').directive('linkAction', linkAction);

linkAction.$inject = ['$parse'];

function linkAction($parse) {
    return function (scope, element, attr) {
        //grabbing the function from the attributes and parsing it (to get parameters I believe, this taken from the code above.
        var fn = $parse(attr['linkAction']);

        //making the handler so it can be bound to different events without repeating again taken from source above
        var handler = function (event) {
            scope.$apply(function () {
                fn(scope, { $event: event });
            }
          )
        };

        //If clicked calling the handler
        element.bind('click', handler);
        //Checking first that it's the enter key "13" then calling the handler
        element.bind('keyup', function (event) { if (event.keyCode === 13) handler(event) });
    }
}