﻿angular.module('SpareParts.Core').service('orderEntryService', orderEntryService);

orderEntryService.$inject = ['Restangular'];

function orderEntryService(Restangular) {

    var _oeEndPoint = Restangular.all('orderentry');
    var _ocEndPoint = Restangular.all('orderconfirm');

    return {
        validateOrderHeader: _validateOrderHeader,
        submitSAPOrders: _submitSAPOrders,
        getOriginLocation: _getOriginLocation,
        getCustomerSite: _getCustomerSite,
        getDropPoint: _getDropPoint,
        getEmployeeById: _getEmployeeById,
        getEmployeeBySloc: _getEmployeeBySloc,
        searchMaterial: _searchMaterial,
        create: _create,
        checkWeightWarning: _checkWeightWarning,
        searchCS: _searchCS,
        searchDP: _searchDP,
        Header: {},
        Items: {},
        DeliveryAddress: {},
        getByOrderId: _getByOrderId,

    };

    function _searchCS(query) {
        return _oeEndPoint.customGET('searchCS?query=' + query).then(
            function (data) {
                return _transformCSResults(data);
            });
    }

    function _getByOrderId(id) {
        return _ocEndPoint.customGET('getOrderDetail?orderId=' + id);
    }

    function _transformCSResults(results) {
        if (results.length === 0) {
            return [{ NoResults: true }];
        }
        return results;
    }


    function _searchDP(query) {
        return _oeEndPoint.customGET('searchDP?query=' + query).then(
            function (data) {
                return _transformDPResults(data);
            });
    }

    function _transformDPResults(results) {
        if (results.length === 0) {
            return [{ NoResults: true }];
        }

        return results;
    }

    function _checkWeightWarning(items) {
        return _oeEndPoint.customPOST(items, 'checkWeightWarning');
    }

    function _create(order) {
        return _oeEndPoint.customPOST(order, 'create');
    }

    function _searchMaterial(materialNumber) {
        return _oeEndPoint.customGET('searchByMaterialNumber?number=' + materialNumber);
    }

    function _validateOrderHeader(postData) {
        return _oeEndPoint.customPOST(postData, 'validateOrderHeader');
    }

    function _submitSAPOrders(postData) {
        var retVal = _oeEndPoint.customPOST(postData, 'submitSAPOrders');
        return retVal;
    }

    function _getOriginLocation(postData) {
        return _oeEndPoint.customPOST(postData, 'getOriginLocation');
    }

    function _getCustomerSite(notificationNumber) {
        return _oeEndPoint.customGET('getCustomerSite?notificationNumber=' + notificationNumber);
    }

    function _getDropPoint(notificationNumber) {
        return _oeEndPoint.customGET('getDropPoint?notificationNumber=' + notificationNumber);
    }

    function _getEmployeeById(empNo) {
        return _oeEndPoint.customGET('getEmployeeById?empNo=' + empNo);
    }
    function _getEmployeeBySloc(sloc) {
        return _oeEndPoint.customGET('getEmployeeBySloc?sloc=' + sloc);
    }
}