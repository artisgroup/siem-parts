﻿angular.module('SpareParts.Core').service('empService', empService);

empService.$inject = ['Restangular'];

function empService(Restangular) {
    var _empEndPoint = Restangular.all('employee');

    return {
        getList: _getList,
        getById: _getById,
        searchEA: _searchEA
    };

    function _getById(id) {
        return _empEndPoint.customGET('getById?id=' + id);
    }

    function _getList() {
        return _empEndPoint.customGET('getList');
    }

    function _searchEA(query) {
        return _empEndPoint.customGET('searchEA?query=' + query).then(
            function (data) {
                return _transformEAResults(data);
            });
    }

    function _transformEAResults(results) {
        if (results.length === 0) {
            return [{ NoResults: true }];
        }

        return results;
    }


}