﻿angular.module('SpareParts.OrderEntry', ['ngSanitize', 'SpareParts.Core', 'ngAnimate', 'restangular', 'ui.directives', 'ui.bootstrap']);

angular.module('SpareParts.OrderEntry').run(['$rootScope', '$log', 'Restangular', function ($rootScope, $log, Restangular) {
    Restangular.setBaseUrl(appBase  + 'api/');

    //Restangular.setErrorInterceptor(
    //    function (response, deferred) {
    //        $log.log('RESPONSE');
    //        $log.log(response);
    //        if (response.status === 500) {
    //            //$rootScope.createToast("error...", "danger", false);
    //            //ngToast.create('a toast message...');
    //            deferred.reject;
    //            return true;
    //        }
    //    }
    //);

    //Restangular.addResponseInterceptor(
    //    function (data) {
    //        $log.log('RESPONSE');
    //        $log.log(data);
    //        return data;
    //    }
    //);


    //$rootScope.createToast = function (message, type, dismissOnTimeout) {

    //    if (angular.isUndefined(dismissOnTimeout) || dismissOnTimeout == null || dismissOnTimeout == "") {
    //        dismissOnTimeout = true;
    //        $rootScope.dismissToast();
    //    }
    //    //ngToast.create('a toast message...');
    //    //ngToast.create({
    //    //    content: message,
    //    //    className: type,
    //    //    dismissButton: true,
    //    //    dismissOnTimeout: dismissOnTimeout,
    //    //    dismissOnClick: false
    //    //});
    //};

    //$rootScope.dismissToast = function () {
    //    ngToast.dismiss();
    //};
}]);

//angular.module("SpareParts.OrderEntry").config([
//    'ngToastProvider', function (ngToast) {
//        ngToast.configure({
//            animation: "fade"
//        });
//    }
//]);