﻿angular.module('SpareParts.OrderEntry').controller('OrderCtrl', OrderCtrl);

OrderCtrl.$inject = ['$log', '$q', '$scope', 'orderEntryService', 'empService'];

function OrderCtrl($log, $q, $scope, orderEntryService, empService) {
    var ord = this;
    ord.reLoad = false;

    var SAPOrderObj = {
        DocType: '',
        PurchasingDoc: '',
        Item: '',
        Material: '',
        Batch: '',
        Quantity: '',
        OrderUnit: '',
        SupplyingSloc: '',
        ReceivingSloc: '',
        DeliveryDate: '',
        DeliveryTime: '',
        Requisitioner: '',
        //additional fields
        Name1: '',  //Hospital Name
        Name2: '',  //Room and Department
        Name3: '',   //Site Contact Name
        Street: '',
        City: '',
        Postcode: '',        
        //Name4: '',   //Site Contact Number
        Street5: '' //Site Remarks CSE Contact Details
    };

    var OrderObj = {
        Id: null
    }


    setUp();

    ord.showErrorDiv = function (fieldName) {
        var shouldShow = _.find(ord.ValidationResult.Fields, function (field) {
            return field.Name === fieldName;
        });
        return shouldShow !== undefined;
    };

    ord.parseSAPOrder = function (skipInputCheck) {
        if (!skipInputCheck) {
            if (!validateInput()) return;
            if (!validateOrdersUnique()) return;
        }
        ord.SAPOrderValid = true;
        populateOrderHeader();

    };

    ord.ReSubmit = function () {
        $('#waitModal').modal('show');
        //var orderObj = {};
        //angular.copy(ord.SAPOrderObj, orderObj);
        //orderObj.ReceivingSloc = ord.EmpId;
        //angular.copy(orderObj, ord.SAPOrderObj);

        for (var i = 0; i < ord.Orders.length; i++) {
            var order = ord.Orders[i];
            order.ReceivingSloc = ord.SAPOrderObj.ReceivingSloc;
            order.DeliveryDate = moment(ord.SAPOrderObj.DeliveryDate, 'DD/MM/YYYY').format('DD.MM.YYYY');
            order.DeliveryTime = ord.SAPOrderObj.DeliveryTime;
            order.Requisitioner = ord.SAPOrderObj.Requisitioner;
            order.PurchasingDoc = ord.SAPOrderObj.PurchasingDoc;
            order.SupplyingSloc = ord.SAPOrderObj.SupplyingSloc;
        }
        ord.parseSAPOrder(true);
    };

    ord.getOrderDetails = function (orderId) {
        if (orderId) {
            ord.reLoad = true;
            ord.Id = orderId;

            var SAPOrderObj = {
                DocType: '',
                PurchasingDoc: '',
                Item: '',
                Material: '',
                Batch: '',
                Quantity: '',
                OrderUnit: '',
                SupplyingSloc: '',
                ReceivingSloc: '',
                DeliveryDate: '',
                DeliveryTime: '',
                Requisitioner: '',
                //additional fields
                Name1: '',  //Hospital Name
                Name2: '',  //Room and Department
                //Name2: '',  //Room and Department
                Name3: '',   //Site Contact Name
                Street: '',
                City: '',
                Postcode: '',
                
                ContactPhone: '',   //Site Contact Number
                Street5: '' //Site Remarks CSE Contact Details
            };

            orderEntryService.getByOrderId(ord.Id).then(function (data) {

                ord.HeaderValidated = false;
                ord.GetDeliveryInfo = false;

                //set values for header, etc
                ord.SAPOrderObj.Id = ord.Id;
                ord.SAPOrderObj.Requisitioner = data.Order.Requisitioner;   //notification
                ord.SAPOrderObj.PurchasingDoc = data.Order.PurchasingDoc;   //purchase order number

                ord.SAPOrderObj.DeliveryType = data.Order.DeliveryType;
                ord.SAPOrderObj.OriginLocation = data.Order.OriginLocation;
                ord.SAPOrderObj.DeliveryDate = data.Order.DeliveryDate;
                ord.SAPOrderObj.ReceivingSloc = data.Employee.Sloc;

                ord.SAPOrderObj.DeliveryTime = data.Order.DeliveryTime;

                ord.SAPOrderObj.DocType = data.Order.DocType;
                ord.SAPOrderObj.SupplyingSloc = data.Order.SLoc;

                ord.SAPOrderObj.Items = data.Order.Items;
                //for additional items
                ord.SAPOrderObj.Name1 = data.Order.CustomerSite;
                ord.SAPOrderObj.Name2 = data.Order.CustomerRoom;
                ord.SAPOrderObj.Name3 = data.Order.CustomerSiteContactName;
                ord.SAPOrderObj.Street = data.Order.Street;
                ord.SAPOrderObj.City = data.Order.City;
                ord.SAPOrderObj.Postcode = data.Order.PostCode;
                ord.SAPOrderObj.ContactPhone = data.Order.CustomerSiteContactPhone;
                ord.SAPOrderObj.Street5 = data.Order.CustomerSiteDeliveryRemarks;
                
                ord.Order = angular.copy(ord.SAPOrderObj);

                if (data.Order.TailGate == "1")
                    ord.chkTailGate = true;

                //reverse field values from ORDER (saved)
                if (data.Order.EngineerAddressId) {
                    ord.DeliveryChoice = 1;
                    ord.Name1 = data.Order.CustomerSite;
                    ord.Sloc = data.Order.EngineerAddressId;
                    ord.FullName = data.Order.CustomerSite;
                    ord.Street = data.Order.Street;
                    ord.City = data.Order.City;
                    ord.DestRegion = data.Order.DestRegion;
                    ord.DestPostCode = data.Order.DestPostCode;
                }
                else if (data.Order.DropPointId) {
                    ord.DeliveryChoice = 2;
                    ord.Name1 = data.Order.CustomerSite;
                    ord.Name2 = data.Order.Name2;
                    ord.Suburb = data.Order.City;
                    ord.DropPointId = data.Order.DropPointId;
                    ord.Street = data.Order.Street;
                    ord.Region = data.Order.DestRegion;
                    ord.PostCode = data.Order.DestPostCode;
                }
                else {
                    ord.DeliveryChoice = 3;
                    //ord.Name1 = data.Order.CustomerSite;
                    //ord.Name2 = data.Order.CustomerRoom;
                    //ord.Name3 = data.Order.CustomerDepartment;
                    //ord.Street = data.Order.Street;
                    //ord.City = data.Order.City;
                    //ord.Country = data.Order.Country;
                    //ord.ContactName = data.Order.CustomerSiteContactName;
                    //ord.ContactPhone = data.Order.CustomerSiteContactPhone;
                    //ord.DeliveryRemarks = data.Order.CustomerSiteDeliveryRemarks;
                    //ord.Region = data.Order.DestRegion;
                    //ord.Region = data.Order.Region;
                    //ord.PostCode = data.Order.DestPostCode;
                    //ord.PostCode = data.Order.PostCode;
                    
                    ord.Name1 = data.Order.Name1;
                    ord.Name2 = data.Order.CustomerRoom;
                    ord.ContactName = data.Order.CustomerSiteContactName;
                    ord.Street = data.Order.Street;
                    ord.City = data.Order.City;
                    ord.Country = data.Order.Country;                    
                    ord.ContactPhone = data.Order.CustomerSiteContactPhone;
                    ord.DeliveryRemarks = data.Order.CustomerSiteDeliveryRemarks;

                    ord.Region = data.Order.DestRegion;
                    ord.Region = data.Order.Region;
                    ord.PostCode = data.Order.DestPostCode;
                    ord.PostCode = data.Order.Postcode;
                }

                ord.Orders.push(ord.Order);

                getPromises();
            });


        }
    }


    function populateOrderHeader() {
        var currValue = ord.SAPOrderObj.DeliveryType;
        angular.copy(ord.Orders[0], ord.SAPOrderObj);

        if (!ord.SAPOrderObj.DeliveryType) {
            if (currValue)
                ord.SAPOrderObj.DeliveryType = currValue;
            else
                ord.SAPOrderObj.DeliveryType = 'LB';
        }

        orderEntryService.validateOrderHeader(ord.SAPOrderObj).then(
            function (data) {
                ord.ValidationResult = data;
                var date = ord.SAPOrderObj.DeliveryDate.split('.');
                var time = ord.SAPOrderObj.DeliveryTime.split(':');
                ord.SAPOrderObj.DeliveryDate = moment(date[2] + date[1] + date[0], 'YYYYMMDD').format('DD.MM.YYYY');
                ord.SAPOrderObj.DeliveryTime = time[0] + ':' + time[1];
                getPromises();
            }
        );
    }


    function getPromises() {
        var originLocationPromise = orderEntryService.getOriginLocation(ord.SAPOrderObj);
        var employeesPromise = empService.getList();
        $q.all([originLocationPromise, employeesPromise]).then(
            function (data) {
                setTimeout(
                    function () {
                        ord.SAPOrderSubmitted = true;
                        var olValid = validateOriginLocation(data[0]);
                        var empValid = validateEmployee(data[1]);
                        ord.HeaderValidated =
                            !ord.ValidationResult.HasErrors
                            && olValid
                            && empValid;
                        ord.GetDeliveryInfo = ord.HeaderValidated;
                        $('#DeliveryDate').datepicker({
                            todayHighlight: true,
                            startDate: new Date(),
                            format: 'dd.mm.yyyy'
                        });

                        $('#waitModal').modal('hide');
                        $scope.$apply();
                    }
                    , 250);

            }
        );
    }

    function validateOriginLocation(ol) {
        ord.SAPOrderObj.OriginLocation = ol;
        if (!ol) {
            ord.ValidationResult.Fields = ord.ValidationResult.Fields || [];
            ord.ValidationResult.Fields.push({ Name: 'Origin Location', ValidationType: 0, Message: 'Origin Location could not be determined, please pick from list.' });
            return false;
        }
        return true;
    }

    function validateEmployee(empData) {
        ord.Employees = empData;
        ord.Sloc = ord.SAPOrderObj.ReceivingSloc;
        if (ord.SAPOrderObj.ReceivingSloc.length < 4 || !empIdInList()) {
            ord.ValidationResult.Fields = ord.ValidationResult.Fields || [];
            ord.ValidationResult.Fields.push({ Name: 'Receiver', ValidationType: 0, Message: 'Employee not found, please pick from list.' });
            return false;
        }
        return true;
    }

    function empIdInList() {
        return _.any(ord.Employees, { Sloc: ord.Sloc });
    }


    function getOriginLocation() {
        orderEntryService.getOriginLocation(ord.SAPOrderObj).then(
            function (data) {
                ord.SAPOrderObj.OriginLocation = data;
            }
        );
    }

    ord.closeInvalidOrderMsg = function () {
        ord.InvalidOrderMsg = '';
    };

    ord.setSloc = function () {
        switch (ord.SAPOrderObj.OriginLocation.toLowerCase()) {
            case '':
            case 'CSML - Frankfurt'.toLowerCase():
            case 'CSML - Singapore'.toLowerCase():
                ord.SAPOrderObj.SupplyingSloc = '';
                break;
            case 'Schenker - Melbourne - 2011'.toLowerCase():
                ord.SAPOrderObj.SupplyingSloc = '2011';
                ord.ReSubmit();
                break;
            case 'Schenker - Sydney - 2012'.toLowerCase():
                ord.SAPOrderObj.SupplyingSloc = '2012';
                ord.ReSubmit();
                break;
            case 'Schenker - Brisbane - 2015'.toLowerCase():
                ord.SAPOrderObj.SupplyingSloc = '2015';
                ord.ReSubmit();
                break;
            case 'Schenker - Perth - 2014'.toLowerCase():
                ord.SAPOrderObj.SupplyingSloc = '2014';
                ord.ReSubmit();
                break;
            case 'Schenker - Adelaide - 2013'.toLowerCase():
                ord.SAPOrderObj.SupplyingSloc = '2013';
                ord.ReSubmit();
                break;
        }

        var originLocation = ord.SAPOrderObj.OriginLocation;
        var $ctrl = $('OriginLocation');
        if (ord.Orders[0].DocType == 'UB') {
            ord.ReSubmit();
            if (originLocation == '') {
                $ctrl.addClass('ng-invalid');
            }
            else {
                $ctrl.removeClass('ng-invalid');
            }
        }
    };

    function validateInput() {
        ord.InvalidOrderMsg = '';
        var orderDetails = ord.SAPOrder.split(/\r\n|\r|\n/g);
        for (var i = 0; i < orderDetails.length; i++) {
            var row = orderDetails[i].split('\t');
            //changed from 12 to 20
            if (row.length !== 19) {
                ord.InvalidOrderMsg = 'The order details you have posted are invalid.  The row should contain at least 19 columns';
                return false;
            }
            var j = 0;
            var order = {};
            angular.copy(SAPOrderObj, order);

            for (var x in order) {
                order[x] = row[j]; j++;
            }
            ord.Orders.push(order);
        }

        return true;
    }

    function validateDocType(item) {
        if (!$.isNumeric(item.PurchasingDoc)) return orderPasteError('Purchase Order must be a number.');

        if (!item.PurchasingDoc) return orderPasteError('Purchase Order must be supplied.');

        if (item.PurchasingDoc.length != 10) return orderPasteError('Purchase Order must be 10 digits.');

        if (!item.ReceivingSloc) return orderPasteError('Receiving Location must be specified.');

        if (item.ReceivingSloc.length < 4) return orderPasteError('Receiving Location must at least 4 numbers.');

        if (!$.isNumeric(item.ReceivingSloc)) return orderPasteError('Receiving Location must be a number.');

        var dateRegEx = new RegExp(/^(?:(?:31(\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/);
        if (!dateRegEx.test(item.DeliveryDate)) return orderPasteError('Delivery date must be in the format dd.MM.yyyy and be a valid date.');

        var timeRegEx = new RegExp(/^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/);
        if (!timeRegEx.test(item.DeliveryTime)) return orderPasteError('Delivery time must be in the format HH:mm:ss and be a valid time.');

        if (!item.Material) return orderPasteError('Material number must be supplied.');

        if (!item.Quantity) return orderPasteError('Order Quantity must be supplied');

        if (!$.isNumeric(item.Quantity)) return orderPasteError('Item Quantity must be a number.');

        //if (parseInt(item.Quantity) < 1 || parseInt(item.Quantity) > 10) return orderPasteError('Item Quantity must be between 1 and 10');

        if (!item.Item) return orderPasteError('Item number must be supplied');

        if (!$.isNumeric(item.Item)) return orderPasteError('Item number must be numeric');

        if (!item.OrderUnit) return orderPasteError('Order Unit must be supplied.');

        if (item.Batch) {
            if (!$.isNumeric(item.Batch)) return orderPasteError('Batch must be a number.');
        }

        var validDocTypes = ['EB', 'ZS', 'NB', 'UB'];


        if (!item.SupplyingSloc && item.DocType == 'UB') return orderPasteError('Supplying Sloc must be supplied.');

        if (item.SupplyingSloc.length < 4 && item.DocType == 'UB') return orderPasteError('Supplying Sloc must at least 4 numbers');



        for (var i = 0; i < validDocTypes.length; i++) {
            if (item.DocType.toLowerCase() == validDocTypes[i].toLowerCase()) return true;
        }
        return orderPasteError('Invalid doc type');
    }

    function orderPasteError(msg) {
        ord.InvalidOrderMsg = msg;
        ord.Orders = [];
        return false;
    }

    function validateOrdersUnique() {
        if (ord.Orders.length === 1) {
            var item = ord.Orders[0];
            return validateDocType(item);
        }


        var prevOrder = ord.Orders[0];
        if (!validateDocType(prevOrder)) return false;




        for (var i = 1; i < ord.Orders.length; i++) {
            var currOrder = ord.Orders[i];
            if (currOrder.DocType != prevOrder.DocType) {
                return orderPasteError('Multiple different Document Types entered');
            }
            if (currOrder.PurchasingDoc != prevOrder.PurchasingDoc) {
                return orderPasteError('Multiple different Purchase Document Numbers entered');
            }

            if (currOrder.SupplyingSloc != prevOrder.SupplyingSloc) {
                return orderPasteError('Multiple different Supplying Locations entered');
            }
            if (currOrder.ReceivingSloc != prevOrder.ReceivingSloc) {
                return orderPasteError('Multiple different Receiving Locations entered');
            }
            if (currOrder.DeliveryDate != prevOrder.DeliveryDate) {
                return orderPasteError('Multiple different Delivery Dates entered');
            }
            if (currOrder.DeliveryTime != prevOrder.DeliveryTime) {
                return orderPasteError('Multiple different Delivery Times entered');
            }
            if (currOrder.Requisitioner != prevOrder.Requisitioner) {
                return orderPasteError('Multiple Requisitioner entered');
            }
            if (currOrder.Item == prevOrder.Item) {
                return orderPasteError('Duplicate Purchase Order Item numbers not allowed');
            }
            //for validation of new fields
            if (currOrder.Name1 != prevOrder.Name1) {
                return orderPasteError('Multiple Customer Name entered.');
            }
            if (currOrder.Name2 != prevOrder.Name2) {
                return orderPasteError('Multiple Room and Department entered.');
            }
            if (currOrder.Name3 != prevOrder.Name3) {
                return orderPasteError('Multiple Site Contact Name entered.');
            }
            if (currOrder.Street != prevOrder.Street) {
                return orderPasteError('Multiple Street entered.');
            }
            if (currOrder.City != prevOrder.City) {
                return orderPasteError('Multiple City entered.');
            }
            if (currOrder.Postcode != prevOrder.Postcode) {
                return orderPasteError('Multiple Postcode entered.');
            }
            //if (currOrder.Name4 != prevOrder.Name4) {
            //    return orderPasteError('Multiple Site Contact Phone entered.');
            //}
            if (currOrder.Street5 != prevOrder.Street5) {
                return orderPasteError('Multiple Site Remarks entered.');
            }


            if (!validateDocType(currOrder)) return false;

            //if (!currOrder.Material) return orderPasteError('Material number must be supplied.');
            //if (!currOrder.Quantity) return orderPasteError('Order Quantity must be supplied');
            //if (!$.isNumeric(currOrder.Quantity)) return orderPasteError('Item Quantity must be a number.');
            //if (parseInt(currOrder.Quantity) < 1 || parseInt(currOrder.Quantity) > 10) return orderPasteError('Item Quantity must be between 1 and 10');

        }
        return true;
    }

    function setUp() {
        ord.SAPOrder = '';
        ord.SAPOrderSubmitted = false;
        ord.SAPOrderValid = false;
        ord.InvalidOrderMsg = '';
        ord.HeaderValidated = false;
        ord.chkTailGate = false;
        ord.ValidationResult = {
            HasErrors: false
        };
        ord.GetDeliveryInfo = false;
        ord.DeliveryTypes = [
            {
                Id: 'LB', Name: 'Latest by'
            },
            {
                Id: 'TS', Name: 'Time specific'
            }
        ];


        ord.SAPOrderObj = {};
        orderEntryService.Header = ord.SAPOrderObj;

        ord.Orders = [];

        window.setTimeout(function () {
            document.getElementById('SAPOrder').focus();
        }, 0);
    }

}

