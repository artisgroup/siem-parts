﻿angular.module('SpareParts.OrderEntry').controller('OrderItemsCtrl', OrderItemsCtrl);

OrderItemsCtrl.$inject = ['$log', '$scope', 'orderEntryService'];

function OrderItemsCtrl($log, $scope, orderEntryService) {
    var oItm = this;

    setUp();

    $scope.$watch(
        function (scope) { return scope.$parent.ord.HeaderValidated; },
        function (newVal, oldVal) {
            if (newVal) {
                oItm.ItemsLoaded = false;
                submitSAPOrders($scope.$parent.ord.Orders);
            }
        });

    oItm.launchModal = function (material) {
        $('#itemDetailsModal').modal('show');
        oItm.ModalOptions.NoResults = true;
        oItm.ModalOptions.Mode = material ? 'Edit' : 'Find';
        oItm.MaterialSearch = '';
        oItm.ModalOptions.MaterialToEdit = material || {};
        if (material) {
            getDimensions(material);
        }
    };

    $('#itemDetailsModal').on('shown.bs.modal', function () {
        $('#MaterialNumber').focus()
    })

    oItm.search = function () {
        oItm.SearchComplete = false;
        orderEntryService.searchMaterial(oItm.MaterialSearch).then(
            function (data) {
                angular.copy(data, oItm.MaterialSearchResult);
                if (data.Material === 'Not found') {
                    oItm.ModalOptions.NoResults = true;
                    oItm.ModalOptions.MaterialToEdit = data;
                } else {
                    oItm.ModalOptions.MaterialToEdit = getDimensions(data);
                    oItm.ModalOptions.NoResults = false;
                }
                oItm.SearchComplete = true;
            }
        );
    };

    oItm.checkBatch = function (batch, item) {
        var $ctrl = $('#batch_' + item);
        if (batch != "") {
            if (!$.isNumeric(batch) || (batch.length > 10)) {
                $ctrl.addClass('ng-invalid');
            } else {
                $ctrl.removeClass('ng-invalid');
            }
        }
        else { $ctrl.removeClass('ng-invalid'); }
    }

    oItm.AddMaterial = function () {
        // first get the last row number
        var rowNum = oItm.Items.length ? (parseInt(oItm.Items[oItm.Items.length - 1].Item)) : 0;
        var newItm = {};
        oItm.MaterialSearchResult.Item = rowNum + 10;
        oItm.MaterialSearchResult.Qty = "1";
        angular.copy(oItm.MaterialSearchResult, newItm);
        newItm.Batch = $scope.$parent.ord.SAPOrderObj.Batch;
        oItm.Items.push(newItm);
        $('#itemDetailsModal').modal('hide');
        checkWeightWarning();
    };

    oItm.removeItem = function (item) {
        for (var i = oItm.Items.length - 1; i >= 0; i--) {
            if (oItm.Items[i].Item === item) {
                oItm.Items.splice(i, 1);
            }
        }
    }

    function getDimensions(material) {
        var weightUOM = material.Weight.split(' ')[1] || '';
        var dim = material.Dim;
        var dimUOM = dim.split(' ')[1] || '';
        material.Weight = material.Weight.split(' ')[0] || '';
        material.WeightUOM = weightUOM;
        material.DimUOM = dimUOM;
        var dims = dim.split('x');
        material.Length = dims[0] || '';
        material.Width = dims[1] || '';
        material.Height = dims[2] || '';
        if (material.Height) {
            material.Height = material.Height.split(' ')[0] || material.Height;
        }
        return material;
    }

    function submitSAPOrders(orders) {
        if ($scope.$parent.ord.Id) {
            loadOrderItems($scope.$parent.ord.SAPOrderObj.Items);
        }
        else {
            orderEntryService.submitSAPOrders(orders).then(loadOrderItems, handleError);
        }
    }

    function loadOrderItems(data) {
        oItm.Items = data;
        orderEntryService.Items = oItm.Items;
        oItm.ItemsLoaded = true;
        checkWeightWarning();
        $scope.$parent.ord.Items = oItm.Items;

    }

    function handleError() {
        oItm.Items = [];
        oItm.ItemsLoaded = true;
    }

    function setUp() {
        oItm.SearchComplete = true;
        oItm.ItemsLoaded = true;
        oItm.ModalOptions = { NoResults: true };
        oItm.MaterialSearchResult = {};
        oItm.Items = {};
        oItm.WeightWarning = '';
        oItm.chkTailGate = false;
        //orderEntryService.Items = oItm.Items;
    }

    function checkWeightWarning() {
        orderEntryService.checkWeightWarning(oItm.Items).then(function (data) {
            oItm.WeightWarning = data;
            $scope.$parent.ord.WeightWarning = data;
        });
    }


}