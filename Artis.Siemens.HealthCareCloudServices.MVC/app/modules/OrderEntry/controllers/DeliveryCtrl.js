﻿angular.module('SpareParts.OrderEntry').controller('DeliveryCtrl', DeliveryCtrl);

DeliveryCtrl.$inject = ['$log', '$scope', '$q', 'orderEntryService', 'empService'];

function DeliveryCtrl($log, $scope, $q, orderEntryService, empService) {
    var del = this;
    del.LoadingDeliveryLocations = false;
    del.CS = {};
    del.DP = {};
    del.Emp = {};
    del.EditDP = false;
    del.EditEA = false;
    del.EditCS = false;


    del.DeliveryLocationsLoaded = false;
    $scope.$watch(
        function (scope) { return scope.$parent.ord.GetDeliveryInfo; },
        function (newVal, oldVal) {
            if (newVal) {
                getDeliveryLocations();
                $scope.$parent.ord.GetDeliveryInfo = false;
            }
        }
    );
    $scope.hideFormDP = function () {

        if (del.EditDP == false) {
            del.EditDP = true;
        }
        else {
            del.EditDP = false;
        }
    }
    $scope.hideFormEA = function () {

        if (del.EditEA == false) {
            del.EditEA = true;
        }
        else {
            del.EditEA = false;
        }
    }
    $scope.hideFormCS = function () {

        if (del.EditCS == false) {
            del.EditCS = true;
        }
        else {

            del.EditCS = false;
        }
    }

    del.searchCS = function (query) {
        return orderEntryService.searchCS(query);
    };

    del.csResultSelected = function (item) {
        if (item.NoResults) {
            $scope.AsyncSearchCS = '';
            return;
        }
        del.CS = item;
        $scope.AsyncSearchCS = '';
    };

    del.searchDP = function (query) {
        return orderEntryService.searchDP(query);
    };

    del.dpResultSelected = function (item) {
        if (item.NoResults) {
            $scope.AsyncSearch = '';
            return;
        }
        del.DP = item;
        $scope.AsyncSearch = '';
    };

    del.searchEA = function (query) {
        return empService.searchEA(query);
    };

    del.eaResultSelected = function (item) {
        if (item.NoResults) {
            $scope.AsyncEmpSearch = '';
            return;
        }
        del.Emp = item;
        $scope.AsyncEmpSearch = '';
    };

    del.selectCustomerSite = function () {
        var order = getOrderObj();
        var tailgate = 0;
        order.CustomerSite = del.CS.Name1;
        order.CustomerRoom = del.CS.Name2;
        order.CustomerDepartment = del.CS.Name3;
        order.Street = del.CS.Street;
        order.City = del.CS.City;
        order.Country = del.CS.Country;
        order.CustomerSiteContactName = del.CS.ContactName;
        order.CustomerSiteContactPhone = del.CS.ContactPhone;
        order.CustomerSiteDeliveryRemarks = del.CS.DeliveryRemarks;
        order.DestRegion = del.CS.Region;
        order.Region = del.CS.Region;
        order.DestPostCode = del.CS.PostCode;
        order.PostCode = del.CS.PostCode;
        //order.CustomerSite = $scope.$parent.ord.SAPOrderObj.Name1;
        //order.CustomerRoom = $scope.$parent.ord.SAPOrderObj.Name2;
        //order.CustomerDepartment = $scope.$parent.ord.SAPOrderObj.Name2;
        //order.Street = $scope.$parent.ord.SAPOrderObj.Street;
        //order.City = $scope.$parent.ord.SAPOrderObj.City;
        //order.Country = del.CS.Country;
        //order.CustomerSiteContactName = $scope.$parent.ord.SAPOrderObj.Name3;
        ////order.CustomerSiteContactPhone = $scope.$parent.ord.SAPOrderObj.Name4;
        //order.CustomerSiteDeliveryRemarks = $scope.$parent.ord.SAPOrderObj.Street5;
        //order.DestRegion = del.CS.Region;
        //order.Region = del.CS.Region;
        //order.DestPostCode = del.CS.PostCode;
        //order.PostCode = $scope.$parent.ord.SAPOrderObj.Postcode;
        order.ServiceLevelType = "C";
        if ($scope.ord.chkTailGate == true) {
            tailgate = 1;
        }
        order.TailGate = tailgate;
        submitOrder(order);
    };

    del.selectDropPoint = function () {
        var order = getOrderObj();
        var tailgate = 0;
        order.CustomerSite = del.DP.Name1;
        order.DropPointId = del.DP.Id;
        order.Street = del.DP.Street;
        order.CustomerRoom = del.DP.Name2;
        order.City = del.DP.Suburb;
        order.DestRegion = del.DP.Region;
        order.DestPostCode = del.DP.PostCode;
        //to populate the region for the Engineer selection
        order.Region = del.DP.Region;
        if ($scope.ord.chkTailGate == true) {
            tailgate = 1;
        }
        order.TailGate = tailgate;
        order.ServiceLevelType = "D";
        submitOrder(order);
    };


    del.selectEngineer = function () {
        var order = getOrderObj();
        var tailgate = 0;
        order.EngineerAddressId = del.Emp.Sloc;
        order.CustomerSite = del.Emp.GivenName + " " + del.Emp.Surname;
        order.Street = del.Emp.Street;
        order.City = del.Emp.City;
        order.DestRegion = del.Emp.Region;
        order.DestPostCode = del.Emp.PostCode;       
        //to populate the region for the Engineer selection
        order.Region = del.Emp.Region;

        if ($scope.ord.chkTailGate == true) {
            tailgate = 1;
        }
        order.TailGate = tailgate;
        order.ServiceLevelType = "E";
        submitOrder(order);
    };

    function submitOrder(order) {
        if (order.Error) {
            alert(order.Error);
            return;
        }
        //do update - or add
        orderEntryService.create(order).then(processResponse);

    }

    function getOrderObj() {
        var order = {};
        angular.copy(orderEntryService.Header, order);
        order.Items = orderEntryService.Items;
        order.SLoc = $scope.$parent.ord.SAPOrderObj.SupplyingSloc;
        if ($scope.$parent.OrderEntryForm.$error.required || $('#tblItems .ng-invalid').length > 0) {
            order.Error = 'Cannot submit Order, please correct errors on the Order Entry form.';
        }
        return order;
    }

    function processResponse(orderId) {
        if (orderId < 1 || !orderId) {
            alert('There was an error saving to the database');
            return;
        }
        var redirect = appBase + '/Order/Confirm?id=' + orderId;
        console.log(redirect);
        document.location.href = redirect;
    }

    function getDeliveryLocations() {
        del.LoadingDeliveryLocations = true;
        var notificationNumber = $scope.$parent.ord.SAPOrderObj.Requisitioner;
        var csPromise = orderEntryService.getCustomerSite(notificationNumber);
        var dpPromise = orderEntryService.getDropPoint(notificationNumber);
        var recSloc = $scope.$parent.ord.SAPOrderObj.ReceivingSloc;
        var empPromise = orderEntryService.getEmployeeBySloc(recSloc);

        $q.all([csPromise, dpPromise, empPromise]).then(
            function (data) {
                del.CS = data[0];
                del.DP = data[1];
                del.Emp = data[2];
                del.Emp.Name1 = 'Siemens Healthcare Ltd';

                //for additional items
                del.CS.Name1 = $scope.$parent.ord.SAPOrderObj.Name1;
                del.CS.Name2 = $scope.$parent.ord.SAPOrderObj.Name2;
                del.CS.Name3 = $scope.$parent.ord.SAPOrderObj.Name2;
                del.CS.ContactName = $scope.$parent.ord.SAPOrderObj.Name3;
                del.CS.DeliveryRemarks = $scope.$parent.ord.SAPOrderObj.Street5;
                //del.CS.ContactPhone = $scope.$parent.ord.SAPOrderObj.Name4;
                del.CS.PostCode = $scope.$parent.ord.SAPOrderObj.Postcode;
                del.CS.City = $scope.$parent.ord.SAPOrderObj.City;
                del.CS.Street = $scope.$parent.ord.SAPOrderObj.Street;

                //update from parent object - if applicable (when re-loading values)
                if ($scope.$parent.ord.DeliveryChoice) {
                    var ord = $scope.$parent.ord;
                    if (ord.DeliveryChoice == "1") {
                        del.Emp.Sloc = ord.EngineerAddressId;
                        del.Emp.CustomerSite = ord.FullName;
                        del.Emp.GivenName = ord.FullName;
                        del.Emp.Surname = "";
                        del.Emp.Street = ord.Street;
                        del.Emp.City = ord.City;
                        del.Emp.Region = ord.DestRegion;
                        del.Emp.PostCode = ord.DestPostCode;

                    }
                    else if (ord.DeliveryChoice == "2") {
                        del.DP.Name1 = ord.Name1;
                        del.DP.Name2 = ord.Name2;
                        del.DP.CustomerSite = ord.Name1;
                        del.DP.DropPointId = ord.Id;
                        del.DP.Street = ord.Street;
                        del.DP.Suburb = ord.Suburb;
                        del.DP.Region = ord.DestRegion;
                        del.DP.DestPostCode = ord.DestPostCode;

                    }
                    else if (ord.DeliveryChoice == "3") {
                        //del.CS.Name1 = ord.Name1;
                        //del.CS.Name2 = ord.Name2;
                        //del.CS.Name3 = ord.Name3;
                        //del.CS.Street = ord.Street;
                        //del.CS.City = ord.City;
                        //del.CS.Country = ord.Country;
                        //del.CS.ContactName = ord.ContactName;
                        del.CS.ContactPhone = ord.ContactPhone;
                        //del.CS.DeliveryRemarks = ord.DeliveryRemarks;
                        //del.CS.Region = ord.Region;
                        //del.CS.PostCode = ord.PostCode;
                        del.CS.Name1 = $scope.$parent.ord.SAPOrderObj.Name1;
                        del.CS.Name2 = $scope.$parent.ord.SAPOrderObj.Name2;
                        del.CS.Name3 = $scope.$parent.ord.SAPOrderObj.Name2;
                        del.CS.Street = $scope.$parent.ord.SAPOrderObj.Street;
                        del.CS.City = $scope.$parent.ord.SAPOrderObj.City;
                        del.CS.Country = ord.Country;
                        del.CS.ContactName = $scope.$parent.ord.SAPOrderObj.Name3;
                        //del.CS.ContactPhone = $scope.$parent.ord.SAPOrderObj.Name4;
                        del.CS.DeliveryRemarks = $scope.$parent.ord.SAPOrderObj.Street5;
                        del.CS.Region = ord.Region;
                        del.CS.PostCode = $scope.$parent.ord.SAPOrderObj.Postcode;
                    }
                }

                del.LoadingDeliveryLocations = false;
                del.DeliveryLocationsLoaded = true;
            }
        );
    }
}