﻿angular.module('SpareParts.OrderTrack', ['ngSanitize', 'SpareParts.Core', 'ngAnimate', 'restangular', 'ui.directives']);

angular.module('SpareParts.OrderTrack').run(['$rootScope', '$log', 'Restangular',
    function ($rootScope, $log, Restangular) {
        Restangular.setBaseUrl(appBase + 'api/');
    }]
);