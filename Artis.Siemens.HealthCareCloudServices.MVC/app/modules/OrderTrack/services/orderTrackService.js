﻿angular.module('SpareParts.OrderTrack').service('orderTrackService', orderTrackService);

orderConfirmService.$inject = ['Restangular'];

function orderTrackService(Restangular) {
    var _otEndPoint = Restangular.all('ordertrack');

    return {
        getTrackDeliveries: _getTrackDeliveries,
        getPartsOrdered: _getPartsOrdered,
        getTransportEvents: _getTransportEvents
    };

    function _getTrackDeliveries(postData) {
        return _otEndPoint.customPOST(postData, 'getTrackDeliveries');
    }

    //function _getPartsOrdered(po, cn) {
    //    return _otEndPoint.customGET('getPartsOrdered?purchaseOrder=' + po + '&conNote=' + cn);
    //}

    //function _getTransportEvents(po, cn) {
    //    return _otEndPoint.customGET('getTransportEvents?purchaseOrder=' + po + '&conNote=' + cn);
    //}
    function _getPartsOrdered(po) {
        return _otEndPoint.customGET('getPartsOrdered?purchaseOrder=' + po);
    }

    function _getTransportEvents(po) {
        return _otEndPoint.customGET('getTransportEvents?purchaseOrder=' + po);
    }
}