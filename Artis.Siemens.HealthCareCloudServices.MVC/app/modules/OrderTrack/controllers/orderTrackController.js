﻿angular.module('SpareParts.OrderTrack').controller('OrderTrackController', OrderTrackController);

OrderTrackController.$inject = ['$log', '$q', '$location', 'empService', 'orderTrackService'];

function OrderTrackController($log, $q, $location, empService, orderTrackService) {
    var trk = this;
    setUp();

    function setUp() {
        //fields for initial query/submit
        trk.Requisitioner = '';
        trk.DestinationRegion = '';
        trk.PurchasingDoc = '';
        trk.OriginLocation = '';
        trk.ReceivingSloc = '';

        trk.SearchResults = false;

        trk.MatchedOrders = [];

        trk.ModalData = {};

        // load employees
        empService.getList().then(function (data) {
            trk.Employees = data;
        })

        //load default view
        trk.RetrievalMode = 'DEFAULT';
        trk.CurrentUserLogin = CurrentLoginId;
        trk.OrdersLoaded = false;

        orderTrackService.getTrackDeliveries(trk).then(function (data) {
            trk.MatchedOrders = data;
            trk.OrdersLoaded = true;
        })
    }

    trk.submitTrackSearch = function () {
        trk.SearchResults = true;
        trk.RetrievalMode = 'SEARCH';
        trk.OrdersLoaded = false;

        //do searching - using the model (trk) which will have search criteria
        orderTrackService.getTrackDeliveries(trk).then(function (data) {
            trk.MatchedOrders = data;
            trk.OrdersLoaded = true;
        })
    }


    //trk.showDetailsModal = function (selectedPO, conNote)
    trk.showDetailsModal = function (selectedPO) {

        //set the current modal item to be from the selected PO
        trk.ModalData = selectedPO;
        //trk.ModalData.SelectedConNote = conNote;
        trk.ModalData.PartsOrdered = [{ Item: 'Fetching...', Material: '', Description: '', Batch: '', Qty: '', SchenkerSHP:'', SchenkerDEL:'' }];
        trk.ModalData.TransportEvents = [{ Date: 'Fetching...', Time: '', Location: '', Event: '', Text: '' }];

        //load parts, using the selected po 
        //orderTrackService.getPartsOrdered(trk.ModalData.PurchaseOrderNumber, conNote).then(function (data) {
        orderTrackService.getPartsOrdered(trk.ModalData.PurchaseOrderNumber).then(function (data) {
            trk.ModalData.PartsOrdered = data;
        });

        //load parts, using the selected po 
        //orderTrackService.getTransportEvents(trk.ModalData.PurchaseOrderNumber, conNote).then(function (data) {
        orderTrackService.getTransportEvents(trk.ModalData.PurchaseOrderNumber).then(function (data) {
            trk.ModalData.TransportEvents = data;
        });

        $('#shippingDetailsModal').modal('show');

    };

}