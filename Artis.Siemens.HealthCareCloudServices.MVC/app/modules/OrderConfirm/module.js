﻿angular.module('SpareParts.OrderConfirm', ['ngSanitize', 'SpareParts.Core', 'ngAnimate', 'restangular', 'ui.directives']);

angular.module('SpareParts.OrderConfirm').run(['$rootScope', '$log', 'Restangular',
    function ($rootScope, $log, Restangular) {
        Restangular.setBaseUrl(appBase + 'api/');
    }]
);