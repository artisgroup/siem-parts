﻿angular.module('SpareParts.OrderConfirm').controller('OrderDetailCtrl', OrderDetailCtrl);

OrderDetailCtrl.$inject = ['$log', '$q', '$location', 'orderEntryService', 'orderConfirmService', 'empService'];

function OrderDetailCtrl($log, $q, $location, orderEntryService, orderConfirmService, empService) {
    var odc = this;
    setUp();

    function setUp() {
        odc.Order = [];
        odc.Id = null;
        odc.TotalWeight = 'Calculating...';
        odc.Employee = { Id: '', GivenName: 'Fetching', Surname: 'Data', Mobile: '' };
        odc.IBase = {};
        odc.ZMon = {};
        odc.AssignedCSE1 = {};
        odc.AssignedCSE2 = {};
        odc.DeliveryZone = '';
        odc.Times = ['Loading...', 'Loading...'];
        odc.OrderDetailsLoaded = true;
        odc.ShowFlightsTable = false;
        odc.DeliverToDropPoint = false;
        odc.WeightWarning = '';
        odc.XML = 'Loading...';
        odc.Cost = null;
       
        
        orderConfirmService.getGenericServices().then(function (data) {
            odc.GenericServices = data;
        })
    }

    odc.goBack = function ()
    {
        var redirect = appBase + '/Order/Create?id=' + odc.Id;
        document.location.href = redirect;
    }

    odc.getGenericServiceDetails = function () {
        //get the details from generic services (loaded already)
        for (i = 0; i < odc.GenericServices.length; i++)
        {
            if (odc.GenericServices[i].Id == odc.genericService.SelectedServiceId)
            {
                odc.genericService.Description = odc.GenericServices[i].AvailableServices;
                odc.genericService.SelectedServiceLevelCode = odc.GenericServices[i].ServiceLevelCode;
                odc.genericService.CutOffTime = odc.GenericServices[i].CutOffTime;
                //odc.Cost.SelectedServiceLevelCode = odc.Cost[i].Cost;
               
            }
        }
    }

    odc.checktailgate = function (status) {
        if (status == "1") {
            return true;
        }
        else {
            return false;
        }
    }

    odc.closeRedirect = function () {
        $('.modal-backdrop').hide();
        document.location.href = "/parts/order/create"
    }

    odc.getOrderDetails = function (id) {
        if (!id) {
            alert('An order id was not supplied.  You will be redirected to the Order Creation screen.');
            document.location.href = appBase;
        } else {
            odc.Id = id;
            odc.OrderDetailsLoaded = false;

            orderConfirmService.getByOrderId(id).then(function (data) {
                odc.Id = id;
                odc.OrderDetailsLoaded = true;
                odc.Services = data.Services;
                odc.Flights = data.Flights;
                odc.LastVisit = data.LastVisit;
                odc.NextVisit = data.NextVisit;
                odc.Order = data.Order;
                odc.IBase = data.IBase;
                odc.Employee = data.Employee;
                odc.ZMon = data.ZMon;
                odc.AssignedCSE1 = data.AssignedCSE1;
                odc.AssignedCSE2 = data.AssignedCSE2;
                odc.TotalWeight = data.TotalWeight;
                odc.WeightWarning = data.WeightWarning;
                odc.ShowFlightsTable = (odc.Order.OriginRegion == 'SGP' || odc.Order.OriginRegion == 'FRA');
                odc.DeliverToDropPoint = (odc.Order.DropPointId ? true : false);
                odc.DeliveryZone = odc.Services.length ? odc.Services[0].DestinationZone : '';
                odc.OrderLink = appBase + 'Order/Confirm?id=' + odc.Id;
                odc.EffectSeverity = odc.getSeverity(data.ZMon.Effect_On_Operation);

                $log.log(odc);
                getTimes();
            });


        }
    }

    odc.getSeverity = function (effectCode) {
        if (effectCode == '1')
            return 'System Down';
        else if (effectCode == '2')
            return 'System Partially Down';
        else if (effectCode == '3')
            return 'Scheduled Service';
        else if (effectCode == '4')
            return 'Commissioning';
        else if (effectCode == '5')
            return 'Technical Support';
        else if (effectCode == '6')
            return 'Apps - Scheduled Service';
        else if (effectCode == '7')
            return 'Apps - Partially Down';
        else if (effectCode == '8')
            return 'Apps - Fully Down';
        else
            return 'Unknown';
    }

    odc.submitOrder = function (serviceLevelCode, serviceLevelDescription, warehousePriority, siemensContactGivenName, siemensContactLastName, siemensContactPhone, Cost) {

        var obj = {
            OrderId: odc.Id,
            ServiceLevelCode: serviceLevelCode,
            ServiceLevelDescription: serviceLevelDescription,
            WarehousePriority: warehousePriority,
            SiemensContactName: siemensContactGivenName + ' ' + siemensContactLastName,
            SiemensContactPhone: siemensContactPhone,
            //include Cost
            Cost:Cost
        };

        $('#submitModal').modal('show')

        orderConfirmService.submitOrder(obj).then(
            function (data) {
                odc.XML = data;
                odc.xmlStatus = "Submitted to Schenker Successfully";
            });
    }

    function getTimes() {
        var origTimePromise = orderConfirmService.getTimeForRegion(odc.Order.OriginRegion);
        var destTimePromise = orderConfirmService.getTimeForRegion(odc.Order.DestRegion);
        $q.all([origTimePromise, destTimePromise]).then(function (data) {
            odc.Times = data;

        });
    }
}