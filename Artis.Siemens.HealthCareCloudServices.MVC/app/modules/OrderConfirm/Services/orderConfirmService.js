﻿angular.module('SpareParts.OrderConfirm').service('orderConfirmService', orderConfirmService);

orderConfirmService.$inject = ['Restangular'];

function orderConfirmService(Restangular) {
    var _ocEndPoint = Restangular.all('orderconfirm');

    return {
        getByOrderId: _getByOrderId,
        getGenericServices: _getGenericServices,
        //calculateWeight: _calculateWeight,
        //calculateETA: _calculateETA,
        getTimeForRegion: _getTimeForRegion,
        submitOrder: _submitOrder
    };


    function _getByOrderId(id) {
        return _ocEndPoint.customGET('getOrderDetail?orderId=' + id);
    }

    function _getGenericServices() {
        return _ocEndPoint.customGET('getGenericServices');
    }

    function _getTimeForRegion(region) {
        return _ocEndPoint.customGET('getTimeForRegion?region=' + region);
    }

    function _submitOrder(data) {
        return _ocEndPoint.customPOST(data, 'submitOrder');
    }
}