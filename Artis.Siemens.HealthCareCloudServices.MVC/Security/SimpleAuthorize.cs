﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Security
{
     [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true,
    AllowMultiple = true)]
    public class SimpleAuthorizeAttribute : AuthorizeAttribute
    {
        private const string IS_AUTHORIZED = "isAuthorized";

        public string RedirectUrl = "~/Accounts/Login";

         protected override bool AuthorizeCore(HttpContextBase httpContext)
         {
             var isAuthorized = httpContext.Session["LoginId"] != null;

             httpContext.Items.Add(IS_AUTHORIZED, isAuthorized);

             if(!isAuthorized) httpContext.Response.Redirect(GetLoginRedirect());

             return isAuthorized;
         }

         //public override void OnAuthorization(AuthorizationContext filterContext)
         //{
         //    //base.OnAuthorization(filterContext);

         //    var isAuthorized = filterContext.HttpContext.Items[IS_AUTHORIZED] != null
         //        ? Convert.ToBoolean(filterContext.HttpContext.Items[IS_AUTHORIZED])
         //        : false;

         //    if (!isAuthorized)// && filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
         //    {
         //        filterContext.RequestContext.HttpContext.Response.Redirect(GetLoginRedirect());
         //    }
         //}

         private string GetLoginRedirect()
         {
             if (ConfigurationManager.AppSettings["InProd"] == "yes")
             {
                 return ConfigurationManager.AppSettings["ESUrl"];
             }
             return "/Account/Login";
         }
    }
}