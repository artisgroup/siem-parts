﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.MVC
{
    public static class Constants
    {
        public static int PAGE_SIZE = 50;
    }
}
