[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Artis.Siemens.HealthCareCloudServices.MVC.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Artis.Siemens.HealthCareCloudServices.MVC.App_Start.NinjectWebCommon), "Stop")]

namespace Artis.Siemens.HealthCareCloudServices.MVC.App_Start
{
    using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
    using Artis.Siemens.HealthCareCloudServices.EF.Repository;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Activation;
    using Ninject.Parameters;
    using Ninject.Syntax;
    using Ninject.Web.Common;
    using System;
    using System.Web;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IEmployeeRepository>().To<EmployeeRepository>();
            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<IMaterialMasterRepository>().To<MaterialMasterRepository>();
            kernel.Bind<ISchenkerStatusUpdateRepository>().To<SchenkerStatusUpdateRepository>();
            kernel.Bind<IMembershipRoleRepository>().To<MembershipRoleRepository>();
            kernel.Bind<IOrderTrackRepository>().To<OrderTrackRepository>();

        }
    }
}
