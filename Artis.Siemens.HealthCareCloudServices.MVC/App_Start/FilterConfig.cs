﻿using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.MVC.Security;

namespace Artis.Siemens.HealthCareCloudServices.MVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new SimpleAuthorizeAttribute());
        }
    }
}
