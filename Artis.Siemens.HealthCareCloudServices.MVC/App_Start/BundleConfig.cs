﻿using System.Web;
using System.Web.Optimization;

namespace Artis.Siemens.HealthCareCloudServices.MVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-2.1.4.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.min.js",
                        "~/Scripts/jquery.validate.unobtrusive.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/scripts/angular.min.js",
                        "~/scripts/angular-animate.min.js",
                        "~/scripts/angular-sanitize.min.js",
                        "~/scripts/lodash.min.js",
                        "~/scripts/restangular.min.js",
                        "~/scripts/select2.min.js",
                        "~/scripts/angular-ui.min.js",
                        "~/scripts/angular-ui/ui-bootstrap.min.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-2.8.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.min.js"));

            //====================================================================================================================

            ////APPLICATION BUNDLES - make sure to use the VERSION of the assembly - to avoid caching
            //bundles.Add(new ScriptBundle("~/js/app/oe")
            //    .Include("~/app/SpareParts.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/module.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/linkAction.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/loadingIndicator.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/stopEvent.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/validationResponse.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/services/empService.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/services/orderEntryService.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/OrderEntry/module.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/OrderEntry/controllers/DeliveryCtrl.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/OrderEntry/controllers/OrderCtrl.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/OrderEntry/controllers/OrderItemsCtrl.js" + Versioning.UpdateJavaScript())
            //    );

            //bundles.Add(new ScriptBundle("~/js/app/oc")
            //    .Include("~/app/SpareParts.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/module.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/linkAction.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/loadingIndicator.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/stopEvent.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/directives/validationResponse.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/services/empService.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/core/services/orderEntryService.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/OrderConfirm/module.js" + Versioning.UpdateJavaScript())
            //    .Include("~/app/modules/OrderConfirm/controllers/OrderDetailCtrl.js" + Versioning.UpdateJavaScript())
            //   .Include("~/app/modules/OrderConfirm/services/orderConfirmService.js" + Versioning.UpdateJavaScript())
            //    );

            //====================================================================================================================


            bundles.Add(new ScriptBundle("~/bundles/MvcBootstrapTimepicker").Include(
                        "~/Scripts/MvcBootstrapTimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/MvcBootstrapTimepicker.css",
                      "~/Content/site.css",
                      "~/Content/select2.css",
                      "~/Content/angular-ui.min.css"
                      ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
