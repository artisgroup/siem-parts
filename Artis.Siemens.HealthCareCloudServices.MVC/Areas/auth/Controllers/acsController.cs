﻿using ComponentPro.Saml2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Artis.Siemens.HealthCareCloudServices.Services;
using System.Collections;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.auth.Controllers
{
    [AllowAnonymous]
    public class acsController : Controller
    {
       
        private MembershipService _membershipService;

        public acsController(MembershipService membershipService)
        {
            _membershipService = membershipService;
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(object data)
        {
            ComponentPro.Saml2.Response samlResponse = ComponentPro.Saml2.Response.Create(Request);
            Assertion samlAssertion;


            if (samlResponse.IsSuccess())
            {
                if (samlResponse.GetAssertions().Count > 0)
                {
                    samlAssertion = samlResponse.GetAssertions()[0];


                    string userName;
                    string email;
                    string sn;
                    string gn;

                    if (samlAssertion.Subject.NameId != null)
                    {
                        userName = samlAssertion.Subject.NameId.NameIdentifier;
                        email = samlAssertion.GetAttributeValue("mail");
                        gn = samlAssertion.GetAttributeValue("givenname");
                        sn = samlAssertion.GetAttributeValue("sn");
                            
                        Session["LoginId"] = userName;
                        Session["DisplayName"] = gn + " " + sn;
                        Session["Firstname"] = gn;
                        Session["Lastname"] = sn;
                        Session["Email"] = email;
                        //send login details to DB and get the roles and store in session

                        var checkkUser = _membershipService.GetUserByLoginName(Session["LoginId"].ToString());

                        if (checkkUser == null)
                        {
                            AspNetUserVM aspnetuser = new AspNetUserVM();
                            aspnetuser.UserName = userName;
                            aspnetuser.Email = email;
                            aspnetuser.Firstname = gn;
                            aspnetuser.Lastname = sn;

                            var aspnetvm = _membershipService.Create(aspnetuser);
                            Session["LoginId"] = userName;
                        }

                        var userRoles = _membershipService.GetUserByLoginName(Session["LoginId"].ToString());

                        string[] roles = new string[userRoles.Roles.Length];
                        for (int i = 0; i < userRoles.Roles.Length; i++)
                        {
                            roles[i] = userRoles.Roles[i].Name.ToString();
                        }
                        Session["Roles"] = roles;

                }
                }
            }
            return Redirect("~/");
           
        }
    }
}