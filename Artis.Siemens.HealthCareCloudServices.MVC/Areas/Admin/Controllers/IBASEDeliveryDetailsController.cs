﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using Artis.Siemens.HealthCareCloudServices.EF.Repository;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using PagedList;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Admin.Controllers
{
    public class IBaseDeliveryDetailsController : BaseController
    {
        private IBaseDeliveryDetailsRepository _repository = new IBaseDeliveryDetailsRepository();

        // GET: IBaseDeliveryDetails
        public ActionResult Index(int? page)
        {
            string[] sessionRoles = (string[])Session["Roles"];
            if (!sessionRoles.Contains("Logistics"))
                return Redirect("~/Home");

            else
            {
                var leadTimes = _repository.All.OrderBy(i => i.DropPoint.Name1);

                var pageNumber = page ?? 1;
                var pageData = leadTimes.ToPagedList(pageNumber, Constants.PAGE_SIZE);

                ViewBag.PageData = pageData;
                return View();
            }

        }

        // GET: IBaseDeliveryDetails/Details/5
        public ActionResult Details(int id)
        {
            IBaseDeliveryDetail iBaseDeliveryDetail = _repository.Find(id);
            if (iBaseDeliveryDetail == null)
            {
                return HttpNotFound();
            }
            return View(iBaseDeliveryDetail);
        }

        // GET: IBaseDeliveryDetails/Create
        public ActionResult Create()
        {
            //ViewBag.DropPointId = new SelectList(db.DropPoints, "Id", "Name1");
            IBaseDeliveryDetailsVM vm = new IBaseDeliveryDetailsVM(_repository.GetDropPoints());
            return View(vm);
        }

        // POST: IBaseDeliveryDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DropPointId,FLId,SpecialRemarks,DeliverySiteContactName,DeliverySiteContactNumber,GenericService,LastEditedTime,LastEditedPerson")] IBaseDeliveryDetail iBaseDeliveryDetail)
        {
            if (ModelState.IsValid)
            {
                iBaseDeliveryDetail.LastEditedPerson = CurrentUsername;
                iBaseDeliveryDetail.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(iBaseDeliveryDetail, EntityState.Added);
                _repository.Save();
                return RedirectToAction("Index");
            }

            //ViewBag.DropPointId = new SelectList(_repository, "Id", "Name1", iBASEDeliveryDetail.DropPointId);
            return View(iBaseDeliveryDetail);
        }

        // GET: IBaseDeliveryDetails/Edit/5
        public ActionResult Edit(int id)
        {
            IBaseDeliveryDetail iBaseDeliveryDetail = _repository.Find(id);
            if (iBaseDeliveryDetail == null)
            {
                return HttpNotFound();
            }
            //ViewBag.DropPointId = new SelectList(db.DropPoints, "Id", "Name1", iBASEDeliveryDetail.DropPointId);
            IBaseDeliveryDetailsVM vm = new IBaseDeliveryDetailsVM(iBaseDeliveryDetail, _repository.GetDropPoints());
            return View(vm);
        }

        // POST: IBaseDeliveryDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DropPointId,FLId,SpecialRemarks,DeliverySiteContactName,DeliverySiteContactNumber,GenericService,LastEditedTime,LastEditedPerson")] IBaseDeliveryDetail iBaseDeliveryDetail)
        {
            if (ModelState.IsValid)
            {
                iBaseDeliveryDetail.LastEditedPerson = CurrentUsername;
                iBaseDeliveryDetail.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(iBaseDeliveryDetail, EntityState.Modified);
                _repository.Save();
                return RedirectToAction("Index");
            }
            //ViewBag.DropPointId = new SelectList(db.DropPoints, "Id", "Name1", iBASEDeliveryDetail.DropPointId);
            return View(iBaseDeliveryDetail);
        }

        // GET: IBaseDeliveryDetails/Delete/5
        public ActionResult Delete(int id)
        {
            IBaseDeliveryDetail iBaseDeliveryDetail = _repository.Find(id);
            if (iBaseDeliveryDetail == null)
            {
                return HttpNotFound();
            }
            return View(iBaseDeliveryDetail);
        }

        // POST: IBaseDeliveryDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IBaseDeliveryDetail iBaseDeliveryDetail = _repository.Find(id);
            _repository.Delete(iBaseDeliveryDetail);
            _repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
