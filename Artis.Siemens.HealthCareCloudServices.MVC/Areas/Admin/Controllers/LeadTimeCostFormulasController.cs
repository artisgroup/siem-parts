﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using Artis.Siemens.HealthCareCloudServices.EF.Repository;
using PagedList;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Admin.Controllers
{
    public class LeadTimeCostFormulasController : BaseController
    {
        private LeadTimeCostFormulaRepository _repository = new LeadTimeCostFormulaRepository();

        // GET: LeadTimeCostFormulas
        public ActionResult Index(int? page, string searchString)
        {
            string[] sessionRoles = (string[])Session["Roles"];
            if (!sessionRoles.Contains("Logistics"))
                return Redirect("~/Home");

            else
            {
                var leadTimes = _repository.All;

                if (!String.IsNullOrEmpty(searchString))
                {
                    leadTimes = leadTimes.Where(l => l.AvailableServices.Contains(searchString)
                                                || l.DestinationPostCode.Contains(searchString)
                                                || l.ServiceLevelDescription.Contains(searchString)
                                                || l.ServiceLevelCode.Contains(searchString)
                                                || l.RateFormula.Contains(searchString)
                                                || l.DestinationCity.Contains(searchString)
                                                || l.DestinationZone.Contains(searchString));
                }

                leadTimes = leadTimes.OrderBy(i => i.OriginRegion);

                var pageNumber = page ?? 1; 
                var pageData = leadTimes.ToPagedList(pageNumber, Constants.PAGE_SIZE); 

                ViewBag.PageData = pageData;
                return View();
            }
        }

        // GET: LeadTimeCostFormulas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int id1 = id ?? default(int);
            LeadTimeCostFormula leadTimeCostFormula = _repository.Find(id1);
            if (leadTimeCostFormula == null)
            {
                return HttpNotFound();
            }
            return View(leadTimeCostFormula);
        }

        // GET: LeadTimeCostFormulas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LeadTimeCostFormulas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OriginRegion,DestinationPostCode,DestinationCity,DestinationZone,AvailableServices,Tailgate,ServiceLevelCode,ServiceLevelDescription,CutOffTime,DeliveryTime,RateFormula,ServiceLevelRemarks,GenericService")] LeadTimeCostFormula leadTimeCostFormula)
        {
            if (ModelState.IsValid)
            {
                leadTimeCostFormula.LastEditedPerson = CurrentUsername;
                leadTimeCostFormula.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(leadTimeCostFormula, EntityState.Added);
                _repository.Save();
                return RedirectToAction("Index");
            }

            return View(leadTimeCostFormula);
        }

        // GET: LeadTimeCostFormulas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int id1 = id ?? default(int);
            LeadTimeCostFormula leadTimeCostFormula = _repository.Find(id1);
            if (leadTimeCostFormula == null)
            {
                return HttpNotFound();
            }
            return View(leadTimeCostFormula);
        }

        // POST: LeadTimeCostFormulas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OriginRegion,DestinationPostCode,DestinationCity,DestinationZone,AvailableServices,Tailgate,ServiceLevelCode,ServiceLevelDescription,CutOffTime,DeliveryTime,RateFormula,ServiceLevelRemarks,GenericService,GenericService")] LeadTimeCostFormula leadTimeCostFormula)
        {
            if (ModelState.IsValid)
            {
                leadTimeCostFormula.LastEditedPerson = CurrentUsername;
                leadTimeCostFormula.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(leadTimeCostFormula, EntityState.Modified);
                _repository.Save();
                return RedirectToAction("Index");
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return View(leadTimeCostFormula);
        }

        // GET: LeadTimeCostFormulas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int id1 = id ?? default(int);
            LeadTimeCostFormula leadTimeCostFormula = _repository.Find(id1);
            if (leadTimeCostFormula == null)
            {
                return HttpNotFound();
            }
            return View(leadTimeCostFormula);
        }

        // POST: LeadTimeCostFormulas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LeadTimeCostFormula leadTimeCostFormula = _repository.Find(id);
            _repository.Delete(leadTimeCostFormula);
            _repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
