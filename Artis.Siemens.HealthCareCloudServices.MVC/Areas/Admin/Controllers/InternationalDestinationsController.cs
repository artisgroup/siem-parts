﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using Artis.Siemens.HealthCareCloudServices.EF.Repository;
using PagedList;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Admin.Controllers
{
    public class InternationalDestinationsController : BaseController
    {
        private InternationalDestinationsRepository _repository = new InternationalDestinationsRepository();

        // GET: InternationalDestinations
        public ActionResult Index(int? page)
        {
            string[] sessionRoles = (string[])Session["Roles"];
            if (!sessionRoles.Contains("Logistics"))
                return Redirect("~/Home");

            else
            { 
                var internationalDestinations = _repository.All.OrderBy(i => i.DestinationPostCode);

                var pageNumber = page ?? 1;
                var pageData = internationalDestinations.ToPagedList(pageNumber, Constants.PAGE_SIZE);

                ViewBag.PageData = pageData;
                return View();

            }
        }

        // GET: InternationalDestinations/Details/5
        public ActionResult Details(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            if (internationalDestination == null)
            {
                return HttpNotFound();
            }
            return View(internationalDestination);
        }

        // GET: InternationalDestinations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InternationalDestinations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DestinationPostCode,DestinationRegion,DestinationAirportCode,DestinationAirportName")] InternationalDestination internationalDestination)
        {
            if (ModelState.IsValid)
            {
                internationalDestination.LastEditedPerson = CurrentUsername;
                internationalDestination.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(internationalDestination, EntityState.Added);
                _repository.Save();
                return RedirectToAction("Index");
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }

            return View(internationalDestination);
        }

        // GET: InternationalDestinations/Edit/5
        public ActionResult Edit(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            if (internationalDestination == null)
            {
                return HttpNotFound();
            }
            return View(internationalDestination);
        }

        // POST: InternationalDestinations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DestinationPostCode,DestinationRegion,DestinationAirportCode,DestinationAirportName")] InternationalDestination internationalDestination)
        {
            if (ModelState.IsValid)
            {
                internationalDestination.LastEditedPerson = CurrentUsername;
                internationalDestination.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(internationalDestination, EntityState.Modified);
                _repository.Save();
                return RedirectToAction("Index");
            }
            return View(internationalDestination);
        }

        // GET: InternationalDestinations/Delete/5
        public ActionResult Delete(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            if (internationalDestination == null)
            {
                return HttpNotFound();
            }
            return View(internationalDestination);
        }

        // POST: InternationalDestinations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InternationalDestination internationalDestination = _repository.Find(id);
            _repository.Delete(internationalDestination);
            _repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
