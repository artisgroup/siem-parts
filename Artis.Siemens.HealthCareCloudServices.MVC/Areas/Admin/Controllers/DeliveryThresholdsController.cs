﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using Artis.Siemens.HealthCareCloudServices.EF.Repository;
using PagedList;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Admin.Controllers
{
    public class DeliveryThresholdsController : BaseController
    {
        private DeliveryThresholdRepository _repository = new DeliveryThresholdRepository();

        // GET: DeliveryThresholds
        public ActionResult Index(int? page)
        {
            string[] sessionRoles = (string[])Session["Roles"];
            if (!sessionRoles.Contains("Logistics") && !sessionRoles.Contains("Service Coordinator"))
                return Redirect("~/Home");

            else
            {
                var deliveryThresholds = _repository.All.OrderBy(i => i.DeliveryType);

                var pageNumber = page ?? 1;
                var pageData = deliveryThresholds.ToPagedList(pageNumber, Constants.PAGE_SIZE);

                ViewBag.PageData = pageData;
                return View();
            }
        }


        // GET: DeliveryThresholds/Details/5
        public ActionResult Details(int id)
        {
            DeliveryThreshold deliveryThreshold = _repository.Find(id);
            if (deliveryThreshold == null)
            {
                return HttpNotFound();
            }
            return View(deliveryThreshold);
        }

        // GET: DeliveryThresholds/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DeliveryThresholds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DeliveryType,MaxGrossWeight,MaxCombinedDimensions,WarningMessage,MaxSingleDimension")] DeliveryThreshold deliveryThreshold)
        {
            if (ModelState.IsValid)
            {
                deliveryThreshold.LastEditedPerson = CurrentUsername;
                deliveryThreshold.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(deliveryThreshold);
                _repository.Save();
                return RedirectToAction("Index");
            }

            return View(deliveryThreshold);
        }

        // GET: DeliveryThresholds/Edit/5
        public ActionResult Edit(int id)
        {
            DeliveryThreshold deliveryThreshold = _repository.Find(id);
            if (deliveryThreshold == null)
            {
                return HttpNotFound();
            }
            return View(deliveryThreshold);
        }

        // POST: DeliveryThresholds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DeliveryType,MaxGrossWeight,MaxCombinedDimensions,WarningMessage,MaxSingleDimension")] DeliveryThreshold deliveryThreshold)
        {
            if (ModelState.IsValid)
            {
                deliveryThreshold.LastEditedPerson = CurrentUsername;
                deliveryThreshold.LastEditedTime = DateTime.Now;
                _repository.InsertOrUpdate(deliveryThreshold, EntityState.Modified);
                _repository.Save();
                return RedirectToAction("Index");
            }
            return View(deliveryThreshold);
        }

        // GET: DeliveryThresholds/Delete/5
        public ActionResult Delete(int id)
        {
            DeliveryThreshold deliveryThreshold = _repository.Find(id);
            if (deliveryThreshold == null)
            {
                return HttpNotFound();
            }
            return View(deliveryThreshold);
        }

        // POST: DeliveryThresholds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DeliveryThreshold deliveryThreshold = _repository.Find(id);
            _repository.Delete(deliveryThreshold);
            _repository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
