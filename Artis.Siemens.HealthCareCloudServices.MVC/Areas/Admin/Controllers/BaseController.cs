﻿using System.Web.Mvc;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Admin.Controllers
{
    public abstract class BaseController : Controller
    {
         protected string CurrentUsername
        {
            get { return Session["LoginId"].ToString(); }
            
        }


    }
}