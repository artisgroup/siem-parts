﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF;
using PagedList;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Admin.Controllers
{
    public class InternationalFlightsCutoffsController : BaseController
    {
        private HCCSContext db = new HCCSContext();

        // GET: InternationalFlightsCutoffs
        public ActionResult Index(int? page)
        {
            string[] sessionRoles = (string[])Session["Roles"];
            if (!sessionRoles.Contains("Logistics"))
                return Redirect("~/Home");

            else
            {           
                var internationalFlightsCutoffs = db.InternationalFlightsCutoffs.ToList().OrderBy(i => i.OriginAirportCode);

                var pageNumber = page ?? 1;
                var pageData = internationalFlightsCutoffs.ToPagedList(pageNumber, Constants.PAGE_SIZE);

                ViewBag.PageData = pageData;
                return View();
            }
        }

        // GET: InternationalFlightsCutoffs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InternationalFlightsCutoff internationalFlightsCutoff = db.InternationalFlightsCutoffs.Find(id);
            if (internationalFlightsCutoff == null)
            {
                return HttpNotFound();
            }
            return View(internationalFlightsCutoff);
        }

        // GET: InternationalFlightsCutoffs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InternationalFlightsCutoffs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //LastEditedTime,LastEditedPerson - removed from screen and controller
        public ActionResult Create([Bind(Include = "Id,OriginAirportCode,OriginAirportName,DestinationAirportCode,DestinationAirportName,FlightNo,CsmlCutoff,DepartureTime,ArrivalTime,AvailableforLocal,EstimatedMetroDelivery,DaysServiced")] InternationalFlightsCutoff internationalFlightsCutoff)
        {
            if (ModelState.IsValid)
            {
                internationalFlightsCutoff.LastEditedPerson = CurrentUsername;
                internationalFlightsCutoff.LastEditedTime = DateTime.Now;
                db.InternationalFlightsCutoffs.Add(internationalFlightsCutoff);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(internationalFlightsCutoff);
        }

        // GET: InternationalFlightsCutoffs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InternationalFlightsCutoff internationalFlightsCutoff = db.InternationalFlightsCutoffs.Find(id);
            if (internationalFlightsCutoff == null)
            {
                return HttpNotFound();
            }
            return View(internationalFlightsCutoff);
        }

        // POST: InternationalFlightsCutoffs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OriginAirportCode,OriginAirportName,DestinationAirportCode,DestinationAirportName,FlightNo,CsmlCutoff,DepartureTime,ArrivalTime,AvailableforLocal,EstimatedMetroDelivery,DaysServiced")] InternationalFlightsCutoff internationalFlightsCutoff)
        {
            if (ModelState.IsValid)
            {
                internationalFlightsCutoff.LastEditedPerson = CurrentUsername;
                internationalFlightsCutoff.LastEditedTime = DateTime.Now;
                db.Entry(internationalFlightsCutoff).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(internationalFlightsCutoff);
        }

        // GET: InternationalFlightsCutoffs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InternationalFlightsCutoff internationalFlightsCutoff = db.InternationalFlightsCutoffs.Find(id);
            if (internationalFlightsCutoff == null)
            {
                return HttpNotFound();
            }
            return View(internationalFlightsCutoff);
        }

        // POST: InternationalFlightsCutoffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InternationalFlightsCutoff internationalFlightsCutoff = db.InternationalFlightsCutoffs.Find(id);
            db.InternationalFlightsCutoffs.Remove(internationalFlightsCutoff);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
