﻿using System.Web.Mvc;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Order
{
    public class OrderAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Order";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Order_default",
                "Order/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional, controller="Home" },
                namespaces: new string[] { "Artis.Siemens.HealthCareCloudServices.MVC.Areas.Order.Controllers" }
            );
        }
    }
}