﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Order.Controllers
{
    public class HomeController : Controller
    {
        // GET: Order/Default
        public ActionResult Index()
        {
            return View();
        }
    }
}