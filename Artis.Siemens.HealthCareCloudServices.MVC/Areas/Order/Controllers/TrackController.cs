﻿using Artis.Siemens.HealthCareCloudServices.MVC.Areas.Admin.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Areas.Order.Controllers
{
    public class TrackController : BaseController
    {
        // GET: Order/Track
        public ActionResult Index()
        {
            ViewBag.LoginId = Session["LoginId"].ToString();
            return View();
        }
    }
}