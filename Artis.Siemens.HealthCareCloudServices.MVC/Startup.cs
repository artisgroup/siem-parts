﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Artis.Siemens.HealthCareCloudServices.MVC
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //app.UseCookieAuthentication(new
            //    CookieAuthenticationOptions
            //    {
            //        AuthenticationType =
            //        DefaultAuthenticationTypes.ApplicationCookie,
            //        AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
            //        LoginPath = new PathString(GetLoginRedirect()),
            //        ExpireTimeSpan = TimeSpan.FromMinutes(30),
            //        SlidingExpiration = true
            //    });

            //app.UseExternalSignInCookie(
            //DefaultAuthenticationTypes.ExternalCookie);
        }

        private string GetLoginRedirect()
        {
            if (ConfigurationManager.AppSettings["InProd"] == "yes")
            {
                return ConfigurationManager.AppSettings["ESUrl"];
            }
            return "/Account/Login";
        }
    }
}
