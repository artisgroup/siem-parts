﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Artis.Siemens.HealthCareCloudServices.Services;
using System.Collections;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers
{
    public class AccountController : Controller
    {
        private MembershipService _membershipService;
    
        public AccountController(MembershipService membershipService)
        {
            _membershipService = membershipService;
        }
        // GET: Account
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult SignOut()
        {
            Session["LoginId"] = null;
            return Redirect("~/");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(string username, string password)
        {
            if (username == "siemens.au" && password == "super5ecR3t")
            {
                Session["LoginId"] = "suman";
                Session["DisplayName"] = "Suman" + " " + "Desu";
                Session["Firstname"] = "Suman";
                Session["Lastname"] = "test";
                Session["Email"] = "suman@test.com";


                //var checkkUser = _membershipService.GetUserByLoginName(Session["LoginId"].ToString());

                //if (checkkUser == null)
                //{
                //    AspNetUserVM aspnetuser = new AspNetUserVM();
                //    aspnetuser.UserName = "suman1";
                //    aspnetuser.Email = "test@test.com";
                //    aspnetuser.Firstname = "firstname";
                //    aspnetuser.Lastname = "lastname";

                //    var aspnetvm = _membershipService.Create(aspnetuser);
                //    Session["LoginId"] = "suman1";
                //}

                var userRoles = _membershipService.GetUserByLoginName(Session["LoginId"].ToString());

                string[] roles = new string[userRoles.Roles.Length];
                for (int i = 0; i < userRoles.Roles.Length; i++)
                {
                    roles[i] = userRoles.Roles[i].Name.ToString();
                }
                Session["Roles"] = roles;
                
                return Redirect("~/Home");
            }
            else
            {
                ModelState.AddModelError("", "Invalid username or password.");
            }

            return View();
        }
    }
}