﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Artis.Siemens.HealthCareCloudServices.Services;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers.API
{
    [RoutePrefix("api/ordertrack")]
    public class OrderTrackController : BaseAPIController
    {
        private OrderTrackService _orderTrackService;

        public OrderTrackController(OrderTrackService ot)
        {
            _orderTrackService = ot;

        }

        [HttpPost, Route("getTrackDeliveries")]
        public IHttpActionResult GetTrackDeliveries(OrderTrackPostVM postData)
        {
            IList<OrderTrackVM> orderDeliveries = new List<OrderTrackVM>();

            var deliveries = _orderTrackService.GetTrackDeliveries(postData);

            if (deliveries != null)
            {
                foreach (var d in deliveries)
                {
                    var otVM = new OrderTrackVM();
                    otVM.OrderId = d.OrderId;
                    otVM.Status = d.Status;
                    otVM.Destination = d.Destination;
                    otVM.System = d.System;
                    otVM.PurchaseOrderNumber = d.PurchaseOrderNumber;
                    otVM.Requisitioner = d.Requisitioner;
                    otVM.ServiceLevel = d.ServiceLevel;
                    otVM.DeliveryDate = d.DeliveryDate;
                    otVM.Origin = d.Origin;
                    otVM.ConNotes = d.ConNotes;
                    otVM.LastEvent = d.LastEvent;
                    otVM.Alerts = d.Alerts;
                    otVM.HotSite = d.HotSite;
                    otVM.CustomerSite = d.CustomerSite;
                    otVM.CustomerRoom = d.CustomerRoom;
                    otVM.CustomerDepartment = d.CustomerDepartment;
                    otVM.Street = d.Street;
                    otVM.City = d.City;
                    otVM.PostCode = d.PostCode;
                    otVM.DestPostCode = d.DestPostCode;
                    otVM.Region = d.Region;
                    otVM.DestRegion = d.DestRegion;
                    otVM.Country = d.Country;
                    otVM.EffectCode = d.EffectCode;
                    otVM.ServiceLevel = d.ServiceLevel;
                    otVM.ServiceLevelDesc = d.ServiceLevelDesc;
                    otVM.EventComment = d.EventComment;
                    otVM.ServiceLevelType = d.ServiceLevelType;
                    otVM.CustomerName = d.CustomerName;
                    otVM.Tailgate = d.Tailgate;
                    otVM.WarehousePriority = d.WarehousePriority;
                    otVM.Cost = d.Cost;

                    orderDeliveries.Add(otVM);
                }
            }

            return Ok(orderDeliveries.OrderBy(od => od.DeliveryDate));
        }

        [HttpGet, Route("getPartsOrdered")]
        //public IHttpActionResult GetPartsOrdered(string purchaseOrder, string conNote)
        //{
        //    IList<OrderItemVM> orderItems = new List<OrderItemVM>();
        //    var partsOrdered = _orderTrackService.GetPartsOrdered(purchaseOrder, conNote);
        public IHttpActionResult GetPartsOrdered(string purchaseOrder)
        {
            IList<OrderItemVM> orderItems = new List<OrderItemVM>();
            var partsOrdered = _orderTrackService.GetPartsOrdered(purchaseOrder);

            if (partsOrdered != null)
            {
                foreach (var p in partsOrdered)
                {
                    var oiVM = new OrderItemVM();

                    oiVM.Item = p.RowNo;
                    oiVM.Material = p.MaterialNo;
                    oiVM.Description = p.Description;
                    oiVM.Batch = p.Batch;
                    oiVM.Qty = p.Qty;
                    oiVM.SchenkerDEL = p.SchenkerDEL;
                    oiVM.SchenkerSHP = p.SchenkerSHP;

                    orderItems.Add(oiVM);
                }
            }

            return Ok(orderItems.OrderBy(o => o.Item));

        }

        [HttpGet, Route("getTransportEvents")]
        //public IHttpActionResult GetTransportEvents(string purchaseOrder, string conNote)
        //{
        //    IList<OrderTrackEventVM> trackEvents = new List<OrderTrackEventVM>();
        //    var statusUpdates = _orderTrackService.GetTransportEvents(purchaseOrder, conNote);
        public IHttpActionResult GetTransportEvents(string purchaseOrder)
        {
            IList<OrderTrackEventVM> trackEvents = new List<OrderTrackEventVM>();
            var statusUpdates = _orderTrackService.GetTransportEvents(purchaseOrder);
            if (statusUpdates != null)
            {
                foreach (var su in statusUpdates)
                {
                    var oteVM = new OrderTrackEventVM();

                    oteVM.EventDateTime = su.EventDateTime;
                    //oteVM.EventDateTime = su.MessageDateTime;
                    oteVM.EventLocation = su.EventLocation;
                    oteVM.ReasonCode = su.ReasonCode;
                    oteVM.EventComment = su.EventComment;
                    oteVM.ConNote = su.ConNote;
                    oteVM.ItemNo = su.ItemNo;
                    //to include tracking URL
                    oteVM.TrackingURL = su.TrackingURL;

                    trackEvents.Add(oteVM);
                }
            }

            return Ok(trackEvents.OrderBy(te => te.EventDateTime));

        }

    }
}
