﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using Artis.Siemens.HealthCareCloudServices.Services;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using System.Collections;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers.API
{
    [RoutePrefix("api/orderentry")]
    public class OrderEntryController : BaseAPIController
    {
        private OrderEntryService _orderEntryService;
        private MaterialMasterService _mmService;

        public OrderEntryController(OrderEntryService orderEntryService, MaterialMasterService mmService)
        {
            _orderEntryService = orderEntryService;
            _mmService = mmService;
        }

        [HttpGet, Route("searchByMaterialNumber")]
        public IHttpActionResult SearchByMaterialNumber(string number)
        {
            return OK(_mmService.GetOrderItem(number));
        }

        [HttpPost, Route("checkWeightWarning")]
        public IHttpActionResult CheckWeightWarning(OrderItemVM[] items)
        {
            return OK(_orderEntryService.CheckWeightWarning(items));
        }

        [HttpPost, Route("validateOrderHeader")]
        public IHttpActionResult ValidateOrderHeader(SAPOrderVM order)
        {
            return OK(_orderEntryService.ValidateSAPOrder(order));
        }
        // getOriginLocation
        [HttpPost, Route("getOriginLocation")]
        public IHttpActionResult GetOriginLocation(SAPOrderVM order)
        {
            return OK(_orderEntryService.GetOriginLocation(order.DocType, order.SupplyingSloc));
        }

        [HttpGet, Route("getCustomerSite")]
        public IHttpActionResult GetCustomerSite(string notificationNumber)
        {
            return Ok(_orderEntryService.GetCustomerSite(notificationNumber));
        }

        [HttpGet, Route("getDropPoint")]
        public IHttpActionResult GetDropPoint(string notificationNumber)
        {
            return Ok(_orderEntryService.GetDropPoint(notificationNumber));
        }

        [HttpGet, Route("getEmployeeById")]
        public IHttpActionResult GetEmployee(string empNo)
        {
            return Ok(_orderEntryService.GetEmployeeById(empNo));
        }

        [HttpGet, Route("getEmployeeBySloc")]
        public IHttpActionResult GetEmployeeBySloc(string sloc)
        {
            return Ok(_orderEntryService.GetEmployeeBySloc(sloc));
        }

        [HttpPost, Route("create")]
        public IHttpActionResult Create(OrderVM order)
        {
            var now = DateTime.Now;
            if (order.Id != null)
            {
                order.CreatedBy = CurrentUsername;
                order.CreatedOn = now;
            }
            order.ModifiedBy = CurrentUsername;
            order.ModifiedOn = now;

            var batchNumber = string.Empty;

            foreach (var orderItem in order.Items)
            {
                if (orderItem.CreatedOn == null)
                    orderItem.Id = 0;

                orderItem.CreatedBy = "";
                orderItem.CreatedOn = now;
                orderItem.ModifiedBy = "";
                orderItem.ModifiedOn = now;

                //make sure to save the batch number for the order
                if (orderItem.Batch != null)
                    batchNumber = orderItem.Batch;

                //if any don't have a batchnumber, assign it
                if (orderItem.Batch == null && batchNumber != null)
                    orderItem.Batch = batchNumber;
            }
            return Ok(_orderEntryService.Create(order));
        }

        [HttpGet, Route("searchCS")]
        public IHttpActionResult SearchCS(string query)
        {
            return OK(_orderEntryService.SearchCS(query));

        }

        [HttpGet, Route("searchDP")]
        public IHttpActionResult SearchDP(string query)
        {
            return OK(_orderEntryService.SearchDP(query));
        }


        [HttpPost, Route("submitSAPOrders")]
        public IHttpActionResult SubmitSAPOrders(SAPOrderVM[] orders)
        {
            IList<OrderItemVM> orderItems = new List<OrderItemVM>();
            int orderLineItem = 10;
            foreach (var order in orders)
            {
                var materialItem = _mmService.GetOrderItem(order.Material);
                materialItem.Item = order.Item;
                materialItem.Qty = Convert.ToInt32(order.Quantity);
                materialItem.Batch = order.Batch;
                orderLineItem += 10;
                orderItems.Add(materialItem);
            }

            return Ok(orderItems.OrderBy(o => o.Item));


        }
    }
}
