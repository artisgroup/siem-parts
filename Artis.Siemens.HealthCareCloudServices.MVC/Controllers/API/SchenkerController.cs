﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.Services;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using System.Configuration;
using System.Text;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers.API
{
    [RoutePrefix("vendor/schenker")]
    [AllowAnonymous]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SchenkerController : BaseAPIController
    {
        private SchenkerService _schenkerService;

        public SchenkerController(SchenkerService schenkerService)
        {
            _schenkerService = schenkerService;
        }


        [HttpPost, Route("OrderStatusUpdate")]
        [AllowAnonymous]
        public IHttpActionResult OrderStatusUpdate(SchenkerStatusUpdate orderUpdate)
        {
            string function = string.Empty;
            
            try
            {
                function = "LogOrderStatusUpdate";
                _schenkerService.LogOrderStatusUpdate(orderUpdate);

                //need to also process the response - and tag the order item with the values
                function = "UpdateOrderItem";
                _schenkerService.UpdateOrderItem(orderUpdate);

                //function = "EmailNotify";
                //_schenkerService.EmailNotify(orderUpdate);

                function = "PosttoZapier";
                _schenkerService.PushOrdertoZapier();

                PosttoZapier();

                return Ok("Order Status Update Received");

            }
            catch(Exception ex)
            {
                throw new Exception("ERROR with " + function + ".    " + ex.Message + " || " + ex.StackTrace);
            }
        }

        [HttpPost, Route("PosttoZapier")]
        [AllowAnonymous]
        public IHttpActionResult PosttoZapier()
        {
            string function = string.Empty;
            string ZapierResponse;
             var ZapierData = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/app_data/ZapierData.json"));
            SchenkerStatusUpdate orderupdate = new SchenkerStatusUpdate();
            try
            {  
                function = "PosttoZapier";
                orderupdate = _schenkerService.PushOrdertoZapier();

                var zapierJSON = _schenkerService.GetOrderforZapier(ZapierData);
                ZapierResponse = ZapOrder(zapierJSON);
                return Ok("Order Status Update Received");

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR with " + function + ".    " + ex.Message + " || " + ex.StackTrace);
            }
        }
        
        private string ZapOrder(string zapierJSON)
        {
            HttpWebRequest req = null;
            Stream reqStream = null;
            HttpWebResponse resp = null;
            Stream respStream = null;
            string response = string.Empty;

            try
            {
                req = WebRequest.Create(ConfigurationManager.AppSettings["Zapier_Post_URL"]) as HttpWebRequest;
                req.KeepAlive = false;
                req.Method = "POST";
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] bytes = encoding.GetBytes(zapierJSON);                
                req.ContentType = "application/json";
                reqStream = req.GetRequestStream();
                reqStream.Write(bytes, 0, bytes.Length);

                try
                {
                    resp = req.GetResponse() as HttpWebResponse;
                    response = resp.ToString();
                }
                catch (WebException ex)
                {
                    resp = (HttpWebResponse)ex.Response;
                    response = ex.Message;
                }

                respStream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(respStream);
                response = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            finally
            {
                if (reqStream != null)
                {
                    reqStream.Close();
                }
                if (resp != null)
                {
                    resp.Close();
                }
                if (respStream != null)
                {
                    respStream.Close();
                }
            }
            return response;
        }

        [HttpGet, Route("GetOrderDetails/{PONumber},{POLine}")]
        [AllowAnonymous]
        public IHttpActionResult GetOrderDetailsZapier(string PONumber, string POLine)
        {
            var order = new OrderVM();

            order = _schenkerService.GetOrderDetailsZapier(PONumber, POLine);
            return OK(order);
        }
        
        [HttpGet, Route("GetDetailsZMON/{NotificationNumber}")]
        [AllowAnonymous]
        public IHttpActionResult GetDetailsZMON(string NotificationNumber)
        {
            var zmon = new ZMON();

            zmon = _schenkerService.GetDetailsZMON(NotificationNumber);
            return OK(zmon);
        }
        [HttpGet, Route("GetIBASEDetails/{Requisitioner}")]
        [AllowAnonymous]
        public IHttpActionResult GetIBASEDetails(string Requisitioner)
        {
            var ibase = new IBase();

            ibase = _schenkerService.GetIBASEDetails(Requisitioner);
            return OK(ibase);
        }
    }
}
