﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Artis.Siemens.HealthCareCloudServices.Services;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers.API
{
    [RoutePrefix("api/employee")]
    public class EmployeeController : BaseAPIController
    {
        private EmployeeService _empService;

        public EmployeeController(EmployeeService empService)
        {
            _empService = empService;
        }
        
        [HttpGet,Route("getList")]
        public IHttpActionResult GetList()
        {
            return OK(_empService.GetActiveEmployees());
        }

        [HttpGet, Route("getById")]
        public IHttpActionResult GetById(string id)
        {
            return Ok(_empService.GetById(id));
        }

        [HttpGet, Route("searchEA")]
        public IHttpActionResult SearchEA(string query)
        {
            return OK(_empService.SearchEmp(query));
        }
       
    }
}
