﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Artis.Siemens.HealthCareCloudServices.Services;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using System.Configuration;
using System.Text;
using Artis.Siemens.HealthCareCloudServices.EF;
using System.Web;


namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers.API
{
    [RoutePrefix("api/orderconfirm")]
    public class OrderConfirmController : BaseAPIController
    {

        private OrderConfirmService _oc;
        private SchenkerService _schenkerService;
        

        public OrderConfirmController(OrderConfirmService oc, SchenkerService schenkerService)
        {
            _oc = oc;
            _schenkerService = schenkerService;
           
        }

        [HttpGet, Route("getOrderDetail")]
        public IHttpActionResult GetOrderDetail(int orderId)
        {
            return Ok(_oc.GetOrderById(orderId));
        }

        [HttpGet, Route("getTimeForRegion")]
        public IHttpActionResult GetTimeForRegion(string region)
        {
            return Ok(DateTimeService.GetTimeForRegion(region));
        }

        [HttpGet, Route("getGenericServices")]
        public IHttpActionResult GetGenericServices()
        {
            return Ok(_oc.GetGenericServices());
        }

        [HttpPost, Route("submitOrder")]
        public IHttpActionResult SubmitOrder(SchenkerOrderPostVM postData)
        {
            string schenkerResponse;
            
            var orderTemplate = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/app_data/orderTemplate.xml"));
            var orderLinesTemplate = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/app_data/orderLinesTemplate.xml"));
            
            string userFName = HttpContext.Current.Session["Firstname"].ToString();
            string userLName = HttpContext.Current.Session["Lastname"].ToString();
            string userEmail = HttpContext.Current.Session["Email"].ToString();
            
            var orderXML = _schenkerService.GetOrderXml(orderTemplate, 
                                                        orderLinesTemplate, 
                                                        postData.OrderId, 
                                                        postData.ServiceLevelCode,
                                                        postData.WarehousePriority,
                                                        postData.SiemensContactName,
                                                        postData.SiemensContactPhone,
                                                        userFName, 
                                                        userLName, 
                                                        userEmail, 
                                                        postData.Cost
                                                       );

            schenkerResponse = PostToSchenker(orderXML);

            // Save back datetime and response message 
            _schenkerService.StatusUpdate(postData.OrderId, postData.ServiceLevelCode, postData.ServiceLevelDescription, orderXML, schenkerResponse, postData.WarehousePriority, postData.Cost);

            PostOrdertoZapier(postData);

            return Ok(orderXML);
        }
        [HttpPost, Route("PostOrdertoZapier")]
        [AllowAnonymous]
        public IHttpActionResult PostOrdertoZapier(SchenkerOrderPostVM postData)
        {
            string zapierResponse;
            var orderTemplateZapier = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/app_data/orderTemplate.json"));
            var orderLinesTemplateZapier = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/app_data/orderLinesTemplate.json"));

            string userFName = HttpContext.Current.Session["Firstname"].ToString();
            string userLName = HttpContext.Current.Session["Lastname"].ToString();
            string userEmail = HttpContext.Current.Session["Email"].ToString();

            var orderJson = _schenkerService.GetOrderJson(orderTemplateZapier,
                                                        orderLinesTemplateZapier,
                                                        postData.OrderId,
                                                        postData.ServiceLevelCode,
                                                        postData.WarehousePriority,
                                                        postData.SiemensContactName,
                                                        postData.SiemensContactPhone,
                                                        userFName,
                                                        userLName,
                                                        userEmail,
                                                        postData.Cost
                                                       );
            string function = string.Empty;
            
            try
            {
                function = "PosttoZapier";

                zapierResponse = PostToZapierOrder(orderJson);
                return Ok("Order Status Update Received");

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR with " + function + ".    " + ex.Message + " || " + ex.StackTrace);
            }
        }

        private string PostToSchenker(string xml)
        {
            HttpWebRequest req = null;
            Stream reqStream = null;
            HttpWebResponse resp = null;
            Stream respStream = null;
            string response = string.Empty;

            try
            {
                req = WebRequest.Create(ConfigurationManager.AppSettings["Schenker_Post_URL"]) as HttpWebRequest;
                req.KeepAlive = false;
                req.Method = "POST";
                byte[] buffer = Encoding.UTF8.GetBytes(xml);
                req.ContentLength = buffer.Length;
                req.ContentType = "text/xml";
                reqStream = req.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);

                try
                {
                    resp = req.GetResponse() as HttpWebResponse;
                    response = resp.ToString();
                }
                catch (WebException ex)
                {
                    resp = (HttpWebResponse)ex.Response;
                    response = ex.Message;
                }

                respStream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(respStream);
                response = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            finally
            {
                if (reqStream != null)
                {
                    reqStream.Close();
                }
                if (resp != null)
                {
                    resp.Close();
                }
                if (respStream != null)
                {
                    respStream.Close();
                }
            }
            return response;
        }

        private string PostToZapierOrder(string json)
        {
            HttpWebRequest req = null;
            Stream reqStream = null;
            HttpWebResponse resp = null;
            Stream respStream = null;
            string response = string.Empty;

            try
            {
                req = WebRequest.Create(ConfigurationManager.AppSettings["ZapierOrder_Post_URL"]) as HttpWebRequest;
                req.KeepAlive = false;
                req.Method = "POST";
                byte[] buffer = Encoding.UTF8.GetBytes(json);
                req.ContentLength = buffer.Length;
                req.ContentType = "application/json";
                reqStream = req.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);

                try
                {
                    resp = req.GetResponse() as HttpWebResponse;
                    response = resp.ToString();
                }
                catch (WebException ex)
                {
                    resp = (HttpWebResponse)ex.Response;
                    response = ex.Message;
                }

                respStream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(respStream);
                response = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            finally
            {
                if (reqStream != null)
                {
                    reqStream.Close();
                }
                if (resp != null)
                {
                    resp.Close();
                }
                if (respStream != null)
                {
                    respStream.Close();
                }
            }
            return response;
        }
    }
}
