﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Artis.Siemens.HealthCareCloudServices.MVC.Controllers.API
{
    public class BaseAPIController : ApiController
    {
        public IHttpActionResult BadRequest(object response = null)
        {
            return ResponseResult(this, HttpStatusCode.BadRequest, response);
        }

        public IHttpActionResult OK(object response = null)
        {
            return ResponseResult(this, HttpStatusCode.OK, response);
        }

        public IHttpActionResult Unauthorized()
        {
            return StatusCode(HttpStatusCode.Unauthorized);
        }

        private ResponseActionResult ResponseResult(ApiController controller, HttpStatusCode statusCode, object value)
        {
            return new ResponseActionResult(controller.Request, statusCode, value);
        }

        protected string CurrentUsername
        {
            get{return HttpContext.Current.Session["LoginId"].ToString();}
            //get {return HttpContext.Current.User.Identity.Name; }
            //get { return "TBD"; }
            
        }
    }

    public class ResponseActionResult : IHttpActionResult
    {
        public HttpRequestMessage Request { get; private set; }
        public object Value { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        public ResponseActionResult(HttpRequestMessage request, HttpStatusCode statusCode, object value)
        {
            this.Request = request;
            this.StatusCode = statusCode;
            this.Value = value;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(ExecuteResult());
        }

        public HttpResponseMessage ExecuteResult()
        {
            return this.Request.CreateResponse(this.StatusCode, this.Value);
        }
    }
}
