﻿using System;
using System.Data.Entity;
using System.Linq;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class DropPointsRepository : IDropPointsRepository
    {
        private readonly HCCSContext _dbContext;


        public DropPointsRepository()
        {
            _dbContext = new HCCSContext();
        }


        public IQueryable<DropPoint> All
        {
            get { return _dbContext.DropPoints; }
        }



        public IQueryable<DropPoint> GetDropPoints()
        {
            var dropPoints = from dp in _dbContext.DropPoints
                             select dp;

            return dropPoints;
        }


        public DropPoint Find(int id)
        {
            var dropPoint = _dbContext.DropPoints.Find(id);
            return dropPoint;
        }

        public void InsertOrUpdate(DropPoint entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var dropPoint = _dbContext.DropPoints.Find(id);
            if (dropPoint != null)
            {
                _dbContext.DropPoints.Remove(dropPoint);
            }
        }

        public void Delete(DropPoint dropPoint)
        {
            if (dropPoint != null)
            {
                _dbContext.DropPoints.Remove(dropPoint);
            }
        }

        public void Save()
        {
            //  insert the last time/person details
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
