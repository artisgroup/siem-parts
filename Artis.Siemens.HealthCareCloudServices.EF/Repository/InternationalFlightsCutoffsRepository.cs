﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class InternationalFlightsCutoffsRepository : IInternationalFlightsCutoffsRepository
    {

        private readonly HCCSContext _dbContext;


        public InternationalFlightsCutoffsRepository()
        {
            _dbContext = new HCCSContext();
        }


        public IQueryable<InternationalFlightsCutoff> All
        {
            get { return _dbContext.InternationalFlightsCutoffs; }
        }




        public InternationalFlightsCutoff Find(int id)
        {
            var internationalFlightsCutoff = _dbContext.InternationalFlightsCutoffs.Find(id);
            return internationalFlightsCutoff;
        }

        public void InsertOrUpdate(InternationalFlightsCutoff entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var internationalFlightsCutoff = _dbContext.InternationalFlightsCutoffs.Find(id);
            if (internationalFlightsCutoff != null)
            {
                _dbContext.InternationalFlightsCutoffs.Remove(internationalFlightsCutoff);
            }
        }

        public void Delete(InternationalFlightsCutoff internationalFlightsCutoff)
        {
            if (internationalFlightsCutoff != null)
            {
                _dbContext.InternationalFlightsCutoffs.Remove(internationalFlightsCutoff);
            }
        }

        public void Save()
        {
            //  insert the last time/person details
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
