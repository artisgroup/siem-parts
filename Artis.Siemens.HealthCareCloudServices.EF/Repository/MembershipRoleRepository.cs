﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class MembershipRoleRepository  : IMembershipRoleRepository
    {


        public AspNetUserVM GetUserByLoginName(string loginName)
        {
            var aspnetUserVM = new AspNetUserVM();
          
            using (var dbContext = new HCCSContext())
            {
                var aspnetUser = dbContext.AspNetUsers.Where(u => u.UserName == loginName).FirstOrDefault();
                aspnetUserVM = Mapper.Map<AspNetUserVM>(aspnetUser);
                //var roles = dbContext.AspNetUserRoles.Where(r => r.UserId == aspnetUser.Id).ToList();
                
            }
            return aspnetUserVM;
        }

        public AspNetUser Create(AspNetUser aspnetuser)
        {
            using (var dbContext = new HCCSContext())
            {
                var fieldStaff = dbContext.AspNetRoles.Where(role => role.Name == "Field Staff").FirstOrDefault();


                dbContext.Entry<AspNetUser>(aspnetuser).State = EntityState.Added;
                dbContext.SaveChanges();
                aspnetuser.Roles.Add(fieldStaff);
                dbContext.Entry(aspnetuser).State = EntityState.Modified;
                dbContext.SaveChanges();
               
                

                return aspnetuser;
            }
        }

       

        public IQueryable<AspNetUserVM> All
        {
            get {
                throw new NotImplementedException();    //IQueryable<AspNetUserVM> All
            }
        }

        public AspNetUserVM Find(int id)
        {
            throw new NotImplementedException();    //Find(int id)
        }

        public void InsertOrUpdate(AspNetUserVM entity, EntityState entityState)
        {
            throw new NotImplementedException();    //InsertOrUpdate
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();    //Delete(int id)
        }

        public void Save()
        {
            throw new NotImplementedException();    //Save()
        }

        public void Dispose()
        {
            throw new NotImplementedException();    //Dispose()
        }
    }
}