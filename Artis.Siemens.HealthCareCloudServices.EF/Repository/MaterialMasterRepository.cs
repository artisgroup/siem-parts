﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class MaterialMasterRepository : IMaterialMasterRepository
    {
        #region Not Implemented

        public IQueryable<MaterialMaster> All
        {
            get
            {
                throw new NotImplementedException();    //IQueryable<MaterialMaster> All 
            }
        }

        public MaterialMaster Find(int id)
        {
            throw new NotImplementedException();    //Find(int id)
        }

        public void InsertOrUpdate(MaterialMaster entity, EntityState entityState)
        {
            throw new NotImplementedException();    //InsertOrUpdate
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();    //Delete(int id)
        }

        public void Save()
        {
            throw new NotImplementedException();    //Save()
        }

        public void Dispose()
        {
            throw new NotImplementedException();    //Dispose()
        }

        #endregion

        public MaterialMaster GetByMaterialNo(string materialNo)
        {
            MaterialMaster matMaster = null;

            using (var dbContext = new HCCSContext())
            {
                if (materialNo != null)
                {
                    materialNo = materialNo.TrimStart('0');
                    matMaster = dbContext.Set<MaterialMaster>().Where(material => material.Id.Substring(SqlFunctions.PatIndex("%[^0]%", material.Id).Value - 1) == materialNo).FirstOrDefault();
                }

            }
            return matMaster;
        }
    }
}
