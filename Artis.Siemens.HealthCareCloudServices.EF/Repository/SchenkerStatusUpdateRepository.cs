﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;
using System.Globalization;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class SchenkerStatusUpdateRepository : ISchenkerStatusUpdateRepository
    {
        #region PUBLIC METHODS

        public IQueryable<SchenkerStatusUpdate> All
        {
            get
            {
                throw new NotImplementedException();    //IQueryable<SchenkerStatusUpdate> All
            }
        }

        public SchenkerOrderVM GetOrderDetails(string serviceLevelCode, int orderId, string warehousePriority, string siemensContactName, string siemensContactPhone, string userFName, string userLName, string userEmail, string Cost)
        {

            var soVM = new SchenkerOrderVM();
            var orderVM = GetOrderVMById(orderId);

            soVM.STOPONumber = orderVM.PurchasingDoc;
            soVM.ExpectedDeliveryDate = orderVM.DeliveryDate;
            soVM.UserFirstName = userFName;
            soVM.UserLastName = userLName;
            soVM.UserEmail = userEmail;
            soVM.NotificationNumber = orderVM.Requisitioner;

            //shouldn't pull the time from ETA - but from the front screen instead
            //if (eta != string.Empty)
            //{
            //    DateTime dt = Convert.ToDateTime(eta.Replace("-", " "), new CultureInfo("en-AU"));
            //    soVM.DeliveryTime = dt.ToString("HH:mm");
            //}
            ////for timezone settings
            DateTime date;               
            TimeZoneInfo tst;
            bool isDaylight;

            if (orderVM.Region.ToUpper().Trim() == "ACT" || orderVM.Region.ToUpper().Trim() == "VIC" || orderVM.Region.ToUpper().Trim() == "NSW" || orderVM.Region.ToUpper().Trim() == "TAS")
            {
                date = Convert.ToDateTime(string.Format("{0} {1}", orderVM.DeliveryDate, orderVM.DeliveryTime), new CultureInfo("en-AU"));
                tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                isDaylight = tst.IsDaylightSavingTime(date);
                if (isDaylight == true)
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+11:00";
                }
                else
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+10:00";
                }
            }
            else if (orderVM.Region.ToUpper().Trim() == "QLD")
            {
                date = Convert.ToDateTime(string.Format("{0} {1}", orderVM.DeliveryDate, orderVM.DeliveryTime), new CultureInfo("en-AU"));
                tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                isDaylight = tst.IsDaylightSavingTime(date);
                if (isDaylight == true)
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+10:00";
                }
                else
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+10:00";
                }
            }
            else if (orderVM.Region.ToUpper().Trim() == "SA")
            {
                date = Convert.ToDateTime(string.Format("{0} {1}", orderVM.DeliveryDate, orderVM.DeliveryTime), new CultureInfo("en-AU"));
                tst = TimeZoneInfo.FindSystemTimeZoneById("Cen. Australia Standard Time");
                isDaylight = tst.IsDaylightSavingTime(date);
                if (isDaylight == true)
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+10:30";
                }
                else
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+09:30";
                }
            }

            else if (orderVM.Region.ToUpper().Trim() == "NT")
            {
                date = Convert.ToDateTime(string.Format("{0} {1}", orderVM.DeliveryDate, orderVM.DeliveryTime), new CultureInfo("en-AU"));
                tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                isDaylight = tst.IsDaylightSavingTime(date);
                if (isDaylight == true)
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+09:30";
                }
                else
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+09:30";
                }
            }
            else if (orderVM.Region.ToUpper().Trim() == "WA")
            {
                date = Convert.ToDateTime(string.Format("{0} {1}", orderVM.DeliveryDate, orderVM.DeliveryTime), new CultureInfo("en-AU"));
                tst = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                isDaylight = tst.IsDaylightSavingTime(date);
                if (isDaylight == true)
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+08:00";
                }
                else
                {
                    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+08:00";
                }
            }
            else
            {
                soVM.DeliveryTime = orderVM.DeliveryTime;
            }
            
             

            
            //if (orderVM.Region == "ACT" || orderVM.Region == "VIC" || orderVM.Region == "NSW" || orderVM.Region == "QLD" || orderVM.Region == "TAS")
            //{
            //    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+10:00";
            //}
            //else if (orderVM.Region == "NT" || orderVM.Region == "SA")
            //{
            //    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+09:30";
            //}
            //else if (orderVM.Region == "WA")
            //{
            //    soVM.DeliveryTime = orderVM.DeliveryTime + " UTC+08:00";
            //}
            //soVM.DeliveryTime = orderVM.DeliveryTime;
            soVM.ServiceLevelCode = serviceLevelCode;
            soVM.DeliveryType = orderVM.DeliveryType;
            soVM.NotificationNumber = orderVM.Requisitioner;
            soVM.WarehousePriority = warehousePriority;
            soVM.SiemensContactName = siemensContactName;
            soVM.SiemensContactPhone = siemensContactPhone;

            //include Cost
            if (Cost != null)
                soVM.Cost = Cost;
            else
                soVM.Cost = "0.00";

            GetAddress(soVM, orderVM);

            foreach (var item in orderVM.Items)
            {
                var soItemVM = new SchenkerOrderItemVM();

                soItemVM.STOPOLineNumber = item.Item;
                soItemVM.SAPMaterialNumber = item.Material.TrimStart('0');
                soItemVM.MaterialDescription = item.Description;
                soItemVM.Plant = soVM.Plant;
                soItemVM.StorageLocation = orderVM.SLoc;
                soItemVM.OrderQty = item.Qty.ToString();
                //soItemVM.UOM = item.Dim.Split(' ')[1];
                soItemVM.UOM = item.UOM;
                soItemVM.BatchNumber = item.Batch;
                soVM.OrderItems.Add(soItemVM);
            }

            return soVM;

        }
        //public bool IsDeliveryCode(string code)
        //{
        //    bool returnCode = false;

        //    using (var dbContext = new HCCSContext())
        //    {
        //        var dc = dbContext.DeliveryCodes.Where(o => o.CarrierStatusCode == code).FirstOrDefault();
        //        if (dc != null)
        //        {
        //            returnCode = dc.IsDeliveryCode;
        //        }
        //    }

        //    return returnCode;
        //}
        //public bool IsETACode(string code)
        //{
        //    bool returnCode = false;

        //    using (var dbContext = new HCCSContext())
        //    {
        //        var dc = dbContext.DeliveryCodes.Where(o => o.CarrierStatusCode == code).FirstOrDefault();
        //        if (dc != null)
        //        {
        //            returnCode = dc.IsETACode;
        //        }
        //    }

        //    return returnCode;
        //}

        public void UpdateSchenkerStatus(int id, string serviceLevelCode, string serviceLevelDescription, string xml, string resp, string warehousePriority, string cost)
        {

            using (var dbContext = new HCCSContext())
            {
                var order = dbContext.Orders.Where(o => o.Id == id).FirstOrDefault();
                if (order != null)
                {
                    order.Id = id;
                    order.SchenkerTimestamp = System.DateTime.Now;
                    order.SchenkerResponse = resp;
                    order.SchenkerXML = xml;
                    order.ServiceLevelCode = serviceLevelCode;
                    order.ServiceLevelDescription = serviceLevelDescription;
                    //inlcude warehousepriority and cost
                    if (warehousePriority == null)
                        order.WarehousePriority = "No";
                    else 
                        order.WarehousePriority = warehousePriority;

                    order.Cost = cost;

                    dbContext.Entry(order).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }
            }
        }

        public void UpdateOrderItem(string purchaseOrderNumber, string itemNo, string conNote, string eventCode, DateTime? eventDateTime)
        {
            using (var dbContext = new HCCSContext())
            {
                //get the order for the PO number first
                var order = dbContext.Orders.Where(o => o.PONumber == purchaseOrderNumber).FirstOrDefault();
                if (order != null)
                {
                    //convert from 00010 to 10 (for example)
                    int intItemNo = 0;
                    if (int.TryParse(itemNo, out intItemNo))
                        itemNo = intItemNo.ToString();

                    //get the order item to update
                    var orderItem = dbContext.OrderItems.Where(i => i.OrderId == order.Id && i.RowNo == itemNo).FirstOrDefault();
                    if (orderItem != null)
                    {
                        //set the event code + messagedatetime + connote #
                        orderItem.SchenkerDeliveryStatus = eventCode;
                        orderItem.SchenkerDeliveryDateTime = eventDateTime;
                        orderItem.SchenkerDeliveryConNote = conNote;

                        if (eventCode == "SHP") orderItem.SchenkerSHP = "Y";
                        if (eventCode == "DEL") orderItem.SchenkerDEL = "Y";

                        dbContext.Entry(orderItem).State = EntityState.Modified;
                        dbContext.SaveChanges();
                    }
                }

            }
        }

        public void InsertOrUpdate(SchenkerStatusUpdate entity, EntityState entityState)
        {
            //_dbContext.Entry(entity).State = entityState;

        }

        public void Insert(SchenkerStatusUpdate entity)
        {
            using (var dbContext = new HCCSContext())
            {
                try
                {
                    entity.MessageDateTime = DateTime.Now;
                    dbContext.Entry<SchenkerStatusUpdate>(entity).State = EntityState.Added;
                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public SchenkerStatusUpdate PushtoZapierOrder()
        {
            var data = new SchenkerStatusUpdate();
            using (var dbContext = new HCCSContext())
            {
                data = dbContext.Set<SchenkerStatusUpdate>().OrderByDescending(a => a.Id).FirstOrDefault();
                if (data != null) return data;
            }
            return data;            
        }
        public OrderVM GetOrderDetailsZapier(string orderid, string POLine)
        {
            using (var dbContext = new HCCSContext())
            {
                var order = dbContext.Orders.Where(o => o.PONumber == orderid).FirstOrDefault();
                if (order != null)
                {
                    var orderVM = Mapper.Map<OrderVM>(order);
                    var items = dbContext.OrderItems.Where(i => i.OrderId == order.Id && i.RowNo == POLine);
                    if (items != null)
                    {
                        var itemsVM = Mapper.Map<IQueryable<OrderItem>, OrderItemVM[]>(items);
                        orderVM.Items = itemsVM;
                    }
                    return orderVM;
                }

            }
            return null;
        }
        public ZMON GetDetailsZMON(string NotificationNumber)
        {
            var soVM = new ZMON();
            using (var dbContext = new HCCSContext())
            {
                soVM = dbContext.Set<ZMON>().Where(a => a.Notification_Number == NotificationNumber).OrderByDescending(a => a.Creation_Date).FirstOrDefault();
                if (soVM != null) return soVM;
            }
            return soVM;
        }
        public IBase GetIBASEDetails(string Requisitioner)
        {

            using (var dbContext = new HCCSContext())
            {
                var zMon = dbContext.Set<ZMON>().Where(z => z.Notification_Number.Trim() == Requisitioner.Trim()).FirstOrDefault();
                if (zMon == null) return null;
                var iBase = dbContext.Set<IBase>().Where(ibase => ibase.FLNumber == zMon.Functional_Location).FirstOrDefault();
                if (iBase == null) return null;
                return new IBase
                {

                    Name1 = iBase.Name1,
                    Name2 = iBase.Name2,
                    Name3 = iBase.Name3,
                    Name4 = iBase.Name4,
                    Street = iBase.Street,
                    City = iBase.City,
                    PostCode = iBase.PostCode,
                    Region = iBase.Region,
                    Country = iBase.Country,

                    FLNumber = iBase.FLNumber.Substring(4,6),
                    SystemStatus = iBase.SystemStatus,
                    EmployeePrimCSEId = iBase.EmployeePrimCSEId,
                    EmployeeSecCSEId = iBase.EmployeeSecCSEId,
                    SerialNo = iBase.SerialNo,
                    EQDesc = iBase.EQDesc,
                    Tel = iBase.Tel,
                    NameCo = iBase.NameCo,
                    Comments = iBase.Comments
                };
            }
        }
        
        public void Delete(int id)
        {
            throw new NotImplementedException();    //Delete(int id)
        }
        public SchenkerStatusUpdate Find(int id)
        {
            throw new NotImplementedException();    //Find(int id)
        }
        public void Save()
        {
            throw new NotImplementedException();    //Save()
        }

        public void Dispose()
        {
            throw new NotImplementedException();    //Dispose
        }

        #endregion

        #region PRIVATE METHODS

        private void GetAddress(SchenkerOrderVM soVM, OrderVM orderVM)
        {
            //var orderRepo = new OrderRepository();
            if (orderVM.ServiceLevelType == "C")
            {
                soVM.DeliveryAddressName1 = orderVM.CustomerSite;
                soVM.DeliveryAddressName2 = orderVM.CustomerRoom;
                soVM.DeliveryAddressName3 = orderVM.CustomerDepartment;
                soVM.DeliveryAddressHouseNumber = orderVM.Street;
                soVM.CityInShipToParty = orderVM.City;
                soVM.PostalCode = orderVM.PostCode;
                soVM.Region = orderVM.Region;
                soVM.SiteContactName = orderVM.CustomerSiteContactName;
                soVM.SiteContactPhone = orderVM.CustomerSiteContactPhone;
                soVM.SpecialDeliveryRemarks = orderVM.CustomerSiteDeliveryRemarks;
                soVM.PostalCode = orderVM.PostCode;
                soVM.CityInShipToParty = orderVM.City;
                soVM.CustomerCountry = orderVM.Country;
                soVM.Plant = GetPlant(orderVM.DocType, orderVM.SLoc, orderVM.DestRegion, orderVM.DestPostCode);
                return;
            }

            if (orderVM.ServiceLevelType == "D")
            {
                soVM.DeliveryAddressName1 = orderVM.CustomerSite;
                soVM.DeliveryAddressName2 = orderVM.CustomerDepartment;
                soVM.DeliveryAddressHouseNumber = orderVM.Street;
                soVM.CityInShipToParty = orderVM.City;
                soVM.PostalCode = orderVM.DestPostCode;
                soVM.Region = orderVM.DestRegion;
                soVM.Plant = GetPlant(orderVM.DocType, orderVM.SLoc, orderVM.DestRegion, orderVM.DestPostCode);
                return;
            }

            if (orderVM.ServiceLevelType == "E")
            {
                soVM.DeliveryAddressName1 = "Siemens Healthcare Ltd";
                soVM.DeliveryAddressName2 = "c/o " + orderVM.CustomerSite;
                soVM.CityInShipToParty = orderVM.City;
                soVM.DeliveryAddressHouseNumber = orderVM.Street;
                soVM.PostalCode = orderVM.DestPostCode;
                soVM.Region = orderVM.DestRegion;
                soVM.Plant = GetPlant(orderVM.DocType, orderVM.SLoc, orderVM.DestRegion, orderVM.DestPostCode);
                return;
            }

        }

        private string GetPlant(string docType, string sLoc, string destinationRegion, string destinationPostCode)
        {
            try
            {
                using (var dbContext = new HCCSContext())
                {
                    var orderRepo = new OrderRepository();


                    if (docType.Equals("UB"))
                    {
                        var plant = dbContext.Set<Plant>()
                           .Where(p => p.OrderType == docType && p.SLOC == sLoc)
                           .Select(p => p.TargetPlant).FirstOrDefault();
                        return plant.ToString();
                    }
                    else
                    {
                        var destinationAirport = orderRepo.GetDestinationAirport(destinationRegion, destinationPostCode);
                        var plant = dbContext.Set<Plant>()
                           .Where(p => p.DestinationAirport == destinationAirport)
                           .Select(p => p.TargetPlant).FirstOrDefault();
                        return plant;
                    }
                }
            }
            catch (Exception ex)
            {
                string mess = ex.InnerException.Message;
            }
            return string.Empty;
        }
        private OrderVM GetOrderVMById(int id)
        {
            using (var dbContext = new HCCSContext())
            {
                var order = dbContext.Orders.Where(o => o.Id == id).FirstOrDefault();
                if (order != null)
                {
                    var orderVM = Mapper.Map<OrderVM>(order);
                    var items = dbContext.OrderItems.Where(i => i.OrderId == id);
                    if (items != null)
                    {
                        var itemsVM = Mapper.Map<IQueryable<OrderItem>, OrderItemVM[]>(items);
                        orderVM.Items = itemsVM;
                    }
                    return orderVM;
                }
            }
            return null;
        }

        #endregion

    }
}
