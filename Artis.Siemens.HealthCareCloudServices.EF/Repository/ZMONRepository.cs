﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class ZMONRepository : IZMONRepository
    {
        private readonly HCCSContext _dbContext;


        public ZMONRepository()
        {
            _dbContext = new HCCSContext();
        }


        public IQueryable<ZMON> All
        {
            get { return _dbContext.ZMON as IQueryable<ZMON>; }
        }

      


        public ZMON Find(int id)
        {
            var zMON = _dbContext.ZMON.Find(id);
            return zMON as ZMON;
        }

        public void InsertOrUpdate(ZMON entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var dropPoint = _dbContext.DropPoints.Find(id);
            if (dropPoint != null)
            {
                _dbContext.DropPoints.Remove(dropPoint);
            }
        }

        public void Delete(ZMON zMON)
        {
            if (zMON != null)
            {
                _dbContext.ZMON.Remove(zMON);
            }
        }

        public void Save()
        {
            //  insert the last time/person details
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

    }
}
