﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using AutoMapper;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class OrderRepository : IOrderRepository
    {
        #region PUBLIC METHODS

        public bool NotificationNumberExists(string notificationNumber)
        {
            var numberExists = false;

            using (var dbContext = new HCCSContext())
            {
                //numberExists = dbContext.Notifications.Any(notification => notification.Id == notificationNumber);
                numberExists = dbContext.ZMONs.Any(z => z.Notification_Number == notificationNumber);
            }

            return numberExists;
        }

        public OrderConfirmVM GetById(int id)
        {
            var ocVM = new OrderConfirmVM();
            var orderVM = GetOrderVMById(id);
            if (orderVM != null)

            {
                ocVM.Order = orderVM;
                ocVM.IBase = Mapper.Map<CustomerSiteVM>(GetIBase(orderVM.Requisitioner));
                ocVM.Employee = Mapper.Map<EmployeeVM>(GetEmployeeBySloc(orderVM.ReceivingSloc));
                ocVM.AssignedCSE1 = Mapper.Map<EmployeeVM>(GetEmployeeById(ocVM.IBase.EmployeePrimCSEId ?? "0"));
                ocVM.AssignedCSE2 = Mapper.Map<EmployeeVM>(GetEmployeeById(ocVM.IBase.EmployeeSecCSEId ?? "0"));
                ocVM.ZMon = Mapper.Map<ZMonVM>(GetZMonByNotificationNumber(orderVM.Requisitioner));

                //var n = GetNotificationByNotificationNumber(orderVM.Requisitioner);
                //if (n != null) ocVM.Notification.EffectCode = n.EffectCode;

                ocVM.LastVisit = GetLastVist(ocVM.Order.Requisitioner);
                ocVM.NextVisit = GetNextVist(ocVM.Order.Requisitioner);
                var originRegion = ocVM.Order.OriginRegion.ToUpper().Trim();
                //inserted to trim the Destination PostCode, City, DestinationRegion 
                var destRegion = ocVM.Order.DestRegion.ToUpper().Trim();
                var destPostCode = ocVM.Order.DestPostCode.Trim();
                var city = ocVM.Order.City.Trim();

                if (originRegion == "FRA" || originRegion == "SGP")
                {
                    //ocVM.Flights = Mapper.Map<List<InternationalFlightsVM>>(GetFlights(originRegion, ocVM.Order.DestRegion, ocVM.Order.DestPostCode));
                    ocVM.Flights = Mapper.Map<List<InternationalFlightsVM>>(GetFlights(originRegion, destRegion, destPostCode));
                    originRegion = ocVM.Order.DestRegion;

                }
                ocVM.Services = Mapper.Map<List<LeadTimeCostFormulaVM>>(GetAvailableServices(originRegion, destPostCode, ocVM.Order.TailGate, city));
            }

            return ocVM;
        }

        public OrderConfirmVM GetByPurchaseOrderNumber(string purchaseOrder)
        {
            var ocVM = new OrderConfirmVM();
            var orderVM = GetOrderVMByPurchaseOrder(purchaseOrder);
            ocVM.Order = orderVM;
            ocVM.ZMons = Mapper.Map<List<ZMonVM>>(GetZMonsByNotificationNumber(orderVM.Requisitioner));

            return ocVM;
        }
        public IBase GetIBase(string notificationNumber)
        {

            using (var dbContext = new HCCSContext())
            {
                var zMon = dbContext.Set<ZMON>().Where(z => z.Notification_Number.Trim() == notificationNumber.Trim()).FirstOrDefault();
                if (zMon == null) return CustomerSiteNotFound();
                var iBase = dbContext.Set<IBase>().Where(ibase => ibase.FLNumber == zMon.Functional_Location).FirstOrDefault();
                if (iBase == null) return CustomerSiteNotFound();
                return new IBase
                {

                    Name1 = iBase.Name1,
                    Name2 = iBase.Name2,
                    Name3 = iBase.Name3,
                    Street = iBase.Street,
                    City = iBase.City,
                    PostCode = iBase.PostCode,
                    Region = iBase.Region,
                    Country = iBase.Country,

                    FLNumber = iBase.FLNumber,
                    SystemStatus = iBase.SystemStatus,
                    EmployeePrimCSEId = iBase.EmployeePrimCSEId,
                    EmployeeSecCSEId = iBase.EmployeeSecCSEId,
                    SerialNo = iBase.SerialNo,
                    EQDesc = iBase.EQDesc,
                    Tel = iBase.Tel,
                    NameCo = iBase.NameCo,
                    Comments = iBase.Comments
                };
            }
        }

        public bool CheckAllOrderItems(string purchaseOrder, string eventCode)
        {
            bool allDone = true;

            //check count of line items for ConNote
            using (var dbContext = new HCCSContext())
            {
                //get the order, then all lineitems
                OrderVM order = GetOrderVMByPurchaseOrder(purchaseOrder);

                foreach (OrderItemVM oi in order.Items)
                {
                    if (eventCode == "SHP")
                    {
                        if (oi.SchenkerSHP != "Y")
                        {
                            allDone = false;
                            break;
                        }
                    }
                    else if (eventCode == "DEL")
                    {
                        if (oi.SchenkerDEL != "Y")
                        {
                            allDone = false;
                            break;
                        }
                    }
                }
            }

            return allDone;
        }
        public DeliveryThreshold GetDeliveryThresholds(string deliveryType)
        {
            using (var dbContext = new HCCSContext())
            {
                var threshold = dbContext.DeliveryThresholds.Where(dt => dt.DeliveryType.Trim() == deliveryType.Trim()).FirstOrDefault();
                return threshold;
            }
        }

        public DropPoint GetDropPoint(string notificationNumber)
        {
            using (var dbContext = new HCCSContext())
            {
                var zMon = dbContext.Set<ZMON>().Where(z => z.Notification_Number.Trim() == notificationNumber.Trim()).FirstOrDefault();
                if (zMon == null) return DropPointNotFound();
                var iBaseDelivery = dbContext.Set<IBaseDeliveryDetail>().Where(ibase => ibase.FLId.Trim() == zMon.Functional_Location.Trim()).FirstOrDefault();
                if (iBaseDelivery == null) return DropPointNotFound();
                var dropPoint = dbContext.Set<DropPoint>().Where(dp => dp.Id == iBaseDelivery.DropPointId).FirstOrDefault();
                if (dropPoint == null) return DropPointNotFound();
                return dropPoint;
            }
        }

        public Employee GetEmployeeBySloc(string sloc)
        {
            var employee = new Employee();
            using (var dbContext = new HCCSContext())
            {
                employee = dbContext.Set<Employee>().Where(emp => emp.Sloc == sloc).FirstOrDefault();
                if (employee != null) return employee;
            }
            return employee;
        }


        public Employee GetEmployeeById(string empId)
        {
            var employee = new Employee();
            using (var dbContext = new HCCSContext())
            {
                employee = dbContext.Set<Employee>().Where(emp => emp.Id == empId).FirstOrDefault();
                if (employee != null) return employee;
            }
            return employee;
        }


        public Order Create(Order order)
        {
            using (var dbContext = new HCCSContext())
            {
                //doing a NEW or UPDATE
                if (order.Id == 0)
                {
                    dbContext.Entry<Order>(order).State = EntityState.Added;
                }
                else
                {
                    foreach (OrderItem oi in order.Items)
                    {
                        if (oi.Id != 0)
                            dbContext.Entry<OrderItem>(oi).State = EntityState.Modified;
                        else
                            dbContext.Entry<OrderItem>(oi).State = EntityState.Added;
                    }

                    dbContext.Entry<Order>(order).State = EntityState.Modified;

                }
                dbContext.SaveChanges();
                return order;
            }
        }


        public List<InternationalFlightsCutoff> GetFlights(string originRegion, string destRegion, string destPostCode)
        {
            string destinationAirportCode = GetDestinationAirport(destRegion, destPostCode);

            if (!string.IsNullOrEmpty(destinationAirportCode))
            {
                using (var dbContext = new HCCSContext())
                {
                    var flights = dbContext.Set<InternationalFlightsCutoff>()
                        .Where(f => f.OriginAirportCode == originRegion && f.DestinationAirportCode == destinationAirportCode);
                    return flights.ToList();
                }
            }
            return new List<InternationalFlightsCutoff>();
        }

        public IList<DropPoint> SearchDP(string query)
        {
            using (var dbContext = new HCCSContext())
            {
                var dropPoints = dbContext.DropPoints.Where(dp => dp.Name1.Contains(query));
                if (dropPoints == null) return new List<DropPoint>();
                return dropPoints.Take(10).ToList();
            }
        }

        public IList<LeadTimeCostFormula> GetGenericServices()
        {
            using (var dbContext = new HCCSContext())
            {
                var leadTimeCostFormulas = dbContext.LeadTimeCostFormulas.Where(dp => dp.GenericService == "1");
                if (leadTimeCostFormulas == null) return new List<LeadTimeCostFormula>();
                return leadTimeCostFormulas.ToList();
            }
        }

        public IList<IBase> SearchCS(string query)
        {
            using (var dbContext = new HCCSContext())
            {
                var ibases = dbContext.IBases.Where(cs => cs.FLNumber.Contains(query));
                if (ibases == null) return new List<IBase>();
                return ibases.Take(10).ToList();
            }
        }

        public DateTime? GetLastVist(string notificationNumber)
        {
            using (var dbContext = new HCCSContext())
            {
                var lastVisit = dbContext.Set<ZMON>()
                    .Where(z => z.Notification_Number == notificationNumber && z.Planned_Start_Date_Time < DateTime.Now)
                    .Select(z => z.Planned_Start_Date_Time).Max();
                return lastVisit;
            }
        }

        public DateTime? GetNextVist(string notificationNumber)
        {
            using (var dbContext = new HCCSContext())
            {
                var nextVisit = dbContext.Set<ZMON>()
                    .Where(z => z.Notification_Number == notificationNumber && z.Planned_Start_Date_Time >= DateTime.Now)
                    .Select(z => z.Planned_Start_Date_Time).Min();
                return nextVisit;
            }
        }

        public List<LeadTimeCostFormula> GetAvailableServices(string region, string postCode, string tailgate, string destinationCity)
        {
            //using (var dbContext = new HCCSContext())
            //{
            //    var services = dbContext.Set<LeadTimeCostFormula>()
            //        .Where(s => s.OriginRegion == region && s.DestinationPostCode == postCode && s.Tailgate == tailgate);
            //    if (services != null) return services.ToList();
            //}

            using (var dbContext = new HCCSContext())
            {
                var services = dbContext.Set<LeadTimeCostFormula>()
                    .Where(s => s.OriginRegion == region && s.DestinationPostCode == postCode && s.Tailgate == tailgate && s.DestinationCity == destinationCity);
                if (services != null) return services.ToList();
            }
            return new List<LeadTimeCostFormula>();
        }


        #endregion

        #region PRIVATE METHODS
        private ZMON GetZMonByNotificationNumber(string notificationNumber)
        {
            using (var dbContext = new HCCSContext())
            {
                return dbContext.ZMONs.Where(z => z.Notification_Number == notificationNumber).FirstOrDefault<ZMON>();
            }
        }

        private List<ZMON> GetZMonsByNotificationNumber(string notificationNumber)
        {
            using (var dbContext = new HCCSContext())
            {
                return dbContext.ZMONs.Where(z => z.Notification_Number == notificationNumber).ToList<ZMON>();
            }
        }
        //private Notification GetNotificationByNotificationNumber(string notificationNumber)
        //{
        //    using (var dbContext = new HCCSContext())
        //    {
        //        return dbContext.Notifications.Where(n => n.Id == notificationNumber).FirstOrDefault();
        //    }
        //}
        private OrderVM GetOrderVMById(int id)
        {
            using (var dbContext = new HCCSContext())
            {
                var order = dbContext.Orders.Where(o => o.Id == id).FirstOrDefault();
                if (order != null)
                {
                    var orderVM = Mapper.Map<OrderVM>(order);
                    var items = dbContext.OrderItems.Where(i => i.OrderId == id)
                                                    .OrderBy(i => i.RowNo);
                    if (items != null)
                    {
                        var itemsVM = Mapper.Map<IQueryable<OrderItem>, OrderItemVM[]>(items);
                        orderVM.Items = itemsVM;
                    }
                    return orderVM;
                }
            }
            return null;
        }

        private OrderVM GetOrderVMByPurchaseOrder(string purchaseOrderNumber)
        {
            using (var dbContext = new HCCSContext())
            {
                var order = dbContext.Orders.Where(o => o.PONumber == purchaseOrderNumber).FirstOrDefault();
                if (order != null)
                {
                    var orderVM = Mapper.Map<OrderVM>(order);
                    var items = dbContext.OrderItems.Where(i => i.OrderId == order.Id);
                    if (items != null)
                    {
                        var itemsVM = Mapper.Map<IQueryable<OrderItem>, OrderItemVM[]>(items);
                        orderVM.Items = itemsVM;
                    }
                    return orderVM;
                }
            }
            return null;
        }
        private IBase CustomerSiteNotFound()
        {
            return new IBase
            {
                Name1 = "Customer Site not found",
                Name2 = "",
                Name3 = "",
                Street = "",
                City = "",
                PostCode = "",
                Region = "",
                Country = "",
                NameCo = "",
                Tel = "",
                Comments = ""
            };
        }

        private DropPoint DropPointNotFound()
        {
            return new DropPoint
            {
                Name1 = "Drop Point not found"
            };
        }

        private Employee EmployeeNotFound()
        {
            return new Employee
            {
                GivenName = "Employee not found"
            };
        }


        internal string GetDestinationAirport(string destRegion, string destPostCode)
        {
            // if destination matches postcode in destination then select airportcode
            // else match region to select airport code
            // 
            using (var dbContext = new HCCSContext())
            {
                var dest = dbContext.Set<InternationalDestination>().Where(id => id.DestinationPostCode == destPostCode).FirstOrDefault();
                if (dest == null)
                {
                    dest = dbContext.Set<InternationalDestination>().Where(id => id.DestinationRegion == destRegion).FirstOrDefault();
                }
                if (dest == null) return string.Empty;
                return dest.DestinationAirportCode;
            }
        }



        #endregion

        #region Not Implemented

        public IQueryable<ViewModels.OrderItemVM> All
        {
            get
            {
                throw new NotImplementedException();    //IQueryable<ViewModels.OrderItemVM> All
            }
        }

        public ViewModels.OrderItemVM Find(int id)
        {
            throw new NotImplementedException();    //Find(int id)
        }
        public void InsertOrUpdate(ViewModels.OrderItemVM entity, System.Data.Entity.EntityState entityState)
        {
            throw new NotImplementedException();    //InsertOrUpdate
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();    //Delete(int id)
        }

        public void Save()
        {
            throw new NotImplementedException();    //Save()
        }

        public void Dispose()
        {
            throw new NotImplementedException();    //Dispose()
        }

        #endregion



    }
}
