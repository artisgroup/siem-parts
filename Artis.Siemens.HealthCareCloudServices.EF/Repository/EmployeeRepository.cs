﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {

        public Employee GetById(string id)
        {
            using (var dbContext = new HCCSContext())
            {
                var employee = dbContext.Employees.Where(emp => emp.Sloc == id).FirstOrDefault();
                return employee;
            }
        }

        public Employee GetByEmployeeId(string id)
        {
            using (var dbContext = new HCCSContext())
            {
                var employee = dbContext.Employees.Where(emp => emp.Id == id).FirstOrDefault();
                return employee;
            }
        }
        public IQueryable<Employee> All
        {
            get
            {
                IQueryable<Employee> employees;
                using (var dbContext = new HCCSContext())
                {
                    employees = dbContext.Employees;
                }
                return employees;

            }
        }

        public IList<Employee> SearchEA(string query)
        {
            using (var dbContext = new HCCSContext())
            {
                var employeerAddress = dbContext.Employees.Where(
                    dp => (dp.GivenName.Contains(query) || dp.LastName.Contains(query) || dp.Sloc.Contains(query))
                    && !string.IsNullOrEmpty(dp.Sloc)
                    );
                if (employeerAddress == null) return new List<Employee>();
                return employeerAddress.Take(10).ToList();
            }
        }



        public IList<Employee> GetAll
        {
            get
            {
                IList<Employee> employees;
                using (var dbContext = new HCCSContext())
                {
                    employees = dbContext.Employees.Where(emp => !string.IsNullOrEmpty(emp.Sloc))
                                                    .OrderBy(e1 => e1.LastName)
                                                    .ThenBy(e2 => e2.GivenName)
                                                    .ToList();
                }
                return employees;
            }
        }

        public Employee Find(int id)
        {
            throw new NotImplementedException();    //Find(int id)
        }

        public void InsertOrUpdate(Domain.Models.Employee entity, System.Data.Entity.EntityState entityState)
        {
            throw new NotImplementedException();    //InsertOrUpdate
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();    //Delete(int id)
        }

        public void Save()
        {
            throw new NotImplementedException();    //Save()
        }

        public void Dispose()
        {
            throw new NotImplementedException();    //Dispose()
        }
    }
}
