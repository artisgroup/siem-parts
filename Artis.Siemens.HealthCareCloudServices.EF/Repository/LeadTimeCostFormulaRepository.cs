﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class LeadTimeCostFormulaRepository : ILeadTimeCostFormulaRepository
    {

        private readonly HCCSContext _dbContext;


        public LeadTimeCostFormulaRepository()
        {
            _dbContext = new HCCSContext();
        }


        public IQueryable<LeadTimeCostFormula> All
        {
            get { return _dbContext.LeadTimeCostFormulas; }
        }

       

        public LeadTimeCostFormula Find(int id)
        {
            var leadTimeCostFormula = _dbContext.LeadTimeCostFormulas.Find(id);
            return leadTimeCostFormula;
        }

        public void InsertOrUpdate(LeadTimeCostFormula entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var leadTimeCostFormula = _dbContext.LeadTimeCostFormulas.Find(id);
            if (leadTimeCostFormula != null)
            {
                _dbContext.LeadTimeCostFormulas.Remove(leadTimeCostFormula);
            }
        }

        public void Delete(LeadTimeCostFormula leadTimeCostFormula)
        {
            if (leadTimeCostFormula != null)
            {
                _dbContext.LeadTimeCostFormulas.Remove(leadTimeCostFormula);
            }
        }

        public void Save()
        {
            
            try
            {
                _dbContext.SaveChanges();
            }
            catch(DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }

            }
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
