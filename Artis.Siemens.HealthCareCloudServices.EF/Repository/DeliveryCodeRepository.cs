﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using PagedList;


namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class DeliveryCodeRepository : IDeliveryCodeRepository
    {
        private readonly HCCSContext _dbContext;

        public DeliveryCodeRepository()
        {
            _dbContext = new HCCSContext();
        }


        public IQueryable<DeliveryCode> All
        {
            get { return _dbContext.DeliveryCodes; }

        }

        public DeliveryCode Find(int id)
        {
            var DeliveryCode = _dbContext.DeliveryCodes.Find(id);
            return DeliveryCode;
        }




        public void InsertOrUpdate(DeliveryCode entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var DeliveryCode = _dbContext.DeliveryCodes.Find(id);
            if (DeliveryCode != null)
            {
                _dbContext.DeliveryCodes.Remove(DeliveryCode);
            }
        }

        public void Delete(DeliveryCode DeliveryCode)
        {
            if (DeliveryCode != null)
            {
                _dbContext.DeliveryCodes.Remove(DeliveryCode);
            }
        }

        public void Save()
        {
            //  insert the last time/person details
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
