﻿using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.ViewModels;
using System.Data.Entity;
using AutoMapper;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class OrderTrackRepository : IOrderTrackRepository
    {
        public IList<OrderTrack> GetTrackDeliveries(OrderTrackPostVM postData)
        {

            List<OrderTrack> deliveries = null;


            // ========================== DEFAULT VIEW ==========================
            if (postData.RetrievalMode == "DEFAULT")
            {
                var employeeSLoc = string.Empty;

                //get the employee - based on current user loginid 
                using (var dbContext = new HCCSContext())
                {
                    var employee = dbContext.Employees.Where(e => e.GID == postData.CurrentUserLogin).FirstOrDefault<Employee>();

                    if (employee != null)
                        employeeSLoc = employee.Sloc;

                    if (employeeSLoc != string.Empty)
                    {
                        //get all orders for the current user sloc (employeeid)
                        var orders = dbContext.Orders.Where(o => o.SLoc == employeeSLoc);
                        if (orders != null)
                        {
                            //got the orders, now fill out with extra data/detail
                            deliveries = GetExtraData(orders.ToList<Order>(), false, false);
                        }
                    }
                }
            }
            else if (postData.RetrievalMode == "SEARCH")
            // ========================== SEARCH ==========================
            {
                // =========== USER HAS ENTERED NOTIFICATION NUMBER ===========
                if (postData.PurchasingDoc != string.Empty && postData.Requisitioner == string.Empty && postData.DestinationRegion == string.Empty && postData.OriginLocation == string.Empty && postData.ReceivingSloc == string.Empty)
                {
                    #region Search - Purchase Order Number
                    using (var dbContext = new HCCSContext())
                    {
                        //get all orders for the current user sloc (employeeid)
                        var orders = dbContext.Orders.Where(o => o.PONumber == postData.PurchasingDoc);
                        if (orders != null)
                        {
                            //got the orders, now fill out with extra data/detail
                            deliveries = GetExtraData(orders.ToList<Order>(), postData.HotSite, postData.Historical);
                        }
                    }
                    #endregion
                }
                else
                {
                    //search based on other fields
                    using (var dbContext = new HCCSContext())
                    {
                        var orders = dbContext.Orders
                            .Where(o => o.NotificationNumber == postData.Requisitioner || postData.Requisitioner == string.Empty)
                            .Where(o => o.DestRegion == postData.DestinationRegion || postData.DestinationRegion == string.Empty)
                            .Where(o => o.OriginLocation == postData.OriginLocation || postData.OriginLocation == string.Empty)
                            .Where(o => o.ReceiverId.ToString() == postData.ReceivingSloc || postData.ReceivingSloc == string.Empty)
                            .Take(100)
                            .ToList<Order>();

                        if (orders != null)
                        {
                            //1. check for DestinationRegion
                            //2. check for OriginLocation
                            //3. check for DeliveryDate
                            if (postData.DeliveryDate != DateTime.MinValue)
                                orders = orders.Where(o => DateTime.Compare(o.DeliveryDate.Date, postData.DeliveryDate.Date) == 0).ToList<Order>();

                            //4. check for ReceivingSloc
                            //got the orders, now fill out with extra data/detail
                            deliveries = GetExtraData(orders.ToList<Order>(), postData.HotSite, postData.Historical);
                        }
                    }
                }
            }

            return deliveries;

        }
        public IList<OrderItem> GetPartsOrdered(string purchaseOrder)
        //public IList<OrderItem> GetPartsOrdered(string purchaseOrder, string conNote)
        {
            List<OrderItem> returnItems = new List<OrderItem>();

            List<OrderItem> items = null;


            using (var dbContext = new HCCSContext())
            {
                //get the order - for the purchase order #
                var order = dbContext.Orders.Where(o => o.PONumber == purchaseOrder).FirstOrDefault();
                if (order != null)
                {
                    //get the items - for the same purchase order number
                    var orderItems = dbContext.OrderItems.Where(i => i.OrderId == order.Id);
                    if (orderItems != null)
                    {
                        items = orderItems.ToList();
                    }
                    returnItems = items;
                }

                // =====================================================================
                //commented out to check to resolve the issue in orders that can't be seen in Track and Trace
                //if getting for all of the connote
                //if (conNote == "-1")
                //{
                //    returnItems = items;
                //}
                // =====================================================================
                else
                {
                    List<SchenkerStatusUpdate> updates = null;

                    //check the schenker status for the connote
                    //var statusUpdates = dbContext.SchenkerStatusUpdates.Where(o => o.PODocNumber == purchaseOrder && o.ConNote == conNote);
                    var statusUpdates = dbContext.SchenkerStatusUpdates.Where(o => o.PODocNumber == purchaseOrder);
                    if (statusUpdates != null)
                    {
                        //got all row items for the chosen connote
                        updates = statusUpdates.ToList();

                        //determine a distinct set of LINE ITEMS 
                        List<string> rowItems = new List<string>();
                        foreach (SchenkerStatusUpdate u in updates)
                        {
                            rowItems.Add(u.ItemNo);
                        }
                        rowItems = rowItems.Distinct<string>().ToList();

                        //go through each item to see if it's in the list determined (just above)
                        foreach (OrderItem oi in items)
                        {
                            foreach (string s in rowItems)
                            {
                                if (oi.RowNo == s)
                                    returnItems.Add(oi);
                            }
                        }
                    }
                }
            }

            return returnItems;

        }

        //public IList<SchenkerStatusUpdate> GetTransportEvents(string purchaseOrder, string conNote)
        public IList<SchenkerStatusUpdate> GetTransportEvents(string purchaseOrder)
        {
            List<SchenkerStatusUpdate> events = null;

            using (var dbContext = new HCCSContext())
            {
                //get the order - for the purchase order #
                var statusUpdates = dbContext.SchenkerStatusUpdates.Where(o => o.PODocNumber == purchaseOrder);
                if (statusUpdates != null)
                {
                    events = statusUpdates.ToList();
                }

                //prune out any for other connotes - if applicble
                //if (conNote != "-1")
                //    events = events.Where(o => o.ConNote == conNote).ToList();

            }
            return events;

        }

        #region PrivateMethods

        private List<OrderTrack> GetExtraData(List<Order> orders, bool hotSite, bool includeHistory)
        {
            SchenkerStatusUpdateRepository _schenkerRepo = new SchenkerStatusUpdateRepository();
            List<OrderTrack> otList = new List<OrderTrack>();

            using (var dbContext = new HCCSContext())
            {
                foreach (Order o in orders)
                {
                    bool includeOrder = false;

                    if (hotSite)
                    {
                        //GET FROM ZMON (CHRIS OC) - field notification_type
                        bool hasHotSiteFromzMon = dbContext.ZMONs.Any(z => z.Notification_Number == o.NotificationNumber && z.Notification_Type == "MH");
                        if (hasHotSiteFromzMon) includeOrder = true;
                    }
                    else
                    {
                        includeOrder = true;
                    }

                    //now include in the return results
                    if (includeOrder)
                    {
                        OrderTrack ot = new OrderTrack();
                        ot.OrderId = o.Id;
                        ot.Destination = o.DestRegion;
                        ot.PurchaseOrderNumber = o.PONumber;
                        ot.Requisitioner = o.NotificationNumber;
                        ot.ServiceLevelType = o.ServiceLevelType;
                        ot.ServiceLevel = o.ServiceLevelDescription;
                        ot.DeliveryDate = o.DeliveryDate;
                        ot.Origin = o.OriginLocation;
                        ot.LastEvent = o.SchenkerTimestamp;
                        ot.CustomerSite = o.CustomerSite;
                        ot.CustomerRoom = o.CustomerRoom;
                        ot.CustomerDepartment = o.CustomerDepartment;
                        ot.Street = o.Street;
                        ot.City = o.City;
                        ot.PostCode = o.PostCode;
                        ot.DestPostCode = o.DestPostCode;
                        ot.Region = o.Region;
                        ot.DestRegion = o.DestRegion;
                        ot.Country = o.Country;
                        ot.WarehousePriority = o.WarehousePriority;
                        
                        string tailGate = o.TailGate;
                        if (tailGate == "0")
                            ot.Tailgate = "No";
                        else if (tailGate == "1")
                            ot.Tailgate = "Yes";


                        //get the order items - check for NOT YET DELIVERED 
                        bool hasOutstandingDeliveries = false;

                        var orderItems = dbContext.OrderItems.Where(oi => oi.OrderId == o.Id);
                        if (orderItems != null)
                        {
                            foreach (OrderItem oi in orderItems.ToList<OrderItem>())
                            {
                                if (oi.SchenkerDEL != "Y")
                                    hasOutstandingDeliveries = true;
                            }
                        }

                        //only include orders with outstanding deliveries (if not searching history)
                        if (includeHistory)
                            includeOrder = true;
                        else
                            includeOrder = hasOutstandingDeliveries;

                        // =============================================================================
                        //get the most recent one
                        var statusUpdates = dbContext.SchenkerStatusUpdates
                                                    .Where(sc => sc.PODocNumber == ot.PurchaseOrderNumber)
                                                    .OrderByDescending(x => x.MessageDateTime)
                                                    .FirstOrDefault<SchenkerStatusUpdate>();
                        if (statusUpdates != null)
                        {
                            ot.LastEvent = statusUpdates.EventDateTime;
                            ot.EventComment = statusUpdates.EventComment;
                            ot.Status = statusUpdates.EventCode;
                        }

                        // =============================================================================
                        //get the connotes
                        var conNotes = dbContext.SchenkerStatusUpdates.Where(sc => sc.PODocNumber == ot.PurchaseOrderNumber).ToList<SchenkerStatusUpdate>();
                        if (conNotes != null)
                        {
                            ot.ConNotes = new List<string>();

                            //poke these into the VM
                            foreach (SchenkerStatusUpdate s in conNotes)
                            {
                                ot.ConNotes.Add(s.ConNote);
                            }

                            //get the unique ones
                            ot.ConNotes = ot.ConNotes.Distinct<string>().ToList();
                        }

                        
                        // =============================================================================
                        var zMon = dbContext.ZMONs.Where(no => no.Notification_Number == ot.Requisitioner).FirstOrDefault();
                        if (zMon != null)
                        {
                            ot.FLNumber = zMon.Functional_Location;
                            var customerName = dbContext.IBases.Where(fl => fl.FLNumber == ot.FLNumber).FirstOrDefault();
                            var fldesc = dbContext.IBases.Where(fl => fl.FLNumber == ot.FLNumber).FirstOrDefault();
                            if (fldesc != null)
                            {
                                ot.System = fldesc.FLDesc;
                            }
                            if (customerName != null)
                            {
                                ot.CustomerName = customerName.Name1;
                            }

                            string Effect = zMon.Effect_On_Operation;

                            if (Effect == "1")
                                ot.EffectCode = "System Down";
                            else if (Effect == "2")
                                ot.EffectCode = "System Partially Down";
                            else if (Effect == "3")
                                ot.EffectCode = "Scheduled Service";
                            else if (Effect == "4")
                                ot.EffectCode = "Commissioning";
                            else if (Effect == "5")
                                ot.EffectCode = "Technical Support";
                            else if (Effect == "6")
                                ot.EffectCode = "Apps - Scheduled Service";
                            else if (Effect == "7")
                                ot.EffectCode = "Apps - Partially Down";
                            else if (Effect == "8")
                                ot.EffectCode = "Apps - Fully Down";
                            else
                                ot.EffectCode = "Unknown";

                            //check the alert for "hot site"
                            if (zMon.Notification_Type == "MH")
                                ot.HotSite = "Hot Site";
                        }

                        if (hasOutstandingDeliveries)
                        {
                            if (ot.DeliveryDate > ot.LastEvent)
                                ot.Alerts = "On Track";
                            else
                                ot.Alerts = "Missed";
                        }
                        else
                        {
                            ot.Alerts = "Delivered";
                        }

                        if (includeOrder)
                            otList.Add(ot);
                    }
                }
            }
            return otList;
        }

        #endregion

        #region Not Implemented

        public IQueryable<OrderTrackVM> All
        {
            get
            {
                throw new NotImplementedException();    //IQueryable<OrderTrackVM> All
            }
        }

        IQueryable<OrderTrack> IEntityRepository<OrderTrack>.All
        {
            get
            {
                throw new NotImplementedException();    //IEntityRepository<OrderTrack>.All
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();    //Delete(int id)
        }

        public void Dispose()
        {
            throw new NotImplementedException();    //Dispose()
        }

        public OrderTrackVM Find(int id)
        {
            throw new NotImplementedException();    //Find(int id)
        }

        public void InsertOrUpdate(OrderTrackVM entity, EntityState entityState)
        {
            throw new NotImplementedException();    //InsertOrUpdate
        }

        public void Save()
        {
            throw new NotImplementedException();    //Save()
        }

        OrderTrack IEntityRepository<OrderTrack>.Find(int id)
        {
            throw new NotImplementedException();    //IEntityRepository<OrderTrack>.Find(int id)
        }

        public void InsertOrUpdate(OrderTrack entity, EntityState entityState)
        {
            throw new NotImplementedException();    //InsertOrUpdate
        }


        #endregion
    }
}
