﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class InternationalDestinationsRepository : IInternationalDestinationsRepository
    {
        private readonly HCCSContext _dbContext;

        public InternationalDestinationsRepository()
        {
            _dbContext = new HCCSContext();
        }


        public IQueryable<InternationalDestination> All
        {
            get { return _dbContext.InternationalDestinations; }

        }


        public InternationalDestination Find(int id)
        {
            var internationalDestination = _dbContext.InternationalDestinations.Find(id);
            return internationalDestination;
        }




        public void InsertOrUpdate(InternationalDestination entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var internationalDestination = _dbContext.InternationalDestinations.Find(id);
            if (internationalDestination != null)
            {
                _dbContext.InternationalDestinations.Remove(internationalDestination);
            }
        }

        public void Delete(InternationalDestination internationalDestination)
        {
            if (internationalDestination != null)
            {
                _dbContext.InternationalDestinations.Remove(internationalDestination);
            }
        }

        public void Save()
        {
            //  insert the last time/person details
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
