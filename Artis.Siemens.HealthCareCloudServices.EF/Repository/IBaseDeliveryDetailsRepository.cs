﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;

namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class IBaseDeliveryDetailsRepository : IIBaseDeliveryDetailsRepository
    {
         private readonly HCCSContext _dbContext;


         public IBaseDeliveryDetailsRepository()
        {
            _dbContext = new HCCSContext();
        }


         public IQueryable<IBaseDeliveryDetail> All
        {
            get { return _dbContext.IBaseDeliveryDetails; }
        }

     

         public IBaseDeliveryDetail Find(int id)
        {
            var iBaseDeliveryDetail = _dbContext.IBaseDeliveryDetails.Find(id);
            return iBaseDeliveryDetail;
        }


        public IEnumerable<DropPoint> GetDropPoints()
        {
            return _dbContext.DropPoints;
        }

         public void InsertOrUpdate(IBaseDeliveryDetail entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var iBaseDeliveryDetail = _dbContext.IBaseDeliveryDetails.Find(id);
            if (iBaseDeliveryDetail != null)
            {
                _dbContext.IBaseDeliveryDetails.Remove(iBaseDeliveryDetail);
            }
        }

        public void Delete(IBaseDeliveryDetail iBaseDeliveryDetail)
        {
            if (iBaseDeliveryDetail != null)
            {
                _dbContext.IBaseDeliveryDetails.Remove(iBaseDeliveryDetail);
            }
        }

        public void Save()
        {
            //  insert the last time/person details
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
