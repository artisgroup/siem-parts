﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Interfaces;
using PagedList;


namespace Artis.Siemens.HealthCareCloudServices.EF.Repository
{
    public class DeliveryThresholdRepository : IDeliveryThresholdRepository
    {
        private readonly HCCSContext _dbContext;

        public DeliveryThresholdRepository()
        {
            _dbContext = new HCCSContext();
        }


        public IQueryable<DeliveryThreshold> All
        {
            get { return _dbContext.DeliveryThresholds; }

        }

        public DeliveryThreshold Find(int id)
        {
            var deliveryThreshold = _dbContext.DeliveryThresholds.Find(id);
            return deliveryThreshold;
        }




        public void InsertOrUpdate(DeliveryThreshold entity, EntityState entityState = EntityState.Added)
        {
            _dbContext.Entry(entity).State = entityState;
        }

        public void Delete(int id)
        {
            var deliveryThreshold = _dbContext.DeliveryThresholds.Find(id);
            if (deliveryThreshold != null)
            {
                _dbContext.DeliveryThresholds.Remove(deliveryThreshold);
            }
        }

        public void Delete(DeliveryThreshold deliveryThreshold)
        {
            if (deliveryThreshold != null)
            {
                _dbContext.DeliveryThresholds.Remove(deliveryThreshold);
            }
        }

        public void Save()
        {
            //  insert the last time/person details
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
