﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class HierarchyConfiguration : EntityTypeConfiguration<Hierarchy>
    {
        public HierarchyConfiguration()
        {
            ToTable("Hierarchies", "sap");

            HasKey(h=> h.Id);

            Property(h => h.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(h => h.IVKGroup)
                .HasMaxLength(512)
                .IsOptional();

            Property(h => h.SystemGroup)
               .HasMaxLength(512)
               .IsOptional();

            Property(h => h.SystemGroupL2)
               .HasMaxLength(512)
               .IsOptional();

            Property(h => h.SystemGroupL3)
               .HasMaxLength(512)
               .IsOptional();

            Property(h => h.Division)
               .HasMaxLength(512)
               .IsOptional();

            Property(h => h.BusinessUnit)
               .HasMaxLength(512)
               .IsOptional();

            Property(h => h.BusinessUnitSubLevel)
               .HasMaxLength(512)
               .IsOptional();

        }
    }
}
