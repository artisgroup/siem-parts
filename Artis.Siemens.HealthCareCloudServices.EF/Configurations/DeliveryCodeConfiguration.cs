﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    class DeliveryCodeConfiguration : EntityTypeConfiguration<DeliveryCode>
    {
        public DeliveryCodeConfiguration()
        {
            ToTable("DeliveryCode", "prd");

            HasKey(d => d.DeliveryCodeId);

            Property(d => d.DeliveryCodeId)
                            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
