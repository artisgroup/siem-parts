﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class InternationalDestinationConfiguration : EntityTypeConfiguration<InternationalDestination>
    {
        public InternationalDestinationConfiguration()
        {
            ToTable("InternationalDestinations", "prd");

            Property(id => id.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(id => id.DestinationPostCode)
                .HasMaxLength(6)
                .IsOptional();

            Property(id => id.DestinationRegion)
                .HasMaxLength(6)
                .IsOptional();

            Property(id => id.DestinationAirportCode)
                .HasMaxLength(6)
                .IsRequired();

            Property(id => id.DestinationAirportName)
                .HasMaxLength(40)
                .IsRequired();

            Property(id => id.LastEditedTime)
                .IsOptional();

            Property(id => id.LastEditedPerson)
                .HasMaxLength(40)
                .IsOptional();
        }
    }
}
