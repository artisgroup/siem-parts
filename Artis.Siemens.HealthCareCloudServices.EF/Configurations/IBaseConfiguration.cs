﻿using System.Data.Entity.ModelConfiguration;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class IBaseConfiguration : EntityTypeConfiguration<IBase>
    {
        public IBaseConfiguration()
        {
            ToTable("IBases", "prd");

            HasKey(ib => ib.FLNumber);

            Property(ib => ib.FLNumber)
                .HasMaxLength(512)
                .IsRequired();

            Property(ib => ib.FLDesc)
                .HasMaxLength(512)
                .IsOptional();

            Property(ib => ib.EQNumber)
                .HasMaxLength(512)
                .IsOptional();

            Property(ib => ib.EQDesc)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.MaterialNo)
                            .IsOptional();

            Property(ib => ib.SerialNo)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.SystemStatus)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.ServiceRegion)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Company)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Plant)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.WorkCentre)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Name1)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Name2)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Name3)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Name4)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Street)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.City)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.PostCode)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Region)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Country)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.GeoCodes)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.TimeZone)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Tel)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Email1)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.Comments)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.SortField)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.SalesOrg)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.URL)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.EvolvePO)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.EvolvePOEndDate)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.ContractLevel)
                            .HasMaxLength(512)
                            .IsOptional();

            Property(ib => ib.ContractStart)
                            .IsOptional();

            Property(ib => ib.ContractEnd)
                            .IsOptional();

            Property(ib => ib.EmployeeSecCSEId)
                            .IsOptional();

            Property(ib => ib.EmployeePrimFASId)
                            .IsOptional();

            Property(ib => ib.EmployeeServiceMgrId)
                            .IsOptional();

            Property(ib => ib.EmployeeSalesMgrId)
                            .IsOptional();

            Property(ib => ib.EmployeePrimCSEId)
                            .IsOptional();


        }
    }
}
