﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class PlantConfiguration:EntityTypeConfiguration<Plant>
    {
        public PlantConfiguration()
        {
            ToTable("Plants", "prd");
        }
    }
}
