﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class DispatchConfiguration : EntityTypeConfiguration<Dispatch>
    {

        public DispatchConfiguration()
        {
            ToTable("Dispatches", "sap");

            HasKey(d => d.Id);

            Property(d => d.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.NotificationId)
                .IsOptional();

            Property(e => e.Cancelled)
                .IsOptional();

            Property(e => e.EmployeeId)
                .IsOptional();

            Property(e => e.ETA)
                .IsOptional();

            Property(e => e.Travel)
                .IsOptional();

            Property(e => e.OnSiteStart)
                .IsOptional();

            Property(e => e.OnSiteFinish)
                .IsOptional();




        }

    }
}
