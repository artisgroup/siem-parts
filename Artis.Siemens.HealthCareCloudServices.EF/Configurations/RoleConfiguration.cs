﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class RoleConfiguration : EntityTypeConfiguration<Role>
    {

        public RoleConfiguration()
        {
            Property(r => r.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(r => r.Name)
                .HasMaxLength(10)
                .IsRequired();

            Property(r => r.Description)
                .HasMaxLength(120)
                .IsOptional();

        }
    }
}
