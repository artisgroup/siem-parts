﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class ValidationDataConfiguration :EntityTypeConfiguration<ValidationData>
    {
        public ValidationDataConfiguration()
        {
            ToTable("ValidationData", "prd");

            HasKey(vd => vd.Id);

            Property(vd => vd.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(vd => vd.Key)
                .HasMaxLength(40)
                .IsRequired();

            Property(vd => vd.Value)
                .HasMaxLength(512)
                .IsRequired();
        }
    }
}
