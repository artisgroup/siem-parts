﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    /// <summary>
    /// This class defines column attributes for DB table creation
    /// </summary>
    public class DropPointConfiguration : EntityTypeConfiguration<DropPoint>
    {

        public DropPointConfiguration()
        {
            ToTable("DropPoints", "PRD");

            HasKey(dp => dp.Id);

            Property(dp => dp.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(dp => dp.Name1)
                .HasMaxLength(40)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("AK_DropPoint_Name") { IsUnique = true })); 

            Property(dp => dp.Name2)
                .HasMaxLength(40)
                .IsOptional();

            Property(dp => dp.Street)
                .HasMaxLength(40)
                .IsRequired();

            Property(dp => dp.Suburb)
               .HasMaxLength(40)
               .IsRequired();

            Property(dp => dp.PostCode)
                .HasMaxLength(6)
                .IsRequired();

            Property(dp => dp.Region)
                .HasMaxLength(6)
                .IsRequired();

            Property(dp => dp.Remarks)
                .HasMaxLength(120)
                .IsOptional();

            Property(dp => dp.CutOffBne)
                .IsRequired();

            Property(dp => dp.CutOffSyd)
                .IsRequired();

            Property(dp => dp.CutOffMel)
                .IsRequired();

            Property(dp => dp.CutOffAdl)
                .IsRequired();

            Property(dp => dp.CutOffPer)
                .IsRequired();

            Property(dp => dp.Long)
                .IsRequired();

            Property(dp => dp.Lat)
                .IsRequired();

            Property(dp => dp.LastEditedTime)
                .HasColumnType("DateTime")
                .IsRequired();

            Property(dp => dp.LastEditedPerson)
                .HasMaxLength(40)
                .IsRequired();

        }



    }
}
