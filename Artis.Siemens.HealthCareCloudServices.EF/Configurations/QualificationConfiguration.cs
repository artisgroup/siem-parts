﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class QualificationConfiguration : EntityTypeConfiguration<Qualification>
    {

        public QualificationConfiguration()
        {
            ToTable("Qualifications", "sap");

             HasKey(q => q.Id);

            Property(q => q.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(q => q.Name)
                .HasMaxLength(512)
                .IsOptional();

            Property(q => q.GID)
               .HasMaxLength(512)
               .IsOptional();

            Property(q => q.QualificationType)
               .HasMaxLength(512)
               .IsOptional();

            Property(q => q.SAPDivision)
               .HasMaxLength(512)
               .IsOptional();

            Property(q => q.IVKGroup)
               .HasMaxLength(512)
               .IsOptional();

            Property(q => q.QualificationId)
               .IsOptional();

            Property(q => q.QualificationDesc)
               .HasMaxLength(512)
               .IsOptional();

        }
    }
}
