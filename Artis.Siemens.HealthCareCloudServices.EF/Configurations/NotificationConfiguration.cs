﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class NotificationConfiguration : EntityTypeConfiguration<Notification>
    {
        public NotificationConfiguration()
        {

            ToTable("Notifications", "prd");

            HasKey(n => n.Id);

            Property(n => n.Type)
                .HasMaxLength(512)
                .IsOptional();

            Property(n => n.Description)
                .HasMaxLength(512)
                .IsOptional();

            //Property(n => n.EQNumber)
            //    .HasMaxLength(512)
            //    .IsOptional();

            Property(n => n.CallerName)
                .HasMaxLength(512)
                .IsOptional();

            Property(n => n.CallerPhone)
                .HasMaxLength(512)
                .IsOptional();

            Property(n => n.NotificationDateTime)
                .IsOptional();

            Property(n => n.EffectCode)
                .HasMaxLength(512)
                .IsOptional();

            Property(n => n.Priority)
                .HasMaxLength(512)
                .IsOptional();

            //Property(n => n.CallClarified)
            //    .HasMaxLength(512)
            //    .IsOptional();

            //Property(n => n.CallClarifiedEmployeeId)
            //    .IsOptional();

            //Property(n => n.SiteVisit)
            //    .HasMaxLength(512)
            //    .IsOptional();

            //Property(n => n.NoSiteVisits)
            //    .HasMaxLength(512)
            //    .IsOptional();

            //Property(n => n.SparePartOrdered)
            //    .HasMaxLength(512)
            //    .IsOptional();

            Property(n => n.Downtime)
                .HasMaxLength(512)
                .IsOptional();


        }



    }
}
