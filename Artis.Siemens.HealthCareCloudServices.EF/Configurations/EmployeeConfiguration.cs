﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            ToTable("Employees", "prd");

            HasKey(e => e.Id);

            //Property(e => e.Id)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.GID)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.GivenName)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.LastName)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.ServiceRegion)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Team)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Position)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Sloc)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.RetSloc)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Email)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Mobile)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.PartsName1)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.PartsName2)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.PartsName3)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Street)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.City)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.PostCode)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Region)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.Country)
                .HasMaxLength(512)
                .IsOptional();
            Property(e => e.DeliveryRemarks)
                .HasMaxLength(512)
                .IsOptional();
        }
    }
}
