﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class PartConsumptionConfiguration : EntityTypeConfiguration<PartConsumption>
    {

        public PartConsumptionConfiguration()
        {
            ToTable("PartConsumptions", "sap");

            HasKey(pc => pc.Id);

            Property(pc => pc.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(pc => pc.NotificationId)
                .IsOptional();

            Property(pc => pc.MaterialMasterId)
                .IsOptional();

            Property(pc => pc.Batch)
                .HasMaxLength(512)
                .IsOptional();

            Property(pc => pc.Quantity)
                .IsOptional();

            Property(pc => pc.Value)
                .IsOptional();

            Property(pc => pc.ConsumptionDate)
                .IsOptional();
        }
    }
}
