﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class InternationalFlightsCutoffConfiguration : EntityTypeConfiguration<InternationalFlightsCutoff>
    {
        public InternationalFlightsCutoffConfiguration()
        {

            ToTable("InternationalFlightsCutoffs", "prd");

            Property(ifc => ifc.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(ifc => ifc.OriginAirportCode)
                .HasMaxLength(6)
                .IsRequired();

            Property(ifc => ifc.OriginAirportName)
                .HasMaxLength(40)
                .IsRequired();

            Property(ifc => ifc.DestinationAirportCode)
                .HasMaxLength(6)
                .IsRequired();

            Property(ifc => ifc.DestinationAirportName)
                .HasMaxLength(40)
                .IsRequired();

            Property(ifc => ifc.FlightNo)
                .HasMaxLength(10)
                .IsRequired();

            Property(ifc => ifc.CsmlCutoff)
                .IsRequired();

            Property(ifc => ifc.DepartureTime)
                .IsRequired(); 
            
            Property(ifc => ifc.ArrivalTime)
                 .IsRequired();

            Property(ifc => ifc.AvailableforLocal)
                .IsRequired();

            Property(ifc => ifc.EstimatedMetroDelivery)
                .IsRequired();

            Property(ifc => ifc.DaysServiced)
                .HasMaxLength(40)
                .IsRequired();

            Property(ifc => ifc.LastEditedTime)
                .IsRequired();

            Property(ifc => ifc.LastEditedPerson)
                .HasMaxLength(40)
                .IsRequired();

        }
    }
}
