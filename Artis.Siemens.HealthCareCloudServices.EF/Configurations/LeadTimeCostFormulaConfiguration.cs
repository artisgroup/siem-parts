﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class LeadTimeCostFormulaConfiguration : EntityTypeConfiguration<LeadTimeCostFormula>
    {

        public LeadTimeCostFormulaConfiguration()
        {
            ToTable("LeadTimeCostFormulas", "prd");

            Property(f => f.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(f => f.OriginRegion)
                .HasMaxLength(6)
                .IsRequired();

            Property(f => f.DestinationPostCode)
                .HasMaxLength(6)
                .IsRequired();

            Property(f => f.DestinationCity)
                .HasMaxLength(40)
                .IsRequired();

            Property(f => f.DestinationZone)
                .HasMaxLength(40)
                .IsRequired();

            Property(f => f.AvailableServices)
               .HasMaxLength(50)
               .IsRequired();

            Property(f => f.Tailgate)
                .HasMaxLength(6)
                .IsRequired();

            Property(f => f.ServiceLevelCode)
                .HasMaxLength(12)
                .IsRequired();

            Property(f => f.ServiceLevelDescription)
                .HasMaxLength(40)
                .IsRequired();

            Property(f => f.CutOffTime)
                .IsOptional();

            Property(f => f.DeliveryTime)
                //.HasMaxLength(40)
                .IsRequired();

            Property(f => f.RateFormula)
                .HasMaxLength(240)
                .IsRequired();

            Property(f => f.ServiceLevelRemarks)
                .HasMaxLength(120)
                .IsOptional();

            Property(f => f.GenericService)
                 .HasMaxLength(6)
                 .IsRequired();

            Property(f => f.LastEditedTime)
                 .HasColumnType("DateTime")
                 .IsRequired();

            Property(f => f.LastEditedPerson)
                .HasMaxLength(40)
                .IsRequired();

        }
    }
}
