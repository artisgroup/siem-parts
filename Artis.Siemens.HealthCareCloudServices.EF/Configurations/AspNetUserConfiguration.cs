﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class AspNetUserConfiguration : EntityTypeConfiguration<AspNetUser>
    {
        public AspNetUserConfiguration()
        {
            ToTable("AspNetUsers", "prd");
            
        }
    }
}
