﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class IBaseDeliveryDetailConfiguration : EntityTypeConfiguration<IBaseDeliveryDetail>
    {

        public IBaseDeliveryDetailConfiguration()
        {
            ToTable("IBaseDeliveryDetails", "prd");

            Property(dd => dd.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(dd => dd.DropPointId)
                .IsRequired();

            Property(dd => dd.FLId)
                .HasMaxLength(50)
                .IsOptional();

            Property(dd => dd.SpecialRemarks)
                .HasMaxLength(120)
                .IsOptional();

            Property(dd => dd.DeliverySiteContactName)
                .HasMaxLength(40)
                .IsRequired();

            Property(dd => dd.DeliverySiteContactNumber)
                .HasMaxLength(40)
                .IsOptional();

            Property(dd => dd.LastEditedTime)
                .IsRequired();

            Property(dd => dd.LastEditedPerson)
                .HasMaxLength(40)
                .IsRequired();
        }
    }
}
