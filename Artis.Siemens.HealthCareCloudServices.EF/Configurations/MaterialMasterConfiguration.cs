﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class MaterialMasterConfiguration : EntityTypeConfiguration<MaterialMaster>
    {

        public MaterialMasterConfiguration()
        {
            ToTable("MaterialMasters", "PRD");

            HasKey(mm => mm.Id);

            //Property(mm => mm.Id)
            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(mm => mm.MaterialDescription)
                .HasMaxLength(42)
                .IsOptional();

            Property(mm => mm.UnitOfMeasure)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.Size)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.GrossWeight)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.NetWeight)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.WeightUOM)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.Volume)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.VolumeUOM)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.Length)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.Width)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.Height)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.DimensionUOM)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.DangerousGoods)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.SAPBusinessUnit)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.Repairable)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.Quality)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.TechnicalCriticality)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.SparePartType)
                .HasMaxLength(512)
                .IsOptional();

            Property(mm => mm.MovingAveragePrice)
                .HasMaxLength(512)
                .IsOptional();

            //Property(mm => mm.Currency)
            //    .HasMaxLength(512)
            //    .IsOptional();



        }
    }
}
