﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Configurations
{
    public class DeliveryThresholdConfiguration : EntityTypeConfiguration<DeliveryThreshold>
    {
        public DeliveryThresholdConfiguration()
        {
            ToTable("DeliveryThresholds", "prd");

            Property(dt => dt.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(dt => dt.DeliveryType)
                .HasMaxLength(40)
                .IsRequired();

            Property(dt => dt.MaxGrossWeight)
                .IsRequired();

            Property(dt => dt.MaxCombinedDimensions)
                .IsRequired();

            Property(dt => dt.MaxSingleDimension)
                .IsRequired();

            Property(dt => dt.WarningMessage)
                .HasMaxLength(150)
                .IsRequired();
        }
    }
}
