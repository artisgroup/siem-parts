namespace Artis.Siemens.HealthCareCloudServices.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DropPoints", "AK_DropPoint_Name");
            CreateTable(
                "dbo.InternationalFlightsCutoffs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OriginAirportCode = c.String(nullable: false, maxLength: 6),
                        OriginAirportName = c.String(nullable: false, maxLength: 40),
                        DestinationAirportCode = c.String(nullable: false, maxLength: 6),
                        DestinationAirportName = c.String(nullable: false, maxLength: 40),
                        FlightNo = c.String(nullable: false, maxLength: 10),
                        CsmlCutoff = c.Time(nullable: false, precision: 7),
                        DepartureTime = c.Time(nullable: false, precision: 7),
                        ArrivalTime = c.Time(nullable: false, precision: 7),
                        AvailableforLocal = c.Time(nullable: false, precision: 7),
                        EstimatedMetroDelivery = c.Time(nullable: false, precision: 7),
                        DaysServiced = c.String(nullable: false, maxLength: 40),
                        LastEditedTime = c.DateTime(nullable: false),
                        LastEditedPerson = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.DeliveryThresholds", "WarningMessage", c => c.String(maxLength: 120));
            AlterColumn("dbo.DropPoints", "Name2", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "PostCode", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "Region", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "Remarks", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "LastEditedTime", c => c.DateTime());
            AlterColumn("dbo.DropPoints", "LastEditedPerson", c => c.String());
            AlterColumn("dbo.IBaseDeliveryDetails", "LastEditedPerson", c => c.String());
            AlterColumn("dbo.InternationalDestinations", "LastEditedTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.InternationalDestinations", "LastEditedPerson", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InternationalDestinations", "LastEditedPerson", c => c.String(maxLength: 40));
            AlterColumn("dbo.InternationalDestinations", "LastEditedTime", c => c.DateTime());
            AlterColumn("dbo.IBaseDeliveryDetails", "LastEditedPerson", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "LastEditedPerson", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "LastEditedTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DropPoints", "Remarks", c => c.String(maxLength: 120));
            AlterColumn("dbo.DropPoints", "Region", c => c.String(nullable: false, maxLength: 6));
            AlterColumn("dbo.DropPoints", "PostCode", c => c.String(nullable: false, maxLength: 6));
            AlterColumn("dbo.DropPoints", "Name2", c => c.String(maxLength: 40));
            AlterColumn("dbo.DeliveryThresholds", "WarningMessage", c => c.String(nullable: false, maxLength: 120));
            DropTable("dbo.InternationalFlightsCutoffs");
            CreateIndex("dbo.DropPoints", "Name1", unique: true, name: "AK_DropPoint_Name");
        }
    }
}
