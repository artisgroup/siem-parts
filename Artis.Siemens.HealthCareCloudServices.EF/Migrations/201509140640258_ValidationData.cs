namespace Artis.Siemens.HealthCareCloudServices.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ValidationData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ValidationData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(nullable: false, maxLength: 40),
                        Value = c.String(nullable: false, maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ValidationData");
        }
    }
}
