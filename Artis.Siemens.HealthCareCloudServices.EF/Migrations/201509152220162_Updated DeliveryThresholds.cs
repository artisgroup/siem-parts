namespace Artis.Siemens.HealthCareCloudServices.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedDeliveryThresholds : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DeliveryThresholds", "WarningMessage", c => c.String(nullable: false, maxLength: 150));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DeliveryThresholds", "WarningMessage", c => c.String(nullable: false, maxLength: 120));
        }
    }
}
