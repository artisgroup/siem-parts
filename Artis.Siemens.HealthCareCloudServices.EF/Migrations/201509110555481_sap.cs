namespace Artis.Siemens.HealthCareCloudServices.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sap : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "sap.Dispatches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotificationId = c.Int(),
                        Cancelled = c.Boolean(),
                        EmployeeId = c.Int(),
                        ETA = c.DateTime(),
                        Travel = c.DateTime(),
                        OnSiteStart = c.DateTime(),
                        OnSiteFinish = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("sap.Employees", t => t.EmployeeId)
                .ForeignKey("sap.Notifications", t => t.NotificationId)
                .Index(t => t.NotificationId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "sap.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GID = c.String(maxLength: 512),
                        GivenName = c.String(maxLength: 512),
                        LastName = c.String(maxLength: 512),
                        JobType = c.String(),
                        ServiceRegion = c.String(maxLength: 512),
                        Team = c.String(maxLength: 512),
                        Position = c.String(maxLength: 512),
                        Sloc = c.String(maxLength: 512),
                        RetSloc = c.String(maxLength: 512),
                        Email = c.String(maxLength: 512),
                        Mobile = c.String(maxLength: 512),
                        PartsName1 = c.String(maxLength: 512),
                        PartsName2 = c.String(maxLength: 512),
                        PartsName3 = c.String(maxLength: 512),
                        Street = c.String(maxLength: 512),
                        City = c.String(maxLength: 512),
                        PostCode = c.String(maxLength: 512),
                        Region = c.String(maxLength: 512),
                        Country = c.String(maxLength: 512),
                        DeliveryRemarks = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "sap.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(maxLength: 512),
                        Description = c.String(maxLength: 512),
                        FLNumber = c.String(maxLength: 512),
                        CallerName = c.String(maxLength: 512),
                        CallerPhone = c.String(maxLength: 512),
                        NotificationDateTime = c.DateTime(),
                        WorkCenter = c.String(maxLength: 512),
                        ServiceRegion = c.String(maxLength: 512),
                        EffectCode = c.String(maxLength: 512),
                        Priority = c.String(maxLength: 512),
                        Contract = c.String(maxLength: 512),
                        CallClarified = c.String(maxLength: 512),
                        CallClarifiedEmployeeId = c.Int(),
                        SiteVisit = c.String(maxLength: 512),
                        NoSiteVisits = c.String(maxLength: 512),
                        SparePartOrdered = c.String(maxLength: 512),
                        Downtime = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("sap.Employees", t => t.CallClarifiedEmployeeId)
                .ForeignKey("sap.IBases", t => t.FLNumber)
                .Index(t => t.FLNumber)
                .Index(t => t.CallClarifiedEmployeeId);
            
            CreateTable(
                "sap.IBases",
                c => new
                    {
                        FLNumber = c.String(nullable: false, maxLength: 512),
                        FLDesc = c.String(maxLength: 512),
                        EQNumber = c.String(maxLength: 512),
                        EQDesc = c.String(maxLength: 512),
                        MaterialMasterId = c.Int(),
                        SerialNo = c.String(maxLength: 512),
                        SystemStatus = c.String(maxLength: 512),
                        ServiceRegion = c.String(maxLength: 512),
                        Company = c.String(maxLength: 512),
                        Plant = c.String(maxLength: 512),
                        WorkCentre = c.String(maxLength: 512),
                        Name1 = c.String(maxLength: 512),
                        Name2 = c.String(maxLength: 512),
                        Name3 = c.String(maxLength: 512),
                        Name4 = c.String(maxLength: 512),
                        Street = c.String(maxLength: 512),
                        City = c.String(maxLength: 512),
                        PostCode = c.String(maxLength: 512),
                        Region = c.String(maxLength: 512),
                        Country = c.String(maxLength: 512),
                        GeoCodes = c.String(maxLength: 512),
                        TimeZone = c.String(maxLength: 512),
                        Tel = c.String(maxLength: 512),
                        Email = c.String(maxLength: 512),
                        Comments = c.String(maxLength: 512),
                        SortField = c.String(maxLength: 512),
                        SalesOrg = c.String(maxLength: 512),
                        DistChannel = c.String(),
                        SAPDivision = c.String(),
                        URL = c.String(maxLength: 512),
                        EvolvePO = c.String(maxLength: 512),
                        EvolvePOEnd = c.String(maxLength: 512),
                        EmployeeSecCSEId = c.Int(),
                        EmployeePrimCSEId = c.Int(),
                        EmployeePrimFASId = c.Int(),
                        EmployeeServiceMgrId = c.Int(),
                        EmployeeSalesMgrId = c.Int(),
                        ContractLevel = c.String(maxLength: 512),
                        ContractStart = c.DateTime(),
                        ContractEnd = c.DateTime(),
                        EmployeeIDPrimCSE_Id = c.Int(),
                        EmployeeIDPrimFAS_Id = c.Int(),
                        EmployeeIDSalesMgr_Id = c.Int(),
                        EmployeeIDSecCSE_Id = c.Int(),
                        EmployeeIDServiceMgr_Id = c.Int(),
                        Employee_Id = c.Int(),
                        Employee_Id1 = c.Int(),
                        Employee_Id2 = c.Int(),
                        Employee_Id3 = c.Int(),
                        Employee_Id4 = c.Int(),
                    })
                .PrimaryKey(t => t.FLNumber)
                .ForeignKey("sap.Employees", t => t.EmployeeIDPrimCSE_Id)
                .ForeignKey("sap.Employees", t => t.EmployeeIDPrimFAS_Id)
                .ForeignKey("sap.Employees", t => t.EmployeeIDSalesMgr_Id)
                .ForeignKey("sap.Employees", t => t.EmployeeIDSecCSE_Id)
                .ForeignKey("sap.Employees", t => t.EmployeeIDServiceMgr_Id)
                .ForeignKey("sap.MaterialMaster", t => t.MaterialMasterId)
                .ForeignKey("sap.Employees", t => t.Employee_Id)
                .ForeignKey("sap.Employees", t => t.Employee_Id1)
                .ForeignKey("sap.Employees", t => t.Employee_Id2)
                .ForeignKey("sap.Employees", t => t.Employee_Id3)
                .ForeignKey("sap.Employees", t => t.Employee_Id4)
                .Index(t => t.MaterialMasterId)
                .Index(t => t.EmployeeIDPrimCSE_Id)
                .Index(t => t.EmployeeIDPrimFAS_Id)
                .Index(t => t.EmployeeIDSalesMgr_Id)
                .Index(t => t.EmployeeIDSecCSE_Id)
                .Index(t => t.EmployeeIDServiceMgr_Id)
                .Index(t => t.Employee_Id)
                .Index(t => t.Employee_Id1)
                .Index(t => t.Employee_Id2)
                .Index(t => t.Employee_Id3)
                .Index(t => t.Employee_Id4);
            
            CreateTable(
                "sap.MaterialMaster",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Desc = c.String(maxLength: 512),
                        UnitOfMeasure = c.String(maxLength: 512),
                        Size = c.String(maxLength: 512),
                        GrossWeight = c.String(maxLength: 512),
                        NetWeight = c.String(maxLength: 512),
                        WeightUOM = c.String(maxLength: 512),
                        Volume = c.String(maxLength: 512),
                        VolumeUOM = c.String(maxLength: 512),
                        Length = c.String(maxLength: 512),
                        Width = c.String(maxLength: 512),
                        Height = c.String(maxLength: 512),
                        DimUOM = c.String(maxLength: 512),
                        DangerousGoods = c.String(maxLength: 512),
                        SAPBusinessUnit = c.String(maxLength: 512),
                        Repairable = c.String(maxLength: 512),
                        Quality = c.String(maxLength: 512),
                        TechnicalCriticality = c.String(maxLength: 512),
                        SparePartType = c.String(maxLength: 512),
                        MovingAveragePrice = c.String(maxLength: 512),
                        Currency = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "sap.PartConsumptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotificationId = c.Int(),
                        MaterialMasterId = c.Int(),
                        Batch = c.String(maxLength: 512),
                        Quantity = c.Int(),
                        Value = c.Double(),
                        ConsumptionDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("sap.MaterialMaster", t => t.MaterialMasterId)
                .ForeignKey("sap.Notifications", t => t.NotificationId)
                .Index(t => t.NotificationId)
                .Index(t => t.MaterialMasterId);
            
            CreateTable(
                "sap.Qualifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        Name = c.String(maxLength: 512),
                        GID = c.String(maxLength: 512),
                        Type = c.String(maxLength: 512),
                        SAPDivision = c.String(maxLength: 512),
                        IVKGroup = c.String(maxLength: 512),
                        QualificationId = c.Int(),
                        QualificationDesc = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("sap.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "sap.Hierarchies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IVKGroup = c.String(maxLength: 512),
                        SystemGroup = c.String(maxLength: 512),
                        SystemGroupL2 = c.String(maxLength: 512),
                        SystemGroupL3 = c.String(maxLength: 512),
                        Division = c.String(maxLength: 512),
                        BusinessUnit = c.String(maxLength: 512),
                        BusinessUnitSubLevel = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 120),
                        Name = c.String(nullable: false, maxLength: 10),
                        EmployeeId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("sap.Employees", t => t.EmployeeId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Roles", "EmployeeId", "sap.Employees");
            DropForeignKey("sap.IBases", "Employee_Id4", "sap.Employees");
            DropForeignKey("sap.IBases", "Employee_Id3", "sap.Employees");
            DropForeignKey("sap.IBases", "Employee_Id2", "sap.Employees");
            DropForeignKey("sap.Qualifications", "EmployeeId", "sap.Employees");
            DropForeignKey("sap.IBases", "Employee_Id1", "sap.Employees");
            DropForeignKey("sap.IBases", "Employee_Id", "sap.Employees");
            DropForeignKey("sap.Notifications", "FLNumber", "sap.IBases");
            DropForeignKey("sap.IBases", "MaterialMasterId", "sap.MaterialMaster");
            DropForeignKey("sap.PartConsumptions", "NotificationId", "sap.Notifications");
            DropForeignKey("sap.PartConsumptions", "MaterialMasterId", "sap.MaterialMaster");
            DropForeignKey("sap.IBases", "EmployeeIDServiceMgr_Id", "sap.Employees");
            DropForeignKey("sap.IBases", "EmployeeIDSecCSE_Id", "sap.Employees");
            DropForeignKey("sap.IBases", "EmployeeIDSalesMgr_Id", "sap.Employees");
            DropForeignKey("sap.IBases", "EmployeeIDPrimFAS_Id", "sap.Employees");
            DropForeignKey("sap.IBases", "EmployeeIDPrimCSE_Id", "sap.Employees");
            DropForeignKey("sap.Dispatches", "NotificationId", "sap.Notifications");
            DropForeignKey("sap.Notifications", "CallClarifiedEmployeeId", "sap.Employees");
            DropForeignKey("sap.Dispatches", "EmployeeId", "sap.Employees");
            DropIndex("dbo.Roles", new[] { "EmployeeId" });
            DropIndex("sap.Qualifications", new[] { "EmployeeId" });
            DropIndex("sap.PartConsumptions", new[] { "MaterialMasterId" });
            DropIndex("sap.PartConsumptions", new[] { "NotificationId" });
            DropIndex("sap.IBases", new[] { "Employee_Id4" });
            DropIndex("sap.IBases", new[] { "Employee_Id3" });
            DropIndex("sap.IBases", new[] { "Employee_Id2" });
            DropIndex("sap.IBases", new[] { "Employee_Id1" });
            DropIndex("sap.IBases", new[] { "Employee_Id" });
            DropIndex("sap.IBases", new[] { "EmployeeIDServiceMgr_Id" });
            DropIndex("sap.IBases", new[] { "EmployeeIDSecCSE_Id" });
            DropIndex("sap.IBases", new[] { "EmployeeIDSalesMgr_Id" });
            DropIndex("sap.IBases", new[] { "EmployeeIDPrimFAS_Id" });
            DropIndex("sap.IBases", new[] { "EmployeeIDPrimCSE_Id" });
            DropIndex("sap.IBases", new[] { "MaterialMasterId" });
            DropIndex("sap.Notifications", new[] { "CallClarifiedEmployeeId" });
            DropIndex("sap.Notifications", new[] { "FLNumber" });
            DropIndex("sap.Dispatches", new[] { "EmployeeId" });
            DropIndex("sap.Dispatches", new[] { "NotificationId" });
            DropTable("dbo.Roles");
            DropTable("sap.Hierarchies");
            DropTable("sap.Qualifications");
            DropTable("sap.PartConsumptions");
            DropTable("sap.MaterialMaster");
            DropTable("sap.IBases");
            DropTable("sap.Notifications");
            DropTable("sap.Employees");
            DropTable("sap.Dispatches");
        }
    }
}
