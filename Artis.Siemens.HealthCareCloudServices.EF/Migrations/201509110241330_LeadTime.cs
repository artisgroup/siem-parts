namespace Artis.Siemens.HealthCareCloudServices.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LeadTime : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LeadTimeCostFormulas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OriginRegion = c.String(nullable: false, maxLength: 6),
                        DestinationPostCode = c.String(nullable: false, maxLength: 6),
                        DestinationCity = c.String(nullable: false, maxLength: 40),
                        DestinationZone = c.String(nullable: false, maxLength: 40),
                        Tailgate = c.String(nullable: false, maxLength: 6),
                        ServiceLevelCode = c.String(nullable: false, maxLength: 12),
                        ServiceLevelDescription = c.String(nullable: false, maxLength: 40),
                        CutOffTime = c.Time(precision: 7),
                        DeliveryTime = c.String(nullable: false, maxLength: 40),
                        RateFormula = c.String(nullable: false, maxLength: 240),
                        ServiceLevelRemarks = c.String(maxLength: 120),
                        LastEditedTime = c.DateTime(nullable: false),
                        LastEditedPerson = c.String(nullable: false, maxLength: 40),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.DeliveryThresholds", "WarningMessage", c => c.String(nullable: false, maxLength: 120));
            AlterColumn("dbo.DropPoints", "Name2", c => c.String(maxLength: 40));
            AlterColumn("dbo.DropPoints", "PostCode", c => c.String(nullable: false, maxLength: 6));
            AlterColumn("dbo.DropPoints", "Region", c => c.String(nullable: false, maxLength: 6));
            AlterColumn("dbo.DropPoints", "Remarks", c => c.String(maxLength: 120));
            AlterColumn("dbo.DropPoints", "LastEditedTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.DropPoints", "LastEditedPerson", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.IBaseDeliveryDetails", "LastEditedPerson", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.InternationalDestinations", "LastEditedTime", c => c.DateTime());
            AlterColumn("dbo.InternationalDestinations", "LastEditedPerson", c => c.String(maxLength: 40));
            AlterColumn("dbo.InternationalFlightsCutoffs", "LastEditedPerson", c => c.String(nullable: false, maxLength: 40));
            CreateIndex("dbo.DropPoints", "Name1", unique: true, name: "AK_DropPoint_Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.DropPoints", "AK_DropPoint_Name");
            AlterColumn("dbo.InternationalFlightsCutoffs", "LastEditedPerson", c => c.String());
            AlterColumn("dbo.InternationalDestinations", "LastEditedPerson", c => c.String());
            AlterColumn("dbo.InternationalDestinations", "LastEditedTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.IBaseDeliveryDetails", "LastEditedPerson", c => c.String());
            AlterColumn("dbo.DropPoints", "LastEditedPerson", c => c.String());
            AlterColumn("dbo.DropPoints", "LastEditedTime", c => c.DateTime());
            AlterColumn("dbo.DropPoints", "Remarks", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "Region", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "PostCode", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DropPoints", "Name2", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DeliveryThresholds", "WarningMessage", c => c.String(maxLength: 120));
            DropTable("dbo.LeadTimeCostFormulas");
        }
    }
}
