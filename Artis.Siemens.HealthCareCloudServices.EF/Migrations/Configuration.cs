using System.Diagnostics.SymbolStore;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Artis.Siemens.HealthCareCloudServices.EF.HCCSContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Artis.Siemens.HealthCareCloudServices.EF.HCCSContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //context.ValidationData.Add(new ValidationData { Key = "DocType", Value = "EB, ZS, NB, UB" });
            //context.ValidationData.Add(new ValidationData { Key = "SupplyingSloc", Value = "2011, 2012, 2013, 2014, 2015" });
            //context.ValidationData.Add(new ValidationData
            //{
            //    Key = "OriginLocation",
            //    Value =
            //        "UB2011=Schenker SYD, UB2012=Schenker MEL,UB2013=Schenker ADL,UB2014=Schenker PER,UB2015=Schenker BRI,ZS=CSML Singapore,EB=CSML Germany, NB=CSML Germany"
            //});
        }
    }
}
