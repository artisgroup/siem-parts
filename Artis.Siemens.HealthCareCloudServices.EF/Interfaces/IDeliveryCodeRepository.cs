﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface IDeliveryCodeRepository : IEntityRepository<DeliveryCode>
    {
    }
}

