﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface IValidationDataRepository : IEntityRepository<ValidationData>
    {
    }
}
