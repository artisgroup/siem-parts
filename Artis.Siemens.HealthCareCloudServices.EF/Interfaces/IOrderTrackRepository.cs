﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface IOrderTrackRepository : IEntityRepository<OrderTrack>
    {
        IList<OrderTrack> GetTrackDeliveries(OrderTrackPostVM postData);
        //IList<OrderItem> GetPartsOrdered(string purchaseOrder, string conNote);
        IList<OrderItem> GetPartsOrdered(string purchaseOrder);
        //IList<SchenkerStatusUpdate> GetTransportEvents(string purchaseOrder, string conNote);
        IList<SchenkerStatusUpdate> GetTransportEvents(string purchaseOrder);
    }
}
