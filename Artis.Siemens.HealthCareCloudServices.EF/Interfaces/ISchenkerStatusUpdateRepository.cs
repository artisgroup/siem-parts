﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface ISchenkerStatusUpdateRepository : IEntityRepository<SchenkerStatusUpdate>
    {
        void Insert(SchenkerStatusUpdate entity);
        SchenkerOrderVM GetOrderDetails(string serviceLevelCode, int orderId, string warehousePriority, string siemensContactName, string siemensContactPhone, string userFName, string userLName, string userEmail, string Cost);
        //void UpdateSchenkerStatus(int id, string serviceLevelCode, string serviceLevelDescription, string xml, string resp);
        void UpdateOrderItem(string purchaseOrderNumber, string itemNo, string conNote, string eventCode, DateTime? eventDateTime);
        //bool IsDeliveryCode(string code);
        //bool IsETACode(string code);

        //include warehouse priority and Cost
        void UpdateSchenkerStatus(int id, string serviceLevelCode, string serviceLevelDescription, string xml, string resp, string warehousePriority, string cost);
        //post data to Zapier
        SchenkerStatusUpdate PushtoZapierOrder();
        OrderVM GetOrderDetailsZapier(string PONumber, string POLine);
        ZMON GetDetailsZMON(string NotificationNumber);
        IBase GetIBASEDetails(string Requisitioner);
        
    }
}
