﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface IMembershipRoleRepository : IEntityRepository<AspNetUserVM>
    {
        AspNetUserVM GetUserByLoginName(string loginName);
        AspNetUser Create(AspNetUser aspnetuser);
      
    }
}
