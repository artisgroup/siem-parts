﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface IEntityRepository<T> : IDisposable
    {
        IQueryable<T> All { get; }
        
        //IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        T Find(int id);
        void InsertOrUpdate(T entity, EntityState entityState);
        void Delete(int id);
        void Save();
    }
}
