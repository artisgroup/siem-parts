﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface IOrderRepository : IEntityRepository<OrderItemVM>
    {
        bool NotificationNumberExists(string notificationNumber);
        IBase GetIBase(string notificationNumber);
        DropPoint GetDropPoint(string notificationNumber);
        IList<IBase> SearchCS(string query);
        IList<DropPoint> SearchDP(string query);
        Employee GetEmployeeById(string empId);
        Employee GetEmployeeBySloc(string sloc);
        Order Create(Order order);
        OrderConfirmVM GetById(int id);
        OrderConfirmVM GetByPurchaseOrderNumber(string purchaseOrder);
        IList<LeadTimeCostFormula> GetGenericServices();
        DeliveryThreshold GetDeliveryThresholds(string deliveryType);
        bool CheckAllOrderItems(string purchaseOrder, string eventCode);
    }
}
