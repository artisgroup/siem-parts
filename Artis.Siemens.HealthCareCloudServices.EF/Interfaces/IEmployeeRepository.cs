﻿using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artis.Siemens.HealthCareCloudServices.ViewModels;

namespace Artis.Siemens.HealthCareCloudServices.EF.Interfaces
{
    public interface IEmployeeRepository : IEntityRepository<Employee>
    {
        IList<Employee> GetAll { get; }
        Employee GetById(string id);
        Employee GetByEmployeeId(string id);
        IList<Employee> SearchEA(string query);
    }
}
