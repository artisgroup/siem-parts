﻿using System.Data.Entity;
using Artis.Siemens.HealthCareCloudServices.Domain.Models;
using Artis.Siemens.HealthCareCloudServices.EF.Configurations;

namespace Artis.Siemens.HealthCareCloudServices.EF
{
    public class HCCSContext : DbContext
    {
        public DbSet<Dispatch> Dispatches { get; set; }
        public DbSet<DeliveryCode> DeliveryCodes { get; set; }
        public DbSet<DeliveryThreshold> DeliveryThresholds { get; set; }
        public DbSet<DropPoint> DropPoints { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Hierarchy> Hierarchies { get; set; }
        public DbSet<IBase> IBases { get; set; }
        public DbSet<IBaseDeliveryDetail> IBaseDeliveryDetails { get; set; }
        public DbSet<InternationalDestination> InternationalDestinations { get; set; }
        public DbSet<InternationalFlightsCutoff> InternationalFlightsCutoffs { get; set; }
        public DbSet<LeadTimeCostFormula> LeadTimeCostFormulas { get; set; }
        public DbSet<MaterialMaster> MaterialMasters { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<PartConsumption> PartConsumptions { get; set; }
        public DbSet<Plant> Plants { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet ValidationData { get; set; }
        public DbSet<ZMON> ZMONs { get; set; }
        
        public DbSet<AspNetRole> AspNetRoles { get; set; }
        public DbSet<AspNetUser> AspNetUsers { get; set; }
        //public DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<SchenkerStatusUpdate> SchenkerStatusUpdates { get; set; }

        public HCCSContext()
            : base("HCCS") 
        {}



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DeliveryThresholdConfiguration());
            modelBuilder.Configurations.Add(new DispatchConfiguration());
            modelBuilder.Configurations.Add(new DropPointConfiguration());
            modelBuilder.Configurations.Add(new DeliveryCodeConfiguration());
            modelBuilder.Configurations.Add(new EmployeeConfiguration());
            modelBuilder.Configurations.Add(new HierarchyConfiguration());
            modelBuilder.Configurations.Add(new IBaseConfiguration());
            modelBuilder.Configurations.Add(new IBaseDeliveryDetailConfiguration());
            modelBuilder.Configurations.Add(new InternationalDestinationConfiguration());
            modelBuilder.Configurations.Add(new InternationalFlightsCutoffConfiguration());
            modelBuilder.Configurations.Add(new LeadTimeCostFormulaConfiguration());
            modelBuilder.Configurations.Add(new MaterialMasterConfiguration());
            modelBuilder.Configurations.Add(new NotificationConfiguration());
            modelBuilder.Configurations.Add(new PartConsumptionConfiguration());
            modelBuilder.Configurations.Add(new PlantConfiguration());
            modelBuilder.Configurations.Add(new QualificationConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new ValidationDataConfiguration());
            modelBuilder.Configurations.Add(new ZMONConfiguration());

            modelBuilder.Configurations.Add(new AspNetRoleConfiguration());
            modelBuilder.Configurations.Add(new AspNetUserConfiguration());
            

            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new OrderItemConfiguration());
            modelBuilder.Configurations.Add(new SchenkerStatusUpdateConfiguration());

            

            modelBuilder.Entity<AspNetUser>()
            .HasMany(a => a.Roles)
            .WithMany()
            .Map(x =>
            {
                x.MapLeftKey("UserId");
                x.MapRightKey("RoleId");
                x.ToTable("AspNetUserRoles","prd");
            });

            //     modelBuilder.Entity<AspNetUser>()
            //.HasMany(c => c.Roles)
            //.WithRequired()
            //.HasForeignKey(c => c.Id);

            //     modelBuilder.Entity<AspNetRole>()
            //         .HasMany(c => c.Users)
            //         .WithRequired()
            //         .HasForeignKey(c => c.Id); 

            //modelBuilder.Configurations.Add(new AspNetUserRolesConfiguration());

            modelBuilder.Entity<Notification>()
                .HasRequired(c => c.CallClarifiedEmployee)
                .WithMany(t => t.Notifications)
                .Map(m => m.MapKey("CallClarifiedEmployeeId"));

            modelBuilder.Entity<Notification>()
                .HasRequired(c => c.FLBase)
                .WithMany(t => t.Notifications)
                .Map(m => m.MapKey("EQNumber"));

            base.OnModelCreating(modelBuilder);
        }

      

        /*
         * NOTES:
         * 1.   Create Domain Classes
         * 2.   Create Context class and add DbSets: Ref domain clasess project
         * 3.   Create Configuration classes and over OnModelCreating
         * 4.   Use PM console to establish migration process: ensure correct project is selected (.EF)
         *      
         *      PM> enable-migrations -contexttypename HCCSContext
         *      Checking if the context targets an existing database...
         *      Code First Migrations enabled for project Artis.Siemens.HealthCareCloudServices.EF.
         *      
         * 5.   Generate the code to create the database
         * 
         *      PM> add-migration 'Initial Configuration'
         *      Scaffolding migration 'Initial Configuration'.
         *      The Designer Code for this migration file includes a snapshot of your current Code First model. 
         *      This snapshot is used to calculate the changes to your model when you scaffold the next migration. 
         *      If you make additional changes to your model that you want to include in this migration, 
         *      then you can re-scaffold it by running 'Add-Migration Initial Configuration' again.
         *      NOTE: Comment out OnModelScaffolding when performing a new add-migration
         *      
         *      The following file is created; can be reviewed before updating database.
         *      - edit this file to establish computed values, default date values etc.
         *      - See MasterDetail demo for examples.
         *      
         * 6.   The final step is to update the database
         * 
         *      PM> update-database  -verbose
         *      Using StartUp project 'Artis.Siemens.HealthCareCloudServices.MVC'.
         *      Using NuGet project 'Artis.Siemens.HealthCareCloudServices.EF'.
         *      Specify the '-Verbose' flag to view the SQL statements being applied to the target database.
         *      Target database is: 'Artis.Siemens.HealthCareCloudServices.EF.HCCSContext' (DataSource: (localdb)\mssqllocaldb, Provider: System.Data.SqlClient, Origin: Convention).
         *      Applying explicit migrations: [201509081153586_Initial Configuration].
         *      Applying explicit migration: 201509081153586_Initial Configuration.
         *      CREATE TABLE [dbo].[DeliveryThresholds] (
         *      [Id] [int] NOT NULL,
         *      [DeliveryType] [nvarchar](40) NOT NULL,
         *      ........
         * 
         *      NOTE:
         *      This Configuration in the Migrations folder can include code to seed the database.
         *      The data to be seeded may be added incrementally; does not clobber previously seeded data.
         *      update-database may be run whenever  schema changes are main to the domain classes (and the
         *      associated configuration classes).
         *      If there no schema changes the the u[pdate just re-runs the seed process.
         */

    }
}
